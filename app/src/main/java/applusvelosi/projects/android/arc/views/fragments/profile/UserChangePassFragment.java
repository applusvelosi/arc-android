package applusvelosi.projects.android.arc.views.fragments.profile;

import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnFocusChangeListener;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;
import applusvelosi.projects.android.arc.R;
import applusvelosi.projects.android.arc.utils.SaltProgressDialog;
import applusvelosi.projects.android.arc.utils.interfaces.RootFragment;
import applusvelosi.projects.android.arc.views.fragments.ActionbarFragment;

public class UserChangePassFragment extends ActionbarFragment implements RootFragment, OnFocusChangeListener{
	private static UserChangePassFragment instance;
	private View actionbarButtonMenu, actionbarButtonDone;
	
	private EditText etOldPassword, etNewPassword, etConfirmPassword;
	private SaltProgressDialog pd;
	
	public static UserChangePassFragment getInstance(){
		if(instance == null)
			instance = new UserChangePassFragment();
		
		return instance;
	}
	
	@Override
	protected RelativeLayout setupActionbar() {
		RelativeLayout actionbar = (RelativeLayout)LayoutInflater.from(activity).inflate(R.layout.actionbar_menudone, null);
		actionbarButtonMenu = actionbar.findViewById(R.id.buttons_actionbar_menu);
		actionbarButtonDone = actionbar.findViewById(R.id.buttons_actionbar_done);
		((TextView)actionbar.findViewById(R.id.tviews_actionbar_title)).setText("Change Password");
		
		actionbarButtonMenu.setOnClickListener(this);
		actionbarButtonDone.setOnClickListener(this);
		return actionbar;
	}

	@Override
	protected View createView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		View v = inflater.inflate(R.layout.fragment_profile_changepass, null);
		
		etOldPassword = (EditText)v.findViewById(R.id.etexts_profile_changepass_oldpass);
		etNewPassword = (EditText)v.findViewById(R.id.etexts_profile_changepass_newpass);
		etConfirmPassword = (EditText)v.findViewById(R.id.etexts_profile_changepass_confirm);
		
		etOldPassword.setOnFocusChangeListener(this);
		etNewPassword.setOnFocusChangeListener(this);
		etConfirmPassword.setOnFocusChangeListener(this);
		
		return v;
	}

	@Override
	public void onClick(View v) {
		if(v == actionbarButtonMenu){
			v.setEnabled(false);
			activity.toggleSidebar(v);
		}else if(v == actionbarButtonDone){
			if(etOldPassword.length() < 1)
				app.showMessageDialog(activity, "Old Password must not be empty");
			else if(etNewPassword.length() < 1)
				app.showMessageDialog(activity, "New Password must not be empty");
			else if(etConfirmPassword.length() < 1)
				app.showMessageDialog(activity, "Confirm Password must not be empty");
			else{
				if(!etNewPassword.getText().toString().equals(etConfirmPassword.getText().toString()))
					app.showMessageDialog(activity, "Confirm Password do not match");
				else{
					if(pd == null)
						pd = new SaltProgressDialog(activity);
					pd.show();
					new Thread(new PasswordChanger()).start();
				}
			}
		}
	}

	@Override
	public void disableUserInteractionsOnSidebarShown() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void enableUserInteractionsOnSidebarHidden() {
		// TODO Auto-generated method stub
		
	}
	
	private class PasswordChanger implements Runnable{
		
		@Override
		public void run() {
			String tempResult;
			try{
				tempResult = app.onlineGateway.changePassword(etOldPassword.getText().toString(), etNewPassword.getText().toString());
			}catch(Exception e){
				e.printStackTrace();
				tempResult = e.getMessage();
			}
			final String result = tempResult;
			
			new Handler(Looper.getMainLooper()).post(new Runnable() {
				
				@Override
				public void run() {
					pd.dismiss();
					app.showMessageDialog(activity, (result.equals("OK"))?"Changed Successfully!":result);
				}
			});
		}		
	}

	@Override
	public void onFocusChange(View v, boolean hasFocus) {
		((TextView)v).setTextColor(activity.getResources().getColor((hasFocus)?R.color.black:R.color.menu_list_separator));
	}

}
