package applusvelosi.projects.android.arc.adapters.lists;

import java.util.ArrayList;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;
import applusvelosi.projects.android.arc.ArcApplication;
import applusvelosi.projects.android.arc.R;
import applusvelosi.projects.android.arc.models.Application;
import applusvelosi.projects.android.arc.models.Document;
import applusvelosi.projects.android.arc.views.SidebarActivity;

public class AppListAdapter extends BaseAdapter{
	private SidebarActivity activity;
	private ArrayList<Application> apps;
	
	public AppListAdapter(SidebarActivity activity, ArrayList<Application> apps){
		this.activity = activity;
		this.apps = apps;
	}

	@Override
	public View getView(int pos, View v, ViewGroup parent) {
		Holder holder;
		if(v == null){
			holder = new Holder();
			v = LayoutInflater.from(activity).inflate(R.layout.cell_application, null);
			holder.tvTitle = (TextView)v.findViewById(R.id.tviews_profile_app_title);
			holder.tvRef = (TextView)v.findViewById(R.id.tviews_profile_app_ref);
			holder.tvAddedOn = (TextView)v.findViewById(R.id.tviews_profile_app_addedon);
			holder.tvStatus = (TextView)v.findViewById(R.id.tviews_profile_app_status);
			v.setTag(holder);
		}
		
		holder = (Holder)v.getTag();
		Application app = apps.get(pos);
		holder.tvTitle.setText(app.getVacancyTitle());
		holder.tvRef.setText("Reference: "+app.getVacancyRef());
		holder.tvAddedOn.setText("Added on "+app.getDateCreated(((ArcApplication)activity.getApplication()).onlineGateway));
		holder.tvStatus.setText(app.getStatusName());
		
		return v;
	}
	
	@Override
	public int getCount() {
		return apps.size();
	}

	@Override
	public Object getItem(int pos) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public long getItemId(int pos) {
		// TODO Auto-generated method stub
		return 0;
	}

	private class Holder{
		private TextView tvTitle, tvRef, tvAddedOn, tvStatus;
	}
}
