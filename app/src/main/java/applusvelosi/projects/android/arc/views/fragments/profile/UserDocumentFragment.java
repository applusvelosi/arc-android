package applusvelosi.projects.android.arc.views.fragments.profile;

import java.util.ArrayList;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemLongClickListener;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import applusvelosi.projects.android.arc.R;
import applusvelosi.projects.android.arc.adapters.lists.DocListAdapter;
import applusvelosi.projects.android.arc.models.Document;
import applusvelosi.projects.android.arc.utils.SaltProgressDialog;
import applusvelosi.projects.android.arc.utils.interfaces.RootFragment;
import applusvelosi.projects.android.arc.views.fragments.ActionbarFragment;

public class UserDocumentFragment extends ActionbarFragment implements RootFragment, OnItemLongClickListener{
	private static UserDocumentFragment instance;
	private View actionbarButtonMenu, actionbarButtonRefresh;
	
	private SaltProgressDialog pd;
	private ListView lv;
	private DocListAdapter adapter;
	private ArrayList<Document> docs;
	
	public static UserDocumentFragment getInstance(){
		if(instance == null)
			instance = new UserDocumentFragment();
		
		return instance;
	}
	
	public static void destroyInstance(){
		instance = null;
	}	
	
	@Override
	protected RelativeLayout setupActionbar() {
		RelativeLayout actionbar = (RelativeLayout)LayoutInflater.from(activity).inflate(R.layout.actionbar_menurefresh, null);
		actionbarButtonMenu = actionbar.findViewById(R.id.buttons_actionbar_menu);
		actionbarButtonRefresh = actionbar.findViewById(R.id.buttons_actionbar_refresh);
		((TextView)actionbar.findViewById(R.id.tviews_actionbar_title)).setText("Documents");
		
		actionbarButtonMenu.setOnClickListener(this);
		actionbarButtonRefresh.setOnClickListener(this);
		return actionbar;
	}

	@Override
	protected View createView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		View v = inflater.inflate(R.layout.fragment_lv, null);
		lv = (ListView)v.findViewById(R.id.lists_lv);
		pd = new SaltProgressDialog(activity);
		if(docs == null){
			docs = new ArrayList<Document>();
			pd.show();
			new Thread(new DocListLoader()).start();
		}
		
		LinearLayout headerView = (LinearLayout)inflater.inflate(R.layout.tv_listview_header, null);
		((TextView)headerView.getChildAt(0)).setText("Below are the documents currently held on your account. Log in online to add or update documents");
		lv.addHeaderView(headerView, null, false);
		
		adapter = new DocListAdapter(activity, docs);
		lv.setAdapter(adapter);
		lv.setOnItemLongClickListener(this);
		
		return v;
	}

	@Override
	public void onClick(View v) {
		if(v == actionbarButtonMenu){
			v.setEnabled(false);
			activity.toggleSidebar(v);
		}else if(v == actionbarButtonRefresh){
			pd.show();
			new Thread(new DocListLoader()).start();
		}
	}

	@Override
	public void disableUserInteractionsOnSidebarShown() {
		lv.setEnabled(false);
	}

	@Override
	public void enableUserInteractionsOnSidebarHidden() {
		lv.setEnabled(true);
	}

	private class DocListLoader implements Runnable{

		@Override
		public void run() {
			Object tempResult;
			try{
				tempResult = app.onlineGateway.getDocuments();
			}catch(Exception e){
				tempResult = e.getMessage();
			}
			final Object result = tempResult;
			
			new Handler(Looper.getMainLooper()).post(new Runnable() {
				
				@Override
				public void run() {
					pd.dismiss();
					if(result instanceof String){
						app.showMessageDialog(activity, result.toString());
					}else{
						docs.clear();
						docs.addAll((ArrayList<Document>)result);
						adapter.notifyDataSetChanged();
					}
					
				}
			});
		}
		
	}

	@Override
	public boolean onItemLongClick(AdapterView<?> parent, View view, final int pos, long id) {
		final Document doc = docs.get(pos-1);
		new AlertDialog.Builder(activity).setTitle(null)
										 .setMessage("Delete "+doc.getName())
										 .setPositiveButton("Confirm", new DialogInterface.OnClickListener() {
											
											@Override
											public void onClick(DialogInterface dialog, int which) {
												dialog.dismiss();
												pd.show();
												new Thread(new Runnable() {
													
													@Override
													public void run() {
														String tempResult;
														try{
															tempResult = app.onlineGateway.deleteDocument(doc.getDocumentID());
														}catch(Exception e){
															e.printStackTrace();
															tempResult = e.getMessage();
														}
														final String result = tempResult;
														
														new Handler(Looper.getMainLooper()).post(new Runnable() {
															
															@Override
															public void run() {
																pd.dismiss();
																if(result.equals("OK")){
																	app.showMessageDialog(activity, "Deleted Successfully");
																	docs.remove(pos-1);
																	adapter.notifyDataSetChanged();
																}else{
																	app.showMessageDialog(activity, result);																	
																}
															}
														});
													}
												}).start();
											}
										})
										.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
											
											@Override
											public void onClick(DialogInterface dialog, int which) {
												dialog.dismiss();
											}
										}).create().show();
		return true;	
	}
}
