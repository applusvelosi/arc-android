package applusvelosi.projects.android.arc.models;

import java.util.Date;
import java.util.HashMap;

import org.json.JSONObject;

import applusvelosi.projects.android.arc.ArcApplication;
import applusvelosi.projects.android.arc.utils.OnlineGateway;

public class Application {

	private HashMap<String, Object> map;
	
	public Application(JSONObject jsonApp) throws Exception{
		map = new HashMap<String, Object>();
		if(jsonApp != JSONObject.NULL) {
			map = OnlineGateway.toMap(jsonApp);
	    }
	}
	
	public int getApplicationID(){
		return Integer.parseInt(map.get("ApplicationID").toString());
	}
	
	public String getVacancyTitle(){
		return map.get("VacancyTitle").toString();
	}
	
	public int getVacancyID(){
		return Integer.parseInt(map.get("VacancyID").toString());
	}

	public String getVacancyRef(){
		return map.get("VacancyRef").toString();
	}

	public String getAppStatus(){
		return map.get("AppStatus").toString();
	}
	
	public String getStatusName(){
		return ((HashMap<String, Object>)map.get("AppStatus")).get("StatusName").toString();
	}

	public String getDateCreated(OnlineGateway g){
		return g.dJsonizeDate(map.get("DateCreated").toString());
	}
	
	public static  String getJSONForJobApplication(JobDetail job, User user, String coverLetter, ArcApplication app) throws Exception{
		String defaultDate = "/Date(-2179180800000+0000)/";
		HashMap<String, Object> application = new HashMap<String, Object>();
		application.put("ActualFinishDate", job.getDateTo());
		application.put("ActualStartDate", job.getDateFrom());
		HashMap<String, Object> appStatus = new HashMap<String, Object>();
		appStatus.put("Status", 2);
		appStatus.put("StatusID", 2);
		appStatus.put("StatusName", "Applied Online");
		application.put("AppStatus", appStatus);
		application.put("AppStatusID", 0);
		application.put("ApplicationID", 0);
		application.put("AppliedOnline", app.onlineGateway.jsonizeDate(new Date()));
		application.put("CVSubmitted", defaultDate);
		application.put("CandidateEmail", user.getEmail());
		application.put("CandidateID", Integer.parseInt(app.offlineGateway.getUserID()));
		application.put("CandidateName", user.getFirstName()+" "+user.getLastname());
		application.put("ClientAccepted", defaultDate);
		application.put("ClientGroupID", job.getClientGroupID());
		application.put("ClientID", job.getClientID());
		application.put("ClientName", job.getClientName());
		application.put("ClientRejected", defaultDate);
		application.put("CoverLetter", coverLetter);
		application.put("DateFrom", job.getDateFrom());
		application.put("DateModified", app.onlineGateway.jsonizeDate(new Date()));
		application.put("DateTo", job.getDateTo());
		application.put("EstimatedFinishDate", job.getDateTo());
		application.put("EstimatedStartDate", job.getDateFrom());
		application.put("Interested", defaultDate);
		application.put("InterviewDate", defaultDate);
		application.put("InterviewDate2", defaultDate);
		application.put("Notes", "");
		application.put("Notified", false);
		HashMap<String, Object> obStatus = new HashMap<String, Object>();
		obStatus.put("Status", 0);
		obStatus.put("StatusID", 0);
		obStatus.put("StatusName", "Unknown");
		application.put("OBStatusID", 0);
		application.put("OfferRejected", defaultDate);
		application.put("OfficeID", job.getOfficeID());
		application.put("OnBoardList", 0);
		application.put("ReferrerID", user.getReferrerID());
		application.put("Resourced", defaultDate);
		application.put("Shortlisted", defaultDate);
		application.put("StaffID", Integer.parseInt(app.offlineGateway.getUserID()));
		application.put("Unsuitable", defaultDate);
		application.put("VacancyID", job.getJobID());
		application.put("VacancyRef", job.getReference());
		application.put("VacancyStaffID", Integer.parseInt(app.offlineGateway.getUserID()));
		application.put("VacancyTitle", job.getTitle());
		
		return app.gson.toJson(application, app.types.hashmapOfStringObject);
	}
	
}
