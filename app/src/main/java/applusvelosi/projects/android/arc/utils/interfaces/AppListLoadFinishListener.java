package applusvelosi.projects.android.arc.utils.interfaces;

public interface AppListLoadFinishListener {

	public void onMyLeavesLoadSuccess();
	public void onMyLeavesLoadFailed(String errorMessage);
}
