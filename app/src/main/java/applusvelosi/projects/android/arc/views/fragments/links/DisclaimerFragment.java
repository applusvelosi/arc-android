package applusvelosi.projects.android.arc.views.fragments.links;

import android.graphics.Bitmap;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.RelativeLayout;
import android.widget.TextView;
import applusvelosi.projects.android.arc.R;
import applusvelosi.projects.android.arc.utils.SaltProgressDialog;
import applusvelosi.projects.android.arc.utils.interfaces.RootFragment;
import applusvelosi.projects.android.arc.views.fragments.ActionbarFragment;

public class DisclaimerFragment extends ActionbarFragment implements RootFragment{
	private static DisclaimerFragment instance;
	
	private SaltProgressDialog pd;
	private View actionbarMenuButton, actionbarRefreshButton;
	private WebView webView;

	private RelativeLayout v;
	
	public static DisclaimerFragment getInstance(){
		if(instance == null)
			instance = new DisclaimerFragment();
		
		return instance;
	}
	
	@Override
	protected RelativeLayout setupActionbar() {
		RelativeLayout actionbarLayout = (RelativeLayout)LayoutInflater.from(activity).inflate(R.layout.actionbar_menurefresh, null);
		actionbarMenuButton = actionbarLayout.findViewById(R.id.buttons_actionbar_menu);
		actionbarRefreshButton = actionbarLayout.findViewById(R.id.buttons_actionbar_refresh);
		((TextView)actionbarLayout.findViewById(R.id.tviews_actionbar_title)).setText("Disclaimer");

		actionbarMenuButton.setOnClickListener(this);
		actionbarRefreshButton.setOnClickListener(this);
		
		return actionbarLayout;
	}

	@Override
	protected View createView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		v = (RelativeLayout)inflater.inflate(R.layout.fragment_webview, null);
		pd = new SaltProgressDialog(activity);
		if(webView == null){
			webView = (WebView)inflater.inflate(R.layout.webview, null);
			webView.setWebViewClient(new webClient());
			webView.loadUrl("http://www.velosijobs.com/app/disclaimer");			
		}else
			((RelativeLayout)webView.getParent()).removeAllViews();
		
		v.addView(webView);
		return v;
	}

	@Override
	public void onClick(View view) {
		if(view == actionbarMenuButton){
			v.setEnabled(false);
			activity.toggleSidebar(v);
		}else if(view == actionbarRefreshButton){
			webView.loadUrl("http://www.velosijobs.com/app/disclaimer");			
		}
	}

	@Override
	public void disableUserInteractionsOnSidebarShown() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void enableUserInteractionsOnSidebarHidden() {
		// TODO Auto-generated method stub
		
	}
	
    private class webClient extends WebViewClient {

        @Override
        public boolean shouldOverrideUrlLoading(WebView view, String url) {
            return false;
        }

        @Override
        public void onPageStarted(WebView view, String url, Bitmap favicon) {
        	pd.show();
        	super.onPageStarted(view, url, favicon);
        }

        @Override
        public void onPageFinished(WebView view, String url) {
        	super.onPageFinished(view, url);
        	pd.dismiss();
        }
        
    }

}
