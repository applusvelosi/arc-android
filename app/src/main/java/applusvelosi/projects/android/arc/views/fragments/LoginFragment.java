package applusvelosi.projects.android.arc.views.fragments;

import org.json.JSONObject;

import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;
import applusvelosi.projects.android.arc.R;
import applusvelosi.projects.android.arc.models.User;
import applusvelosi.projects.android.arc.utils.SaltProgressDialog;
import applusvelosi.projects.android.arc.utils.interfaces.RootFragment;
import applusvelosi.projects.android.arc.views.SidebarActivity;
import applusvelosi.projects.android.arc.views.fragments.profile.UserDetailFragment;


public class LoginFragment extends ActionbarFragment implements OnClickListener, RootFragment{
	private SaltProgressDialog pd;
	private View buttonLogin, buttonList, buttonForgotPassword;
	private TextView tviewUsername, tviewPassword;
	private boolean isViewCreatead = false;
		
	@Override
	protected RelativeLayout setupActionbar() {
		RelativeLayout actionbarLayout = (RelativeLayout)activity.getLayoutInflater().inflate(R.layout.actionbar_login, null);
		buttonList = actionbarLayout.findViewById(R.id.buttons_actionbar_menu);
		buttonList.setOnClickListener(this);
		return actionbarLayout;
	}

	@Override
	protected View createView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		View view = inflater.inflate(R.layout.fragment_login, null);

		buttonForgotPassword = view.findViewById(R.id.tviews_login_forgotpassword);
		buttonLogin = view.findViewById(R.id.buttons_login_login);
		buttonForgotPassword.setOnClickListener(this);
		buttonLogin.setOnClickListener(this);
		
		tviewUsername = (TextView)view.findViewById(R.id.etexts_login_username);
		tviewPassword = (TextView)view.findViewById(R.id.etexts_login_password);
		
//		tviewUsername.setText("applusvelositest@gmail.com");
//		tviewPassword.setText("Password1234");
		isViewCreatead = true;
		return view;
	}
	
	
	@Override
	public void onClick(View v) {
		if(v == buttonList){
			v.setEnabled(false);
			((SidebarActivity)getActivity()).toggleSidebar(v);
		}else if(v == buttonForgotPassword){
			activity.changeChildPage(new ForgotPasswordFragment());
		}else if(v == buttonLogin){
			if(pd == null)
				pd = new SaltProgressDialog(activity);
			pd.show();
			new Thread(new Runnable() {
				
				@Override
				public void run() {
					Object tempResult;
					try{
						tempResult = app.onlineGateway.login(tviewUsername.getText().toString(), tviewPassword.getText().toString());						
					}catch(Exception e){
						e.printStackTrace();
						tempResult = e.getMessage();
					}
					final Object result = tempResult;
					new Handler(Looper.getMainLooper()).post(new Runnable() {
						
						@Override
						public void run() {
							pd.dismiss();
							if(result == null)
								app.showMessageDialog(activity, "Username or password is incorrect");
							else{								
								try{
									System.out.println(result.getClass());
									app.offlineGateway.saveUserDataWithID(((JSONObject)result).getString("CandidateID"));
									activity.login(LoginFragment.this, (JSONObject)result);
								}catch(Exception e){
									app.showMessageDialog(activity, e.getMessage());
								}
							}
						}
					});
				}
			}).start();
		}
	}

	@Override
	public void disableUserInteractionsOnSidebarShown() {
		new Thread(new Runnable() {
			
			@Override
			public void run() {
				while(!isViewCreatead){}
				new Handler(Looper.getMainLooper()).post(new Runnable() {
					
					@Override
					public void run() {
						buttonForgotPassword.setEnabled(false);
						buttonLogin.setEnabled(false);
						tviewUsername.setEnabled(false);
						tviewPassword.setEnabled(false);
					}
				});
			}
		}).start();
	}

	@Override
	public void enableUserInteractionsOnSidebarHidden() {
		new Thread(new Runnable() {
			
			@Override
			public void run() {
				while(!isViewCreatead){}
				new Handler(Looper.getMainLooper()).post(new Runnable() {
					
					@Override
					public void run() {
						buttonForgotPassword.setEnabled(true);
						buttonLogin.setEnabled(true);
						tviewUsername.setEnabled(true);
						tviewPassword.setEnabled(true);						
					}
				});
			}
		}).start();
	}

}
