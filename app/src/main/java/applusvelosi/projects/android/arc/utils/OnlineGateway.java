package applusvelosi.projects.android.arc.utils;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;

import applusvelosi.projects.android.arc.ArcApplication;
import applusvelosi.projects.android.arc.models.Application;
import applusvelosi.projects.android.arc.models.Document;
import applusvelosi.projects.android.arc.models.Employment;
import applusvelosi.projects.android.arc.models.JobDetail;
import applusvelosi.projects.android.arc.models.JobLatest;
import applusvelosi.projects.android.arc.models.JobSummary;
import applusvelosi.projects.android.arc.models.SavedSearch;

public class OnlineGateway {
	//api urls
	private final String apiURLLocation = "https://arcapi.velosi.com/Geolocation.svc/json/";
	private final String apiURLVacancy = "https://arcapi.velosi.com/VacancySvc.svc/json/";
	private final String apiURLCandidates = "https://arcapi.velosi.com/CandidateSvc.svc/json/";
	private final String apiURLUserInfoReferences = "https://arcapi.velosi.com/Reference.svc/json/";
	private final String apiURLDocuments = "https://arcapi.velosi.com/DocumentSvc.svc/json/";
	private final String apiURLEmployments = "https://arcapi.velosi.com/CandidateJobSvc.svc/json/";
	private final String apiURLApplications = "https://arcapi.velosi.com/ApplicationSvc.svc/json/";
	private final String apiURLSavedSearches = "https://arcapi.velosi.com/JobsByEmailSvc.svc/json/";

//	private final String apiURLLocation = "http://10.63.201.28:80/Geolocation.svc/json/";
//	private final String apiURLVacancy = "http://10.63.201.28:80/VacancySvc.svc/json/";
//	private final String apiURLCandidates = "http://10.63.201.28:80/CandidateSvc.svc/json/";
//	private final String apiURLUserInfoReferences = "http://10.63.201.28:80/Reference.svc/json/";
//	private final String apiURLDocuments = "http://10.63.201.28:80/DocumentSvc.svc/json/";
//	private final String apiURLEmployments = "http://10.63.201.28:80/CandidateJobSvc.svc/json/";
//	private final String apiURLApplications = "http://10.63.201.28:80/ApplicationSvc.svc/json/";
//	private final String apiURLSavedSearches = "http://10.63.201.28:80/JobsByEmailSvc.svc/json/";
	private WebRequest webRequest;
	public ArcApplication app;
	
	public OnlineGateway(ArcApplication app){
		this.app = app;
		webRequest = new WebRequest();
	}
	
	//UTILITY METHODS
	private Bitmap getBitmapFromUrl(String src) throws IOException{
        URL url = new URL(src);
        HttpURLConnection connection = (HttpURLConnection) url.openConnection();
        connection.setDoInput(true);
        connection.connect();
        InputStream input = connection.getInputStream();
        return BitmapFactory.decodeStream(input);
	}

	//Deprecated
//	private String httpGet(String url) throws Exception {
//		System.out.println("GET URL: "+url);
//		HttpClient httpclient = new DefaultHttpClient();
//		HttpGet httpGETRequest = new HttpGet(url);
//		httpGETRequest.setHeader("AuthKey", "3/iQGvD5]cmTu85H(syvsg");
//	    HttpResponse response = httpclient.execute(httpGETRequest);
//	    return EntityUtils.toString(response.getEntity());
//	}
	//Deprecated
//	private String httpPost(String url, String postBodyJSON) throws Exception{
//		System.out.println("POST URL: "+url);
//		System.out.println("POST Body "+postBodyJSON);
//
//		HttpClient httpClient = new DefaultHttpClient();
//		HttpPost httpPostRequest = new HttpPost(url);
//		httpPostRequest.setHeader("Content-Type", "application/json");
//		httpPostRequest.setHeader("Accept", "application/json");
//		httpPostRequest.setHeader("AuthKey", "3/iQGvD5]cmTu85H(syvsg");
//		httpPostRequest.setEntity(new StringEntity(postBodyJSON));
//
//	    HttpResponse response = httpClient.execute(httpPostRequest);
//	    String result = EntityUtils.toString(response.getEntity());
//	    System.out.println("POST RESULT "+result);
//	    return result;
//	}

	public String encodeString(String string) throws Exception{
		return URLEncoder.encode(string, "UTF-8");
	}
	
	//returns an encoded object of the current date-time
	public String now() throws Exception{
		return encodeString(app.dateTimeFormat.format(app.calendar.getTime()));
	}
	
	public String dJsonizeDate(String d){
		int idx1 = d.indexOf("(");
	    int idx2 = d.indexOf(")") - 5;
	    String s = d.substring(idx1+1, idx2);

	    return app.dateFormatDefault.format(new Date(Long.valueOf(s)));
	}
	
	public String jsonizeDate(String dateStringWithDateFormatDefault) throws Exception{
//		long epoch = app.dateFormatDefault.parse(dateStringWithDateFormatDefault).getTime()/1000;
//		return "/Date("+epoch+"+0000)/";		
		return "/Date("+app.dateFormatDefault.parse(dateStringWithDateFormatDefault).getTime()+"+0000)/";		
	}
		
	public String jsonizeDate(Date date) throws Exception{
//		long epoch = date.getTime()/1000;
//		return "/Date("+epoch+"+0000)/";		
		return "/Date("+date.getTime()+"+0000)/";		
	}
	
	public Object getReferrers() throws Exception{
		JSONArray result = new JSONObject(webRequest.makeWebServiceCall(apiURLUserInfoReferences+"ReferrerGetList", WebRequest.GET, null)).getJSONArray("ReferrerGetListResult");
		LinkedHashMap<Integer, String> refs = new LinkedHashMap<Integer, String>();
		for(int i=0; i<result.length(); i++){
			JSONObject jsonRef = result.getJSONObject(i);
			refs.put(jsonRef.getInt("ReferrerID"), jsonRef.getString("Description"));
		}
		
		return refs;
	}
	
	public Object getLocationSuggestions(String searchStr) throws Exception{
		JSONArray result = new JSONObject(webRequest.makeWebServiceCall(apiURLLocation+"Search?s="+searchStr.toLowerCase()+"&n="+50,WebRequest.GET,null)).getJSONArray("SearchJSONResult");
		ArrayList<String> locations = new ArrayList<String>();
		for(int i=0; i<result.length(); i++)
			locations.add(result.getJSONObject(i).getString("LocationFull"));
		
		return locations;
	}

	public Object getAdvanceSearchResults(String name, String type, String location, String radius, String jobType, String countryID, String postedWithin) throws Exception{
		StringBuilder sb = new StringBuilder(apiURLVacancy+"Search");
		sb.append("?search="+name);
		sb.append("&loc="+URLEncoder.encode(location, "UTF-8"));
		sb.append("&searchIn="+type);
		sb.append("&radius="+radius);
		sb.append("&vacType="+type);
		sb.append("&countryID="+countryID);
		sb.append("&days="+postedWithin);
		sb.append("&top=0");
		JSONArray result = new JSONObject(webRequest.makeWebServiceCall(sb.toString(), WebRequest.GET, null)).getJSONArray("SearchJSONResult");
		ArrayList<JobSummary> jobSummaries = new ArrayList<JobSummary>();
		for(int i=0; i<result.length(); i++)
			jobSummaries.add(new JobSummary(result.getJSONObject(i), this));
		
		return jobSummaries;
	}

	
	//PUBLIC METHODS
	public Object login(String username, String password) throws Exception{
		String result = webRequest.makeWebServiceCall(apiURLCandidates+"CheckAuthentication?e="+username+"&k="+password,WebRequest.GET,null);
				//httpGet(apiURLCandidates+"CheckAuthentication?e="+username+"&k="+password);
		if(result.contains("CheckAuthenticationResult\":null"))
			return null;
		else
			return new JSONObject(result).getJSONObject("CheckAuthenticationResult");
	}
	
	public Object getCandidateData() throws Exception{
//		return new JSONObject(httpGet(apiURLCandidates+"GetByID?id="+app.offlineGateway.getUserID())).getJSONObject("GetByIDResult");
		return  new JSONObject(webRequest.makeWebServiceCall(apiURLCandidates+"GetByID?id="+app.offlineGateway.getUserID(), WebRequest.GET, null)).getJSONObject("GetByIDResult");
	}
	
	public ArrayList<JobLatest> getLatestVacancies(String text, String searchType, String loc, String radius, String jobType, String country, String days) throws Exception{
		ArrayList<JobLatest> jobs = new ArrayList<JobLatest>();
//		JSONArray jsonJobs = new JSONObject(httpGet(apiURLVacancy+"Search?search="+text+"&loc="+loc+"&searchIn="+searchType+"&radius="+radius+"&vacType="+jobType+"&countryID="+country+"&days="+days+"&top=0")).getJSONArray("SearchJSONResult");
		JSONArray jsonJobs = new JSONObject(webRequest.makeWebServiceCall(apiURLVacancy+"Search?search="+text+"&loc="+loc+"&searchIn="+searchType+"&radius="+radius+"&vacType="+jobType+"&countryID="+country+"&days="+days+"&top=0", WebRequest.GET, null)).getJSONArray("SearchJSONResult");
		for(int i=0; i<jsonJobs.length(); i++)
			jobs.add(new JobLatest(jsonJobs.getJSONObject(i), this));
		
		return jobs;
	}
	
	public JobDetail getJobDetailByID(String jobID) throws Exception{
		return new JobDetail(toMap(new JSONObject(webRequest.makeWebServiceCall(apiURLVacancy+"GetByID?id="+jobID, WebRequest.GET, null)).getJSONObject("GetByID_JSONResult")));
	}
	
	public ArrayList<Document> getDocuments() throws Exception{
		ArrayList<Document> docs = new ArrayList<Document>();
		JSONArray jsonDocs = new JSONObject(webRequest.makeWebServiceCall(apiURLDocuments+"GetByCandidateID?id="+app.offlineGateway.getUserID(), WebRequest.GET, null)).getJSONArray("GetByCandidateIDResult");
		for(int i=0; i<jsonDocs.length(); i++){
			JSONObject jsonDoc = jsonDocs.getJSONObject(i);
			if(!jsonDoc.getBoolean("Archive"))
				docs.add(new Document(jsonDoc, this));
		}
		return docs;
	}

	public ArrayList<Employment> getEmployments() throws Exception{
		ArrayList<Employment> emps = new ArrayList<Employment>();
		JSONArray jsonEmps = new JSONObject(webRequest.makeWebServiceCall(apiURLEmployments+"GetByCandidateID?id="+app.offlineGateway.getUserID(), WebRequest.GET, null)).getJSONArray("GetByCandidateIDResult");
		for(int i=0; i<jsonEmps.length(); i++)
			emps.add(new Employment(jsonEmps.getJSONObject(i)));
		
		return emps;
	}

	public ArrayList<Application> getApplications() throws Exception{
		ArrayList<Application> apps = new ArrayList<Application>();
		JSONArray jsonApps = new JSONObject(webRequest.makeWebServiceCall(apiURLApplications+"GetByCandidateID?id="+app.offlineGateway.getUserID(), WebRequest.GET, null)).getJSONArray("GetByCandidateIDResult");
		for(int i=0; i<jsonApps.length(); i++)
			apps.add(new Application(jsonApps.getJSONObject(i)));
		
		return apps;
	}
	
	public ArrayList<SavedSearch> getSavedSearches() throws Exception{
		ArrayList<SavedSearch> ss = new ArrayList<SavedSearch>();
		JSONArray jsonSS = new JSONObject(webRequest.makeWebServiceCall(apiURLSavedSearches+"GetByCandidateID?id="+app.offlineGateway.getUserID(), WebRequest.GET, null)).getJSONArray("GetByCandidateIDResult");
		for(int i=0; i<jsonSS.length(); i++)
			ss.add(new SavedSearch(jsonSS.getJSONObject(i)));
		
		return ss;
	}
	
	public String changeAllSavedSearchStatusByID(int candidateID, boolean willAlert) throws Exception{
		return new JSONObject(webRequest.makeWebServiceCall(apiURLSavedSearches+"ChangeStatusByCandidateID?id="+candidateID+"&enabled="+willAlert, WebRequest.GET, null)).getString("ChangeStatusByCandidateIDResult");
	}
	
	public String deleteSavedSearchWithJBEID(int jbeID) throws Exception{
		return new JSONObject(webRequest.makeWebServiceCall(apiURLSavedSearches+"Delete?id="+jbeID, WebRequest.GET, null)).getString("DeleteResult");
	}
	
	public String deleteEmployment(int employmentID) throws Exception{
		return new JSONObject(webRequest.makeWebServiceCall(apiURLEmployments+"Delete?id="+employmentID, WebRequest.GET, null)).getString("DeleteResult");
	}
	
	public String deleteDocument(int documentID) throws Exception{
		return new JSONObject(webRequest.makeWebServiceCall(apiURLDocuments+"Delete?id="+documentID, WebRequest.GET, null)).getString("DeleteResult");
	}
	
	public String saveSearch(String jsonContents) throws Exception{
		String body = new StringBuilder("{\"j\":").append(jsonContents).append("}").toString();
		return (new JSONObject(webRequest.makeWebServiceCall(apiURLSavedSearches+"Save", WebRequest.POST, body)).getString("SaveResult")==null)?"Failed":null;
	}

	public String saveEmployment(String jsonContents) throws Exception{
		String body = new StringBuilder("{\"j\":").append(jsonContents).append("}").toString();
		return (new JSONObject(webRequest.makeWebServiceCall(apiURLEmployments+"Save",WebRequest.POST,body)).getString("SaveResult")==null)?"Failed":null;
	}
	
	public String saveCandidateDetail(String json) throws Exception{
		int result = new JSONObject(webRequest.makeWebServiceCall(apiURLCandidates+"Save", WebRequest.POST,"{\"c\":"+json+"}")).getInt("SaveResult");
		
		return (result>0)?null:"Unknown Error";
	}
	
	public String applyJob(String jobJSON) throws Exception{
		String body = new StringBuilder("{\"d\":").append(jobJSON).append("}").toString();
		return new JSONObject(webRequest.makeWebServiceCall(apiURLApplications+"Save", WebRequest.POST, body)).getString("SaveResult");
	}

	public String changePassword(String oldPassword, String newPassword) throws Exception{
		return new JSONObject(webRequest.makeWebServiceCall(apiURLCandidates+"ChangePassword?id="+app.offlineGateway.getUserID()+"&o="+oldPassword+"&p="+newPassword, WebRequest.GET, null)).getString("ChangePasswordResult");
	}

	public String closeAccount() throws Exception{
		return new JSONObject(webRequest.makeWebServiceCall(apiURLCandidates+"CloseByID?id="+app.offlineGateway.getUserID(), WebRequest.GET, null)).getString("CloseByIDResult");
	}
	
	public String resetPassword(String email) throws Exception{
		return new JSONObject(webRequest.makeWebServiceCall(apiURLCandidates+"ResetPassword?e="+email, WebRequest.GET, null)).getString("ResetPasswordResult");
	}
	
	public static HashMap<String, Object> toMap(JSONObject object) throws JSONException {
	    HashMap<String, Object> map = new HashMap<String, Object>();

	    Iterator<String> keysItr = object.keys();
	    while(keysItr.hasNext()) {
	        String key = keysItr.next();
	        Object value = object.get(key);

	        if(value instanceof JSONArray) {
	            value = toList((JSONArray) value);
	        }

	        else if(value instanceof JSONObject) {
	            value = toMap((JSONObject) value);
	        }
	        map.put(key, value);
	    }
	    return map;
	}

	public static List toList(JSONArray array) throws JSONException {
	    List<Object> list = new ArrayList<Object>();
	    for(int i = 0; i < array.length(); i++) {
	        Object value = array.get(i);
	        if(value instanceof JSONArray) {
	            value = toList((JSONArray) value);
	        }

	        else if(value instanceof JSONObject) {
	            value = toMap((JSONObject) value);
	        }
	        list.add(value);
	    }
	    return list;
	}	
}
