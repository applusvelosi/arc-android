package applusvelosi.projects.android.arc.views.fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import applusvelosi.projects.android.arc.ArcApplication;
import applusvelosi.projects.android.arc.views.SidebarActivity;

public abstract class ActionbarFragment extends Fragment implements OnClickListener{
	protected SidebarActivity activity;
	public ArcApplication app;
	protected abstract RelativeLayout setupActionbar();
	protected abstract View createView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState);
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		super.onCreateView(inflater, container, savedInstanceState);
		activity = (SidebarActivity)getActivity();
		app = (ArcApplication)activity.getApplication();
		
		RelativeLayout actionbar = setupActionbar();
		if(actionbar!=null)
			activity.setupActionbar(actionbar);
		return createView(inflater, container, savedInstanceState);
	}
}
