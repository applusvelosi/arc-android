package applusvelosi.projects.android.arc.views.fragments.profile;

import java.util.ArrayList;

import org.json.JSONObject;

import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnFocusChangeListener;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.Spinner;
import android.widget.TableRow;
import android.widget.TextView;
import applusvelosi.projects.android.arc.R;
import applusvelosi.projects.android.arc.adapters.spinners.SimpleSpinnerAdapter;
import applusvelosi.projects.android.arc.models.User;
import applusvelosi.projects.android.arc.utils.SaltDatePicker;
import applusvelosi.projects.android.arc.utils.SaltProgressDialog;
import applusvelosi.projects.android.arc.utils.interfaces.RootFragment;
import applusvelosi.projects.android.arc.views.fragments.ActionbarFragment;
import applusvelosi.projects.android.arc.views.fragments.SingleCountrySelectionFragment;
import applusvelosi.projects.android.arc.views.fragments.SingleCountrySelectionFragment.SingleCountrySelectionInterface;

public class UserDetailFragment extends ActionbarFragment implements RootFragment, SingleCountrySelectionInterface, OnFocusChangeListener{
	private static UserDetailFragment instance;

	private View actionbarButtonRefresh, actionbarButtonSave, actionbarButtonMenu;
	private SaltProgressDialog pd;
	private User user;
	
	private TextView tvPercentCompleted, tvCountry, tvLocPrefsCnt, tvLangaugesCnt;
	private EditText etFirstName, etLastName, etEmail, etAltEmail, etPhone, etAltPhone, etMobile, etBday,
					 etAddress, etCity, etState, etPostcode, etLinkedIn, etTwitter, etSkype,
					 etUniversity, etSubject,
					 etAvailableFrom,
					 etPreferredJobs,
					 etSalaryFrom, etSalaryTo,
					 etMainSkills;
	private CheckBox cbEUAuthorized, cbAllowAlerts, cbIsPermanent, cbIsTemporary, cbIsContract, cbIsPartTime;
	private Spinner  spMarital, spGender, spYearGradauted, spEducation, spLicense, spNationality, spEthnicity, spReferrer, 
					 spRelocate, spNoticePeriod, spCurrency, spSalaryType;
		
	private static int scrollTo = -1;
	
	public static UserDetailFragment getInstance(){
		if(instance == null)
			instance = new UserDetailFragment();
		
		return instance;
	}
	
	public static void destroyInstance(){
		instance = null;
	}	
	
	@Override
	protected RelativeLayout setupActionbar() {
		RelativeLayout actionbar = (RelativeLayout)LayoutInflater.from(activity).inflate(R.layout.actionbar_userdetail, null);
		actionbarButtonMenu = actionbar.findViewById(R.id.buttons_actionbar_menu);
		actionbarButtonRefresh = actionbar.findViewById(R.id.buttons_actionbar_refresh);
		actionbarButtonSave = actionbar.findViewById(R.id.buttons_actionbar_done);
		((TextView)actionbar.findViewById(R.id.tviews_actionbar_title)).setText("Details");
		
		actionbarButtonMenu.setOnClickListener(this);
		actionbarButtonRefresh.setOnClickListener(this);
		actionbarButtonSave.setOnClickListener(this);
		return actionbar;
	}

	@Override
	protected View createView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		final View v = inflater.inflate(R.layout.fragment_profile_detail, null);
		tvPercentCompleted = (TextView)v.findViewById(R.id.tviews_profile_detail_percentcomplete);
		
		etFirstName = (EditText)v.findViewById(R.id.etexts_profile_detail_fname);
		etLastName = (EditText)v.findViewById(R.id.etexts_profile_detail_lname);
		etEmail = (EditText)v.findViewById(R.id.etexts_profile_detail_email);
		etAltEmail = (EditText)v.findViewById(R.id.etexts_profile_detail_altemail);
		spGender = (Spinner)v.findViewById(R.id.spinners_profile_detail_gender);
		spGender.setAdapter(new SimpleSpinnerAdapter(activity, app.userChoices.getGenderList()));
		etPhone = (EditText)v.findViewById(R.id.etexts_profile_detail_phone);
		etAltPhone = (EditText)v.findViewById(R.id.etexts_profile_detail_altphone);
		etMobile = (EditText)v.findViewById(R.id.etexts_profile_detail_mobile);
		etBday = (EditText)v.findViewById(R.id.etexts_profile_detail_bday);

		etAddress = (EditText)v.findViewById(R.id.etexts_profile_detail_address);
		etCity = (EditText)v.findViewById(R.id.etexts_profile_detail_city);
		etState = (EditText)v.findViewById(R.id.etexts_profile_detail_state);
		etPostcode = (EditText)v.findViewById(R.id.etexts_profile_detail_postcode);
		tvCountry = (TextView)v.findViewById(R.id.tviews_profile_detail_countrycnt);
		etLinkedIn = (EditText)v.findViewById(R.id.etexts_profile_detail_linkedin);
		etTwitter = (EditText)v.findViewById(R.id.etexts_profile_detail_twitter);
		etSkype = (EditText)v.findViewById(R.id.etexts_profile_detail_skype);
		cbEUAuthorized = (CheckBox)v.findViewById(R.id.cboxs_profile_detail_euauthorized);
		
		etUniversity = (EditText)v.findViewById(R.id.etexts_profile_detail_university);
		etSubject = (EditText)v.findViewById(R.id.etexts_profile_detail_subject);
		spYearGradauted = (Spinner)v.findViewById(R.id.spinners_profile_detail_yeargraduated);
		spYearGradauted.setAdapter(new SimpleSpinnerAdapter(activity, app.userChoices.getYearGraduatedList()));
		spEducation = (Spinner)v.findViewById(R.id.spinners_profile_detail_education);
		spEducation.setAdapter(new SimpleSpinnerAdapter(activity, app.userChoices.getEducationList()));

		spLicense = (Spinner)v.findViewById(R.id.spinners_profile_detail_drivinglicense);
		spLicense.setAdapter(new SimpleSpinnerAdapter(activity, app.userChoices.getLicense()));
		spNationality = (Spinner)v.findViewById(R.id.spinners_profile_detail_nationality);
		spNationality.setAdapter(new SimpleSpinnerAdapter(activity, app.userChoices.getNationalities()));
		spEthnicity = (Spinner)v.findViewById(R.id.spinners_profile_detail_ethnicity);
		spEthnicity.setAdapter(new SimpleSpinnerAdapter(activity, app.userChoices.getEthnicities()));
		spReferrer = (Spinner)v.findViewById(R.id.spinners_profile_detail_referrer);
		spReferrer.setAdapter(new SimpleSpinnerAdapter(activity, new ArrayList<String>(app.userChoices.getReferrers().values())));
		spMarital = (Spinner)v.findViewById(R.id.spinners_profile_detail_maritalstatus);
		spMarital.setAdapter(new SimpleSpinnerAdapter(activity, app.userChoices.getMaritalStatuses()));
		tvLocPrefsCnt = (TextView)v.findViewById(R.id.tviews_profile_detail_locprefscnt);
		spRelocate = (Spinner)v.findViewById(R.id.spinners_profile_detail_relocate);
		spRelocate.setAdapter(new SimpleSpinnerAdapter(activity, app.userChoices.getRelocateList()));
		spNoticePeriod = (Spinner)v.findViewById(R.id.spinners_profile_detail_noticeperiod);
		spNoticePeriod.setAdapter(new SimpleSpinnerAdapter(activity, app.userChoices.getAvailabilityList()));
		
		etAvailableFrom = (EditText)v.findViewById(R.id.etexts_profile_detail_availablefrom);
		tvLangaugesCnt = (TextView)v.findViewById(R.id.tviews_profile_detail_languagescnt);
		
		cbAllowAlerts = (CheckBox)v.findViewById(R.id.cboxs_profile_detail_emailpref);
		
		cbIsPermanent = (CheckBox)v.findViewById(R.id.cboxs_profile_detail_permanent);
		cbIsTemporary = (CheckBox)v.findViewById(R.id.cboxs_profile_detail_temporary);
		cbIsContract = (CheckBox)v.findViewById(R.id.cboxs_profile_detail_contract);
		cbIsPartTime = (CheckBox)v.findViewById(R.id.cboxs_profile_detail_parttime);
		
		etPreferredJobs = (EditText)v.findViewById(R.id.etexts_profile_detail_preferredjobtitles);
		
		spCurrency = (Spinner)v.findViewById(R.id.spinners_profile_detail_currency);
		spCurrency.setAdapter(new SimpleSpinnerAdapter(activity, app.userChoices.getCurrencyList()));
		etSalaryFrom = (EditText)v.findViewById(R.id.etexts_profile_detail_salaryfrom);
		etSalaryTo = (EditText)v.findViewById(R.id.etexts_profile_detail_salaryto);
		spSalaryType = (Spinner)v.findViewById(R.id.spinners_profile_detail_salarytype);
		spSalaryType.setAdapter(new SimpleSpinnerAdapter(activity, app.userChoices.listSalaryTypes));
		etMainSkills = (EditText)v.findViewById(R.id.etexts_profile_detail_mainskills);

		if(user == null){
			pd = new SaltProgressDialog(activity);
			pd.show();
			new Thread(new ProfileDetailGetter()).start();
		}else
			populateFields();
		
		tvCountry.setOnClickListener(this);
		tvLocPrefsCnt.setOnClickListener(this);
		tvLangaugesCnt.setOnClickListener(this);
		
		etFirstName.setOnFocusChangeListener(this);
		etLastName.setOnFocusChangeListener(this);
		etEmail.setOnFocusChangeListener(this);
		etAltEmail.setOnFocusChangeListener(this);
		etPhone.setOnFocusChangeListener(this);
		etAltPhone.setOnFocusChangeListener(this);
		etMobile.setOnFocusChangeListener(this);
		etBday.setOnFocusChangeListener(this);
		etAddress.setOnFocusChangeListener(this);
		etCity.setOnFocusChangeListener(this);
		etState.setOnFocusChangeListener(this);
		etPostcode.setOnFocusChangeListener(this);
		etLinkedIn.setOnFocusChangeListener(this);
		etTwitter.setOnFocusChangeListener(this);
		etSkype.setOnFocusChangeListener(this);
		etUniversity.setOnFocusChangeListener(this);
		etSubject.setOnFocusChangeListener(this);
		etAvailableFrom.setOnFocusChangeListener(this);
		etPreferredJobs.setOnFocusChangeListener(this);
		etSalaryFrom.setOnFocusChangeListener(this);
		etSalaryTo.setOnFocusChangeListener(this);
		etMainSkills.setOnFocusChangeListener(this);
		
		v.post(new Runnable() {
			
			@Override
			public void run() {
				if(scrollTo >= 0){
					((ScrollView)v).smoothScrollTo(0, scrollTo);
					scrollTo = -1;
				}
			}
		});
		
		return v;
	}

	@Override
	public void onClick(View v) {
		if(v == actionbarButtonMenu){
			v.setEnabled(false);
			activity.toggleSidebar(v);
		}else if(v == actionbarButtonRefresh){
			pd.show();
			new Thread(new ProfileDetailGetter()).start();			
		}else if(v == actionbarButtonSave){
			pd.show();
			new Thread(new UserDetailSaver()).start();
		}else if(v == tvCountry){
			scrollTo = ((TableRow)tvCountry.getParent().getParent()).getTop()-30;
			activity.changeChildPage(SingleCountrySelectionFragment.getInstance());
			SingleCountrySelectionFragment.getInstance().setSearchCountryInterface(this);
		}else if(v == tvLocPrefsCnt){
			scrollTo = ((TableRow)tvLocPrefsCnt.getParent().getParent()).getTop()-30;
			activity.changeChildPage(PreferredLocationSelectionFragment.getInstance());
		}else if(v == tvLangaugesCnt){
			scrollTo = ((TableRow)tvLangaugesCnt.getParent().getParent()).getTop()-30;
			activity.changeChildPage(LanguagesSelectionFragment.getInstance());
		}
	}
	
	private void populateFields(){
		tvPercentCompleted.setText(user.getPercentComplete()+" %");
		etFirstName.setText(user.getFirstName());
		etLastName.setText(user.getLastname());
		etEmail.setText(user.getEmail());
		spGender.setSelection(app.userChoices.getGenderPosForKey(user.getGenderID()));
		etAltEmail.setText(user.getAltEmail());
		etPhone.setText(user.getPhone());
		etAltPhone.setText(user.getAltPhone());
		etMobile.setText(user.getMobile());
		etBday.setText(user.getBirthdate(app.onlineGateway));
		etAddress.setText(user.getAddress());
		etCity.setText(user.getCity());
		etState.setText(user.getState());
		etPostcode.setText(user.getPostCode());
		tvCountry.setText(user.getCountryName());
		etLinkedIn.setText(user.getLinkedIn());
		etTwitter.setText(user.getTwitter());
		etSkype.setText(user.getSkype());
		cbEUAuthorized.setChecked(user.isEUAuthorized());
		etUniversity.setText(user.getUniversity());
		etSubject.setText(user.getSubject());
		spYearGradauted.setSelection(app.userChoices.getYearGraduatedList().indexOf(user.getYearGraduated()));
		spEducation.setSelection(app.userChoices.getEducPositionWithKey(user.getEducationID()));
		spLicense.setSelection(app.userChoices.getLicensePosForKey(user.getDrivingLicenseID()));
		spNationality.setSelection(app.userChoices.getNationalityPosForKey(user.getNationalityID()));
		spEthnicity.setSelection(app.userChoices.getEthPosForKey(user.getEthnicityID()));
		spReferrer.setSelection(new ArrayList<Integer>(app.userChoices.getReferrers().keySet()).indexOf(user.getReferrerID()));
		spMarital.setSelection(app.userChoices.getMarStatPosForKey(user.getMaritalStatusID()));
		tvLocPrefsCnt.setText(String.valueOf(user.getPreferredLocations().size()));
		spRelocate.setSelection(app.userChoices.getRelocatePosForKey(user.getRelocationID()));
		spNoticePeriod.setSelection(app.userChoices.getAvailPosForKey(user.getNoticePeriodID()));
		etAvailableFrom.setText(user.getAvailableFrom(app.onlineGateway));
		tvLangaugesCnt.setText(String.valueOf(user.getLanguages().size()));
		cbAllowAlerts.setChecked(user.isAlertAllowed());
		cbIsPermanent.setChecked(user.isPermanent());
		cbIsTemporary.setChecked(user.isTemporary());
		cbIsContract.setChecked(user.isContract());
		cbIsPartTime.setChecked(user.isPartTime());
		etPreferredJobs.setText(user.getJobTitles());
		spCurrency.setSelection(app.userChoices.getCurrencyList().indexOf(user.getCurrency()));
		etSalaryFrom.setText(user.getSalaryFrom());
		etSalaryTo.setText(user.getSalaryTo());
		spSalaryType.setSelection(app.userChoices.getSalaryPosForKey(user.getSalaryTypeID()));
		etMainSkills.setText(user.getMainSkills());
	}

	@Override
	public void disableUserInteractionsOnSidebarShown() {
		
	}

	@Override
	public void enableUserInteractionsOnSidebarHidden() {
		
	}
	
	public class ProfileDetailGetter implements Runnable{

		@Override
		public void run() {
			Object tempResult;
			try {
				tempResult = app.onlineGateway.getCandidateData();
			} catch (Exception e) {
				e.printStackTrace();
				tempResult = e.getMessage();
			}
			
			final Object result = tempResult;
			
			new Handler(Looper.getMainLooper()).post(new Runnable() {
				
				@Override
				public void run() {
					pd.dismiss();
					if(result instanceof String){
						app.showMessageDialog(activity, result.toString());
					}else{
						try{
							user = new User(app, (JSONObject)result);
							activity.user = user;
							populateFields();
						}catch(Exception e){
							e.printStackTrace();
							app.showMessageDialog(activity, e.getMessage());
						}		
					}
				}
			});
		}
		
	}

	private class UserDetailSaver implements Runnable{

		@Override
		public void run() {
			String fname = etFirstName.getText().toString();
			String lname = etLastName.getText().toString();
			String email = etEmail.getText().toString();
			String altEmail = etAltEmail.getText().toString();
			int genderID = app.userChoices.getGenderKeyForDesc(spGender.getSelectedItem().toString());
			String gender = spGender.getSelectedItem().toString();
			String phone = etPhone.getText().toString();
			String altPhone = etAltPhone.getText().toString();
			String mobile = etMobile.getText().toString();
			String bday = etBday.getText().toString();
			String address = etAddress.getText().toString();
			String city = etCity.getText().toString();
			String state = etState.getText().toString();
			String postCode = etPostcode.getText().toString();
			String linkedIn = etLinkedIn.getText().toString();
			String twitter = etTwitter.getText().toString();
			String skype = etSkype.getText().toString();
			boolean isEu = cbEUAuthorized.isChecked();
			String university = etUniversity.getText().toString();
			int licenseID = app.userChoices.getLicenseKeyForDesc(spLicense.getSelectedItem().toString());
			String license = spLicense.getSelectedItem().toString();
			String subject = etSubject.getText().toString();
			int yearGraduated = Integer.parseInt(spYearGradauted.getSelectedItem().toString());
			int educationID = app.userChoices.getEducKeyForDesc(spEducation.getSelectedItem().toString());
			String education = spEducation.getSelectedItem().toString();
			int natID = app.userChoices.getNatKeyForDesc(spNationality.getSelectedItem().toString());
			String nationality = spNationality.getSelectedItem().toString();
			int ethnicityID = app.userChoices.getEthnicityKeyForDesc(spEthnicity.getSelectedItem().toString());
			String ethnicity = spEthnicity.getSelectedItem().toString();
			String referrer = spReferrer.getSelectedItem().toString();
			int referrerID = new ArrayList<String>(app.userChoices.getReferrers().values()).indexOf(referrer);
			int marStatID = app.userChoices.getMarStatKeyForDesc(spMarital.getSelectedItem().toString());
			String marStat = spMarital.getSelectedItem().toString();
			int relocateID = app.userChoices.getRelocateKeyForDesc(spRelocate.getSelectedItem().toString());
			String relocate = spRelocate.getSelectedItem().toString();
			int noticePeriodID = app.userChoices.getAvailKeyForDesc(spNoticePeriod.getSelectedItem().toString());
			String noticePeriod = spNoticePeriod.getSelectedItem().toString();
			String availableFrom = etAvailableFrom.getText().toString();
			boolean isAlertAllowed = cbAllowAlerts.isChecked();
			boolean isPermanent = cbIsPermanent.isChecked();
			boolean isTemporary = cbIsTemporary.isChecked();
			boolean isContract = cbIsContract.isChecked();
			boolean isPartTime = cbIsPartTime.isChecked();
			String jobTitles = etPreferredJobs.getText().toString();
			String curThree = app.userChoices.getCurrencyThreeForDesc(spCurrency.getSelectedItem().toString());
			long salaryFrom = Long.parseLong(etSalaryFrom.getText().toString());
			long salaryTo = Long.parseLong(etSalaryTo.getText().toString());
			int salaryTypeID = app.userChoices.getSalaryKeyForDesc(spSalaryType.getSelectedItem().toString());
			String salaryType = spSalaryType.getSelectedItem().toString();
			String mainSkills = etMainSkills.getText().toString();
			
			String tempResult;
			try{				
				tempResult = app.onlineGateway.saveCandidateDetail(
						user.getJSON(	fname, lname, email, altEmail, genderID, gender, 
										phone, altPhone, mobile, bday, address, city, state, postCode, 
										linkedIn, twitter, skype, isEu, university, licenseID, license, 
										subject, yearGraduated, educationID, education, natID, nationality, 
										ethnicityID, ethnicity, referrerID, marStatID, marStat, 
										relocateID, relocate, noticePeriodID, noticePeriod, availableFrom, 
										isAlertAllowed, isPermanent, isTemporary, isContract, isPartTime, 
										jobTitles, curThree, salaryFrom, salaryTo, salaryTypeID, salaryType, mainSkills, app));
			}catch(Exception e){
				e.printStackTrace();
				tempResult = e.getMessage();
			}
			final String result = tempResult;
			new Handler(Looper.getMainLooper()).post(new Runnable() {
				
				@Override
				public void run() {
					pd.dismiss();
					app.showMessageDialog(activity, (result!=null)?result:"Saved Successfully");
				}
			});
		}
		
	}

	@Override
	public void onCountrySelected(int selCountryID, String selCountryName) {
		user.setCountry(selCountryID, selCountryName);
		tvCountry.setText(selCountryName);
	}

	@Override
	public void onFocusChange(View v, boolean hasFocus) {
		((TextView)v).setTextColor(activity.getResources().getColor((hasFocus)?R.color.black:R.color.menu_list_separator));
		
		if(v == etBday && hasFocus){
			new SaltDatePicker(activity, etBday);
		}else if(v == etAvailableFrom && hasFocus)
			new SaltDatePicker(activity, etAvailableFrom);
	}
}
