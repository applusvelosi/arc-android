package applusvelosi.projects.android.arc.views;

import java.util.ArrayList;

import org.json.JSONObject;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.support.v4.app.FragmentActivity;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.Animation.AnimationListener;
import android.view.animation.TranslateAnimation;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.FrameLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.Toast;
import applusvelosi.projects.android.arc.ArcApplication;
import applusvelosi.projects.android.arc.R;
import applusvelosi.projects.android.arc.adapters.lists.GroupedListAdapter;
import applusvelosi.projects.android.arc.models.GroupedListHeader;
import applusvelosi.projects.android.arc.models.GroupedListSidebarItem;
import applusvelosi.projects.android.arc.models.User;
import applusvelosi.projects.android.arc.utils.SaltProgressDialog;
import applusvelosi.projects.android.arc.utils.interfaces.GroupedListItemInterface;
import applusvelosi.projects.android.arc.views.fragments.ActionbarFragment;
import applusvelosi.projects.android.arc.views.fragments.AdvanceSearchFragment;
import applusvelosi.projects.android.arc.views.fragments.HomeFragment;
import applusvelosi.projects.android.arc.views.fragments.LoginFragment;
import applusvelosi.projects.android.arc.views.fragments.SidebarForeFragment;
import applusvelosi.projects.android.arc.views.fragments.links.AboutusFragment;
import applusvelosi.projects.android.arc.views.fragments.links.CVHelpFragment;
import applusvelosi.projects.android.arc.views.fragments.links.CopyrightFragment;
import applusvelosi.projects.android.arc.views.fragments.links.DisclaimerFragment;
import applusvelosi.projects.android.arc.views.fragments.links.NewsFragment;
import applusvelosi.projects.android.arc.views.fragments.links.PrivacyFragment;
import applusvelosi.projects.android.arc.views.fragments.links.TermsFragment;
import applusvelosi.projects.android.arc.views.fragments.profile.UserApplicationFragment;
import applusvelosi.projects.android.arc.views.fragments.profile.UserChangePassFragment;
import applusvelosi.projects.android.arc.views.fragments.profile.UserDetailFragment;
import applusvelosi.projects.android.arc.views.fragments.profile.UserDocumentFragment;
import applusvelosi.projects.android.arc.views.fragments.profile.UserEmploymentFragment;
import applusvelosi.projects.android.arc.views.fragments.profile.UserSaveSearchesFragment;

public class SidebarActivity extends FragmentActivity implements AnimationListener, OnItemClickListener{
	private final String SIDEBAR_HIDDEN = "sidebar_hidden";
	private final String SIDEBAR_SHOWN = "sidebar_shown";
	private final String HOME = "Home";
	private final String LOGIN = "Login";
	private final String SEARCH = "Advance Search";
	private final String PROFILE_DETAILS = "My Details";
	private final String PROFILE_DOCUMENTS = "My Documents";
	private final String PROFILE_EMPLOYMENTS = "My Employments";
	private final String PROFILE_APPLICATIONS = "My Applications";
	private final String PROFILE_SEARCHES = "My Saved Searches";
	private final String PROFILE_CHANGEPASS = "Change Password";
	private final String PROFILE_DELETE = "Delete this Account";
	private final String PROFILE_LOGOUT = "Logout";
	private final String LINK_ABOUTUS = "About Us";
	private final String LINK_CVHELP = "CV Help";
	private final String LINK_NEWS = "News";
	private final String LINK_TERMS = "Terms";
	private final String LINK_PRIVACY = "Privacy";
	private final String LINK_COPYRIGHT = "Copyright";
	private final String LINK_DISCLAIMER = "Disclaimer";
	private final String LINK_SECURITYPOLICY = "Security Policy";
	private final String REPORTBUG = "Report Bug";
	private int maxSidebarShownWidth = 550;
	private final int TOGGLE_SPEED = 300;
	private SidebarForeFragment mainFragment;
	private ListView menuList;
	private GroupedListAdapter adapter;
	private ArrayList<GroupedListItemInterface> sidebarItems;
	private FrameLayout foreFragment;
	private TranslateAnimation animationShowSidebar, animationHideSidebar;
	private ActionbarFragment toBeShownFragment;
	private GroupedListSidebarItem lastMenuItemClicked;
	public User user;
	
	private SaltProgressDialog pd;
	private View menuButton;
		
	@Override
	protected void onCreate(Bundle arg0) {
		super.onCreate(arg0);
		setContentView(R.layout.activity_sidebar);			
		menuList = (ListView)findViewById(R.id.lists_sidebar);
		sidebarItems = new ArrayList<GroupedListItemInterface>();
		adapter = new GroupedListAdapter(this, sidebarItems);
		menuList.setAdapter(adapter);
		foreFragment = (FrameLayout)findViewById(R.id.containers_activities_home_fore);
		setupSidebar();

		mainFragment = SidebarForeFragment.getInstance(this);
		getSupportFragmentManager().beginTransaction().replace(foreFragment.getId(), mainFragment).commit();
		//animations
		animationShowSidebar = new TranslateAnimation(0, maxSidebarShownWidth, 0, 0);
		animationShowSidebar.setDuration(TOGGLE_SPEED);
		animationShowSidebar.setFillEnabled(true);
		animationShowSidebar.setAnimationListener(this);
		animationHideSidebar = new TranslateAnimation(0, -maxSidebarShownWidth, 0, 0);
		animationHideSidebar.setDuration(TOGGLE_SPEED);
		animationHideSidebar.setFillEnabled(true);
		animationHideSidebar.setAnimationListener(this);
		foreFragment.setTag(SIDEBAR_HIDDEN);		
	}
	
	//sidebar
	public void setupSidebar(){
		sidebarItems.clear();
		sidebarItems.add(new GroupedListHeader(" "));
		sidebarItems.add(new GroupedListSidebarItem(HOME, getResources().getDrawable(R.drawable.icon_home),getResources().getDrawable(R.drawable.icon_home_sel)));
		sidebarItems.add(new GroupedListSidebarItem(SEARCH, getResources().getDrawable(R.drawable.icon_advancesearch),getResources().getDrawable(R.drawable.icon_advancesearch_sel)));
		sidebarItems.add(new GroupedListHeader("PROFILE"));
		if(((ArcApplication)getApplication()).offlineGateway.isLoggedIn()){
			sidebarItems.add(new GroupedListSidebarItem(PROFILE_DETAILS, getResources().getDrawable(R.drawable.icon_details),getResources().getDrawable(R.drawable.icon_details_sel)));			
			sidebarItems.add(new GroupedListSidebarItem(PROFILE_DOCUMENTS, getResources().getDrawable(R.drawable.icon_document),getResources().getDrawable(R.drawable.icon_documents_sel)));			
			sidebarItems.add(new GroupedListSidebarItem(PROFILE_EMPLOYMENTS, getResources().getDrawable(R.drawable.icon_employment),getResources().getDrawable(R.drawable.icon_employment_sel)));			
			sidebarItems.add(new GroupedListSidebarItem(PROFILE_APPLICATIONS, getResources().getDrawable(R.drawable.icon_application),getResources().getDrawable(R.drawable.icon_application_sel)));			
			sidebarItems.add(new GroupedListSidebarItem(PROFILE_SEARCHES, getResources().getDrawable(R.drawable.icon_searches),getResources().getDrawable(R.drawable.icon_searches_sel)));			
			sidebarItems.add(new GroupedListSidebarItem(PROFILE_CHANGEPASS, getResources().getDrawable(R.drawable.icon_changepass),getResources().getDrawable(R.drawable.icon_changepass_sel)));			
			sidebarItems.add(new GroupedListSidebarItem(PROFILE_DELETE, getResources().getDrawable(R.drawable.icon_trash),getResources().getDrawable(R.drawable.icon_trash_sel)));			
			sidebarItems.add(new GroupedListSidebarItem(PROFILE_LOGOUT, getResources().getDrawable(R.drawable.icon_logout),getResources().getDrawable(R.drawable.icon_logout_sel)));			
		}else{
			sidebarItems.add(new GroupedListSidebarItem(LOGIN, getResources().getDrawable(R.drawable.icon_login),getResources().getDrawable(R.drawable.icon_login_sel)));			
		}
		sidebarItems.add(new GroupedListHeader("LINKS"));
		sidebarItems.add(new GroupedListSidebarItem(LINK_ABOUTUS, getResources().getDrawable(R.drawable.icon_link),getResources().getDrawable(R.drawable.icon_link_sel)));
		sidebarItems.add(new GroupedListSidebarItem(LINK_CVHELP, getResources().getDrawable(R.drawable.icon_link),getResources().getDrawable(R.drawable.icon_link_sel)));
		sidebarItems.add(new GroupedListSidebarItem(LINK_NEWS, getResources().getDrawable(R.drawable.icon_link),getResources().getDrawable(R.drawable.icon_link_sel)));
		sidebarItems.add(new GroupedListSidebarItem(LINK_TERMS, getResources().getDrawable(R.drawable.icon_link),getResources().getDrawable(R.drawable.icon_link_sel)));
		sidebarItems.add(new GroupedListSidebarItem(LINK_PRIVACY, getResources().getDrawable(R.drawable.icon_link),getResources().getDrawable(R.drawable.icon_link_sel)));
		sidebarItems.add(new GroupedListSidebarItem(LINK_COPYRIGHT, getResources().getDrawable(R.drawable.icon_link),getResources().getDrawable(R.drawable.icon_link_sel)));
		sidebarItems.add(new GroupedListSidebarItem(LINK_DISCLAIMER, getResources().getDrawable(R.drawable.icon_link),getResources().getDrawable(R.drawable.icon_link_sel)));
		sidebarItems.add(new GroupedListHeader(" "));
		sidebarItems.add(new GroupedListSidebarItem(REPORTBUG, getResources().getDrawable(R.drawable.icon_reportbug),getResources().getDrawable(R.drawable.icon_reportbug_sel)));
		sidebarItems.add(new GroupedListHeader(" "));
		adapter.notifyDataSetChanged();
	}
	
	private void hideSidebar(){
		foreFragment.startAnimation(animationHideSidebar);
		mainFragment.currRootFragment.enableUserInteractionsOnSidebarHidden();
		menuList.setOnItemClickListener(null);
	}
	
	public void toggleSidebar(View menuButton){
		this.menuButton = menuButton;
		if(foreFragment.getTag().toString().equals(SIDEBAR_SHOWN)){
			hideSidebar();
		}else{
			foreFragment.startAnimation(animationShowSidebar);
			mainFragment.currRootFragment.disableUserInteractionsOnSidebarShown();
			menuList.setOnItemClickListener(this);
		}
		
	}

//	private void toggleSidebar(){
//		if(foreFragment.getTag().toString().equals(SIDEBAR_SHOWN)){
//			hideSidebar();
//		}else{
//			foreFragment.startAnimation(animationShowSidebar);
//			mainFragment.currRootFragment.disableUserInteractionsOnSidebarShown();
//			menuList.setOnItemClickListener(this);
//		}
//		
//	}
	
	public SidebarForeFragment getMainFragment(){
		return mainFragment;
	}
		
	public void setupActionbar(RelativeLayout actionbarLayout){
		mainFragment.setupActionbar(actionbarLayout);
	}
	
	public void changeChildPage(ActionbarFragment fragment){
		mainFragment.changePage(fragment);
	}
	
	@Override
	public void onBackPressed() {
		if(getSupportFragmentManager().getBackStackEntryCount() < 1){
			new AlertDialog.Builder(this).setMessage("Are you sure you want to exit ARC?")
										 .setPositiveButton("Yes", new OnClickListener() {
											
											@Override
											public void onClick(DialogInterface dialog, int which) {
												dialog.dismiss();
												SidebarActivity.super.onBackPressed();											
											}
										})
										.setNegativeButton("No", new OnClickListener() {
											
											@Override
											public void onClick(DialogInterface dialog, int which) {
												dialog.dismiss();
											}
										}).create().show();
//			toggleSidebar();
		}else
			super.onBackPressed();											

	}
		
	@Override
	public void onAnimationStart(Animation animation) {
		foreFragment.setTag((animation==animationShowSidebar)?SIDEBAR_SHOWN:SIDEBAR_HIDDEN);
	}

	@Override
	public void onAnimationEnd(Animation animation) {
		foreFragment.setX((animation==animationShowSidebar)?maxSidebarShownWidth:0);	
		
		if(animation == animationHideSidebar && toBeShownFragment!=null)
			mainFragment.changePage(toBeShownFragment);
		menuButton.setEnabled(true);
	}

	@Override
	public void onAnimationRepeat(Animation animation) {}

	@Override
	public void onItemClick(AdapterView<?> parent, View view, int pos, long id) {
		GroupedListItemInterface sidebarItem = sidebarItems.get(pos);
		if(sidebarItem instanceof GroupedListSidebarItem){ //prevents actions when clicking sidebar headers
			if(!(((GroupedListSidebarItem) sidebarItem).getLabel().toString().equals(PROFILE_DELETE)||((GroupedListSidebarItem) sidebarItem).getLabel().toString().equals(REPORTBUG))){
				if(lastMenuItemClicked != null)
					lastMenuItemClicked.displayAsNormalItem();
				
				((GroupedListSidebarItem) sidebarItem).displayAsSelectedItem();
				lastMenuItemClicked = (GroupedListSidebarItem)sidebarItem;							
			}
			
			if(((GroupedListSidebarItem) sidebarItem).getLabel().toString().equals(HOME)) toBeShownFragment = HomeFragment.getInstance();
			else if(((GroupedListSidebarItem) sidebarItem).getLabel().toString().equals(SEARCH)) toBeShownFragment = new AdvanceSearchFragment(); 
			else if(((GroupedListSidebarItem) sidebarItem).getLabel().toString().equals(LOGIN)) toBeShownFragment = new LoginFragment();
			else if(((GroupedListSidebarItem) sidebarItem).getLabel().toString().equals(PROFILE_DETAILS)) toBeShownFragment = UserDetailFragment.getInstance();
			else if(((GroupedListSidebarItem) sidebarItem).getLabel().toString().equals(PROFILE_DOCUMENTS)) toBeShownFragment = UserDocumentFragment.getInstance();
			else if(((GroupedListSidebarItem) sidebarItem).getLabel().toString().equals(PROFILE_EMPLOYMENTS)) toBeShownFragment = UserEmploymentFragment.getInstance();
			else if(((GroupedListSidebarItem) sidebarItem).getLabel().toString().equals(PROFILE_APPLICATIONS)) toBeShownFragment = UserApplicationFragment.getInstance();
			else if(((GroupedListSidebarItem) sidebarItem).getLabel().toString().equals(PROFILE_SEARCHES)) toBeShownFragment = UserSaveSearchesFragment.getInstance();
			else if(((GroupedListSidebarItem) sidebarItem).getLabel().toString().equals(PROFILE_CHANGEPASS)) toBeShownFragment = UserChangePassFragment.getInstance();
			else if(((GroupedListSidebarItem) sidebarItem).getLabel().toString().equals(PROFILE_DELETE)) new AccountDeleteDialog(this).create().show();
			else if(((GroupedListSidebarItem) sidebarItem).getLabel().toString().equals(PROFILE_LOGOUT)) logout();
			else if(((GroupedListSidebarItem) sidebarItem).getLabel().toString().equals(LINK_ABOUTUS)) toBeShownFragment = AboutusFragment.getInstance();
			else if(((GroupedListSidebarItem) sidebarItem).getLabel().toString().equals(LINK_CVHELP)) toBeShownFragment = CVHelpFragment.getInstance();
			else if(((GroupedListSidebarItem) sidebarItem).getLabel().toString().equals(LINK_NEWS)) toBeShownFragment = NewsFragment.getInstance();
			else if(((GroupedListSidebarItem) sidebarItem).getLabel().toString().equals(LINK_TERMS)) toBeShownFragment = TermsFragment.getInstance();			
			else if(((GroupedListSidebarItem) sidebarItem).getLabel().toString().equals(LINK_PRIVACY)) toBeShownFragment = PrivacyFragment.getInstance();
			else if(((GroupedListSidebarItem) sidebarItem).getLabel().toString().equals(LINK_COPYRIGHT)) toBeShownFragment = CopyrightFragment.getInstance();
			else if(((GroupedListSidebarItem) sidebarItem).getLabel().toString().equals(LINK_DISCLAIMER)) toBeShownFragment = DisclaimerFragment.getInstance();
			else if(((GroupedListSidebarItem) sidebarItem).getLabel().toString().equals(PROFILE_DELETE)) new AccountDeleteDialog(this).create().show();
			else if(((GroupedListSidebarItem) sidebarItem).getLabel().toString().equals(REPORTBUG)) reportBug();
			
			hideSidebar();
		}
	}	
	
	private class AccountDeleteDialog extends AlertDialog.Builder{

		public AccountDeleteDialog(Context ctx) {
			super(ctx);
			
			setTitle(null);
			setMessage("Are you sure you want to delete this account?");
			setNegativeButton("Delete", new DialogInterface.OnClickListener() {
				
				@Override
				public void onClick(DialogInterface dialog, int which) {
					dialog.dismiss();
					if(pd == null)
						pd = new SaltProgressDialog(SidebarActivity.this);
					pd.show();
					new Thread(new Runnable() {
						
						@Override
						public void run() {
							String tempResult;
							try{
								tempResult = ((ArcApplication)getApplication()).onlineGateway.closeAccount();
							}catch(Exception e){
								e.printStackTrace();
								tempResult = e.getMessage();
							}
							final String result = tempResult;
							
							new Handler(Looper.getMainLooper()).post(new Runnable() {
								
								@Override
								public void run() {
									pd.dismiss();
									if(result.equals("OK")){
										((ArcApplication)getApplication()).showMessageDialog(SidebarActivity.this, "Deleted Successfully!");										
										logout();
									}else
										((ArcApplication)getApplication()).showMessageDialog(SidebarActivity.this, result);
								}
							});
						}
					}).start();
				}
			});
			setPositiveButton("Cancel", new DialogInterface.OnClickListener() {
				
				@Override
				public void onClick(DialogInterface dialog, int which) {
					dialog.dismiss();
				}
			});
		}
	}

	public void login(LoginFragment key, JSONObject jsonUser) throws Exception{
		user = new User((ArcApplication)getApplication(), jsonUser);
		setupSidebar();
		toBeShownFragment = HomeFragment.getInstance();
		changeChildPage(HomeFragment.getInstance());
		
	}
	
	private void logout(){
		((ArcApplication)getApplication()).offlineGateway.logout();
		LoginFragment loginFragment = new LoginFragment();
		toBeShownFragment = loginFragment;
		changeChildPage(loginFragment);
		UserApplicationFragment.destroyInstance();
		UserDetailFragment.destroyInstance();
		UserDocumentFragment.destroyInstance();
		UserEmploymentFragment.destroyInstance();
		UserSaveSearchesFragment.destroyInstance();
		setupSidebar();
	}
	
	private void reportBug(){
		Intent i = new Intent(Intent.ACTION_SEND);
		i.setType("message/rfc822");
		i.putExtra(Intent.EXTRA_EMAIL  , new String[]{"martin.coles@applusvelosi.com"});
		i.putExtra(Intent.EXTRA_SUBJECT, "ARC Android Issue");
		i.putExtra(Intent.EXTRA_TEXT   , "");
		try {
		    startActivity(Intent.createChooser(i, "Report Bug"));
		} catch (android.content.ActivityNotFoundException ex) {
		    Toast.makeText(this, "There are no email clients installed.", Toast.LENGTH_SHORT).show();
		}
	}
}
