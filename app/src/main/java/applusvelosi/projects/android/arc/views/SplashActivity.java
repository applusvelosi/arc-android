package applusvelosi.projects.android.arc.views;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.provider.Settings;

import java.util.LinkedHashMap;

import applusvelosi.projects.android.arc.ArcApplication;
import applusvelosi.projects.android.arc.R;

public class SplashActivity extends Activity {
	private ArcApplication app;
	AlertDialog dialog;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_splash);
		dialog = new AlertDialog.Builder(this).setMessage("Wifi required. Please turn on your Wifi")
				 .setPositiveButton("Turn On", new DialogInterface.OnClickListener() {

					@Override
					public void onClick(DialogInterface dialog, int which) {
						startActivity(new Intent(Settings.ACTION_WIFI_SETTINGS));
						dialog.dismiss();
					}
				})
				.setNeutralButton("Ok", new DialogInterface.OnClickListener() {

					@Override
					public void onClick(DialogInterface dialog, int which) {
						startActivity(new Intent(SplashActivity.this, SplashActivity.class));
						finish();
						dialog.dismiss();
					}
				})
				.setNegativeButton("Quit", new DialogInterface.OnClickListener() {

					@Override
					public void onClick(DialogInterface dialog, int which) {
						dialog.dismiss();
						SplashActivity.this.finish();
					}
				}).create();

		dialog.setOnDismissListener(new DialogInterface.OnDismissListener() {

			@Override
			public void onDismiss(DialogInterface dialog) {
				if(app.isNetworkAvailable())
					dialog.dismiss();
				else
					SplashActivity.this.dialog.show();
			}
		});
		
	    app = (ArcApplication)getApplication();
	    
		new Thread(new Runnable() {
			
			@Override
			public void run() {
				Object tempResult;
				try{
					tempResult = app.onlineGateway.getReferrers();
				}catch(Exception e){
					e.printStackTrace();
					tempResult = e.getMessage();
				}
				final Object result = tempResult;
				new Handler(Looper.getMainLooper()).post(new Runnable() {
					
					@Override
					public void run() {
						if(result instanceof String)
							dialog.show();
						else{
							app.userChoices.setReferrers((LinkedHashMap<Integer, String>)result);
							startActivity(new Intent(SplashActivity.this, SidebarActivity.class));
							finish();
						}
					}
				});				
			}
		}).start();
	}	
}
