package applusvelosi.projects.android.arc.models;

import java.util.HashMap;

import org.json.JSONObject;

import applusvelosi.projects.android.arc.utils.OnlineGateway;

public class Document {

	private HashMap<String, Object> doc;
	private OnlineGateway g;
	
	public Document(JSONObject jsonDocument, OnlineGateway g) throws Exception{
		doc = new HashMap<String, Object>();
		this.g = g;
		if(jsonDocument != JSONObject.NULL) {
			doc = OnlineGateway.toMap(jsonDocument);
	    }
	}
	
	public int getDocumentID(){
		return Integer.parseInt(doc.get("DocID").toString());
	}
	
	public String getName(){
		return doc.get("DocName").toString();
	}
	
	public String getExpireDate(){
		return g.dJsonizeDate(doc.get("DateExpiry").toString());
	}
	
	public String getExtension(){
		return doc.get("Ext").toString();
	}
	
	public float getSize(){
		return Float.parseFloat(doc.get("FileSize").toString())/1024;
	}
	
	public int getType(){
		return Integer.parseInt(doc.get("DocType").toString());
	}
}
