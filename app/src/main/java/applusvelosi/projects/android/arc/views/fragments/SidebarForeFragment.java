
package applusvelosi.projects.android.arc.views.fragments;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import applusvelosi.projects.android.arc.ArcApplication;
import applusvelosi.projects.android.arc.R;
import applusvelosi.projects.android.arc.utils.interfaces.RootFragment;
import applusvelosi.projects.android.arc.views.SidebarActivity;
import applusvelosi.projects.android.arc.views.fragments.profile.UserDetailFragment;

public class SidebarForeFragment extends Fragment{
	private static SidebarForeFragment instance;
	private SidebarActivity activity;
	private View view; //global visibility to avoid NPE on resume for setupActionbar
	private RelativeLayout actionbar;
	public RootFragment currRootFragment;
	
	public static SidebarForeFragment getInstance(SidebarActivity key){
		if(instance == null)
			instance = new SidebarForeFragment();
		
		return instance;
	}
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		activity = (SidebarActivity)getActivity();
		view = inflater.inflate(R.layout.fragment_sidebar_fore, null);
		
		actionbar = (RelativeLayout)view.findViewById(R.id.actionbar_top);		
		changePage((((ArcApplication)getActivity().getApplication()).offlineGateway.isLoggedIn())?HomeFragment.getInstance():new LoginFragment());
		return view;
	}
	
	public void changePage(Fragment fragment){	
		if(fragment instanceof RootFragment){
			currRootFragment = (RootFragment) fragment;
			for(int i=activity.getSupportFragmentManager().getBackStackEntryCount(); i>0; i--)
				activity.getSupportFragmentManager().popBackStack();

			activity.getSupportFragmentManager().beginTransaction().replace(R.id.activity_view, fragment).commit();		
		}else	
			activity.getSupportFragmentManager().beginTransaction().addToBackStack("").replace(R.id.activity_view, fragment).commit();		
	}
	
	public void setupActionbar(RelativeLayout actionbarContentViews){	
		if(actionbar!=null && actionbarContentViews!=null){
			actionbar.removeAllViews();
			actionbar.addView(actionbarContentViews);									
		}else{
			activity.startActivity(new Intent(activity, SidebarActivity.class));
			activity.finish();
		}
	}
}
