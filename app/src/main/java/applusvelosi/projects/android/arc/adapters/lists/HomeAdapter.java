package applusvelosi.projects.android.arc.adapters.lists;

import java.util.ArrayList;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;
import applusvelosi.projects.android.arc.R;
import applusvelosi.projects.android.arc.models.JobLatest;
import applusvelosi.projects.android.arc.views.SidebarActivity;

public class HomeAdapter extends BaseAdapter{
	private ArrayList<JobLatest> jobs;
	private SidebarActivity activity;
	
	public HomeAdapter(SidebarActivity activity, ArrayList<JobLatest> jobs){
		this.activity = activity;
		this.jobs = jobs;
	}
	
	@Override
	public View getView(int pos, View v, ViewGroup parent) {
		Holder holder;
		if(v == null){
			holder = new Holder();
			v = LayoutInflater.from(activity).inflate(R.layout.cell_home, null);
			holder.tvTitle = (TextView)v.findViewById(R.id.tviews_home_cells_title);
			holder.tvLocation = (TextView)v.findViewById(R.id.tviews_home_cells_location);
			holder.tvDuration = (TextView)v.findViewById(R.id.tviews_home_cells_duration);
			holder.tvSalary = (TextView)v.findViewById(R.id.tviews_home_cells_salary);
			v.setTag(holder);
		}
		
		holder = (Holder)v.getTag();
		JobLatest job = jobs.get(pos);
		holder.tvTitle.setText(job.getTitle());
		holder.tvLocation.setText(job.getLocation());
		holder.tvDuration.setText(job.getDuration());
		holder.tvSalary.setText(job.getSalary());
		
		return v;
	}

	@Override
	public int getCount() {
		return jobs.size();
	}

	@Override
	public Object getItem(int pos) {
		return jobs.get(pos);
	}

	@Override
	public long getItemId(int pos) {
		return 0;
	}
	
	private class Holder{
		public TextView tvTitle, tvLocation, tvDuration, tvSalary;
	}
}
