package applusvelosi.projects.android.arc.utils;

import android.app.ProgressDialog;
import android.content.Context;
import applusvelosi.projects.android.arc.ArcApplication;

public class SaltProgressDialog extends ProgressDialog{
	
	public SaltProgressDialog(Context context) {
		super(context);

		super.setMessage("Loading");
		super.setCancelable(false);
		super.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
		super.setIndeterminate(true);
	}
		
	@Override
	public void show() {
		if(!super.isShowing())
			super.show();
	}
	
}
