package applusvelosi.projects.android.arc.utils.interfaces;

import android.app.Activity;
import android.view.View;

public interface GroupedListItemInterface {
	
	public View getTextView(Activity activity);

}
