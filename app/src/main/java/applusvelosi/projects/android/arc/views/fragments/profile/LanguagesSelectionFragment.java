package applusvelosi.projects.android.arc.views.fragments.profile;

import java.util.ArrayList;
import java.util.LinkedHashMap;

import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.AdapterView.OnItemClickListener;
import applusvelosi.projects.android.arc.ArcApplication;
import applusvelosi.projects.android.arc.R;
import applusvelosi.projects.android.arc.adapters.lists.MultipleLabelCheckboxSelectionAdapter;
import applusvelosi.projects.android.arc.adapters.lists.MultipleLabelCheckboxSelectionAdapter.MultiSelectionLabelCheckboxSelectionAdapterHolder;
import applusvelosi.projects.android.arc.adapters.lists.HomeAdapter;
import applusvelosi.projects.android.arc.adapters.lists.MultipleLabelCheckboxSelectionAdapter.MultipleLabelCheckboxSelector;
import applusvelosi.projects.android.arc.models.JobLatest;
import applusvelosi.projects.android.arc.utils.SaltProgressDialog;
import applusvelosi.projects.android.arc.utils.interfaces.RootFragment;
import applusvelosi.projects.android.arc.views.fragments.ActionbarFragment;

public class LanguagesSelectionFragment extends ActionbarFragment implements OnCheckedChangeListener, OnItemClickListener, MultipleLabelCheckboxSelector{
	private static LanguagesSelectionFragment instance;
	private View actionbarBackButton, actionbarDoneButton;
	private TextView actionbarTitle;
	
	private ListView lv;
	private ArrayList<String> languages, selecteds;
	private MultipleLabelCheckboxSelectionAdapter adapter;
	
	public static LanguagesSelectionFragment getInstance(){
		if(instance == null)
			instance = new LanguagesSelectionFragment();
		
		return instance;
	}
	
	@Override
	protected RelativeLayout setupActionbar() {
		RelativeLayout actionbarLayout = (RelativeLayout)LayoutInflater.from(activity).inflate(R.layout.actionbar_backdone, null);
		actionbarBackButton = actionbarLayout.findViewById(R.id.buttons_actionbar_back);
		actionbarDoneButton = actionbarLayout.findViewById(R.id.buttons_actionbar_done);
		actionbarTitle = (TextView)actionbarLayout.findViewById(R.id.tviews_actionbar_title);
		actionbarTitle.setText("Select Languages");

		actionbarTitle.setOnClickListener(this);
		actionbarBackButton.setOnClickListener(this);
		actionbarDoneButton.setVisibility(View.GONE);
		
		return actionbarLayout;
	}

	@Override
	protected View createView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		View v = inflater.inflate(R.layout.fragment_lv, null);
		lv = (ListView)v.findViewById(R.id.lists_lv);		
		languages = app.userChoices.getLanguages();
		selecteds = activity.user.getLanguages();
		
		adapter = new MultipleLabelCheckboxSelectionAdapter(this, languages, selecteds);
		lv.setAdapter(adapter);
		lv.setOnItemClickListener(this);
		return v;
	}

	@Override
	public void onClick(View v) {
		if(v == actionbarBackButton || v == actionbarTitle){
			activity.onBackPressed();
		}
	}
	
	@Override
	public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
		String selCountry = ((MultiSelectionLabelCheckboxSelectionAdapterHolder)((RelativeLayout)buttonView.getParent()).getTag()).tv.getText().toString();

		if(selecteds.contains(selCountry))
			selecteds.remove(selCountry);
		else
			selecteds.add(selCountry);
		
		activity.user.updatePreferredLanguages(selecteds);
	}

	@Override
	public void onItemClick(AdapterView<?> parent, View v, int pos, long id) {
		MultiSelectionLabelCheckboxSelectionAdapterHolder holder = (MultiSelectionLabelCheckboxSelectionAdapterHolder)v.getTag();
		holder.cb.setChecked(!(selecteds.contains(holder.tv.getText().toString())));		
	}

	@Override
	public OnCheckedChangeListener getCheckChangeListener() {
		return this;
	}

}
