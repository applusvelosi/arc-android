package applusvelosi.projects.android.arc.views.fragments.profile;

import java.util.Set;

import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnFocusChangeListener;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TableRow;
import android.widget.TextView;
import applusvelosi.projects.android.arc.R;
import applusvelosi.projects.android.arc.adapters.spinners.SimpleSpinnerAdapter;
import applusvelosi.projects.android.arc.models.SavedSearch;
import applusvelosi.projects.android.arc.utils.SaltProgressDialog;
import applusvelosi.projects.android.arc.utils.enums.JobTypes;
import applusvelosi.projects.android.arc.utils.enums.PostedDays;
import applusvelosi.projects.android.arc.utils.enums.SearchTypes;
import applusvelosi.projects.android.arc.views.fragments.ActionbarFragment;
import applusvelosi.projects.android.arc.views.fragments.JobSummaryFragment;
import applusvelosi.projects.android.arc.views.fragments.SearchLocationFragment;
import applusvelosi.projects.android.arc.views.fragments.SearchLocationFragment.SearchLocationInterface;
import applusvelosi.projects.android.arc.views.fragments.SingleCountrySelectionFragment;
import applusvelosi.projects.android.arc.views.fragments.SingleCountrySelectionFragment.SingleCountrySelectionInterface;

public class UserSavedSearchDetailFragment extends ActionbarFragment implements SingleCountrySelectionInterface, SearchLocationInterface, OnFocusChangeListener{
	private static final String KEY_SS_POS = "userapplicationtdetailskeyemploymentposition";
	
	private View actionbarButtonBack, actionbarButtonSearch;
	private TextView actionbarTitle, actionbarButtonSave;
	
	private EditText etSearchName, etSearchFor, etDistance;
	private TextView tvSelectedCountryName, tvSelectedLocation;
	private TableRow trLocation, trCountryRegion;
	private Spinner spSearchTypes, spJobType, spPostedWithin;
	
	private static int selCountryID = -1;
	private static String selLocation;
	private static String selCountryName;
	private SavedSearch ss;
	private SaltProgressDialog pd;
	
	public static UserSavedSearchDetailFragment newInstance(int pos){
		UserSavedSearchDetailFragment frag = new UserSavedSearchDetailFragment();
		Bundle b = new Bundle();
		b.putInt(KEY_SS_POS, pos);
		
		frag.setArguments(b);
		return frag;
	}
	
	@Override
	protected RelativeLayout setupActionbar() {
		RelativeLayout actionbar = (RelativeLayout)LayoutInflater.from(activity).inflate(R.layout.actionbar_savedsearchesdetail, null);
		actionbarButtonBack = actionbar.findViewById(R.id.buttons_actionbar_back);
		actionbarButtonSave = (TextView)actionbar.findViewById(R.id.buttons_actionbar_done);
		actionbarButtonSearch = actionbar.findViewById(R.id.buttons_actionbar_search);
		actionbarTitle = (TextView)actionbar.findViewById(R.id.tviews_actionbar_title);
		actionbarTitle.setText("Saved Search Detail");
		
		actionbarTitle.setOnClickListener(this);
		actionbarButtonBack.setOnClickListener(this);
		actionbarButtonSearch.setOnClickListener(this);
		actionbarButtonSave.setOnClickListener(this);
		return actionbar;
	}

	@Override
	protected View createView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		View v = inflater.inflate(R.layout.fragment_profile_savesearchdetail, null);
		pd = new SaltProgressDialog(activity);
		
		etSearchName = (EditText)v.findViewById(R.id.etexts_savesearchdetail_name);
		etSearchFor = (EditText)v.findViewById(R.id.etexts_savesearchdetail_for);
		spSearchTypes = (Spinner)v.findViewById(R.id.spinners_savesearchdetail_in);
		trLocation = (TableRow)v.findViewById(R.id.trs_savesearchdetail_location);
		tvSelectedLocation = (TextView)v.findViewById(R.id.tviews_savesearchdetail_location);
		trCountryRegion = (TableRow)v.findViewById(R.id.trs_savesearchdetail_countryregion);
		tvSelectedCountryName = (TextView)v.findViewById(R.id.tviews_savesearchdetail_countryregion);
		etDistance = (EditText)v.findViewById(R.id.etexts_savesearchdetail_distance);
		spJobType = (Spinner)v.findViewById(R.id.spinners_savesearchdetail_jobtype);
		spPostedWithin = (Spinner)v.findViewById(R.id.spinners_savesearchdetail_postedwithin);
		
		spSearchTypes.setAdapter(new SimpleSpinnerAdapter(activity, SearchTypes.values));
		spJobType.setAdapter(new SimpleSpinnerAdapter(activity, JobTypes.values));
		spPostedWithin.setAdapter(new SimpleSpinnerAdapter(activity, PostedDays.values));
		
		if(getArguments() != null){
			ss = UserSaveSearchesFragment.getInstance().getSSS(this).get(getArguments().getInt(KEY_SS_POS));
			
			actionbarTitle.setText(ss.getName());
			etSearchName.setText(ss.getName());
			etSearchFor.setText(ss.getSearchFor());
			actionbarTitle.setText(ss.getName());
			spSearchTypes.setSelection(SearchTypes.values.indexOf(ss.getSearchIn()));
			etDistance.setText(ss.getDistance());
			spJobType.setSelection(JobTypes.values.indexOf(JobTypes.getValueForKey(ss.getJobTypeID())));
			spPostedWithin.setSelection(PostedDays.values.indexOf(PostedDays.getValueForKey(ss.getPostedWithin())));

			if(selLocation != null) ss.setLocation(selLocation);
			if(selCountryID >= 0) ss.setCountryID(selCountryID);
			
			tvSelectedCountryName.setText(ss.getCountryName());
			tvSelectedLocation.setText(ss.getLocation());
		}else{
			if(selCountryID < 0){
				tvSelectedCountryName.setText("Any");
				tvSelectedLocation.setText("Any");
			}else{
				tvSelectedCountryName.setText(selCountryName);
				tvSelectedLocation.setText(selLocation);	
			}
		}
		
		trLocation.setOnClickListener(this);
		trCountryRegion.setOnClickListener(this);
		
		etSearchName.setOnFocusChangeListener(this);
		etSearchFor.setOnFocusChangeListener(this);
		etDistance.setOnFocusChangeListener(this);
		
		return v;
	}

	@Override
	public void onClick(View v) {
		if(v == actionbarButtonBack || v == actionbarTitle)
			activity.onBackPressed();
		else if(v == actionbarButtonSearch){
			String name = etSearchFor.getText().toString();
			String type = spSearchTypes.getSelectedItem().toString(); 
			String location = tvSelectedLocation.getText().toString();
			int radius = Integer.parseInt(etDistance.getText().toString());
			String jobType = spJobType.getSelectedItem().toString();
			int postedWithinID = PostedDays.getKeyForValue(spPostedWithin.getSelectedItem().toString());
			int countryID = (selCountryID<0)?0:selCountryID;
			activity.changeChildPage(JobSummaryFragment.newInstance(name, type, location, radius, jobType, countryID, postedWithinID));
		}else if(v == actionbarButtonSave){ //this page is only for save edited save searches
			pd.show();
			new Thread(new Runnable() {
				
				@Override
				public void run() {
					String tempResult;
					try{
						String name = etSearchName.getText().toString();
						String searchFor = etSearchFor.getText().toString();
						String searchType = spSearchTypes.getSelectedItem().toString(); 
						int searchTypeID = SearchTypes.getKeyForValue(searchType);
						String location = tvSelectedLocation.getText().toString();
						int distance = Integer.parseInt(etDistance.getText().toString());
						int jobTypeID = JobTypes.getKeyForValue(spJobType.getSelectedItem().toString());
						String jobType = spJobType.getSelectedItem().toString();
						int countryID = selCountryID;
						int postedWithinID = PostedDays.getKeyForValue(spPostedWithin.getSelectedItem().toString());
						
						tempResult = app.onlineGateway.saveSearch(ss.getJSONFromEditSavedSearch(name, searchFor, searchTypeID, searchType, location, 0, 0, countryID, distance, jobTypeID, jobType, postedWithinID, app));
					}catch(Exception e){
						e.printStackTrace();
						tempResult = e.getMessage();
					}
					final String result = tempResult;
					
					new Handler(Looper.getMainLooper()).post(new Runnable() {
						
						@Override
						public void run() {
							pd.dismiss();
							app.showMessageDialog(activity, (result==null)?"Saved Successfully":result);
						}
					});
				}
			}).start();
		}else if(v==trLocation){
			SearchLocationFragment.getInstance().setSearchLocationInterface(this);
			activity.changeChildPage(SearchLocationFragment.getInstance());
		}else if(v == trCountryRegion){
			SingleCountrySelectionFragment.getInstance().setSearchCountryInterface(this);
			activity.changeChildPage(SingleCountrySelectionFragment.getInstance());
		}
	}

	@Override
	public void onCountrySelected(int selCountryID, String selCountryName) {
		UserSavedSearchDetailFragment.selCountryID = selCountryID;
	}

	@Override
	public void onLocationSelected(String location) {
		UserSavedSearchDetailFragment.selLocation = location;
	}
	
	 public static int getIndex(Set<? extends Object> set, Object value) {
		   int result = 0;
		   for (Object entry:set) {
		     if (entry.equals(value)) return result;
		     result++;
		   }
		   return -1;
	 }

	@Override
	public void onFocusChange(View v, boolean hasFocus) {
		((TextView)v).setTextColor(activity.getResources().getColor((hasFocus)?R.color.black:R.color.menu_list_separator));
	}
	 
}
