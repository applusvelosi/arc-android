package applusvelosi.projects.android.arc.models;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import applusvelosi.projects.android.arc.ArcApplication;
import applusvelosi.projects.android.arc.utils.OnlineGateway;

public class User {

	HashMap<String, Object> user;
	
	public User(ArcApplication app, JSONObject jsonUser) throws Exception{
		user = new HashMap<String, Object>();
		if(jsonUser != JSONObject.NULL) {
	        user = OnlineGateway.toMap(jsonUser);
	    }
	} 
	
	public String getUserID(){
		return user.get("CandidateID").toString();
	}
	
	public String getPercentComplete(){
		return user.get("PercentComplete").toString();
	}
	
	public String getFirstName(){
		return user.get("FirstName").toString();
	}
	
	public String getLastname(){
		return user.get("LastName").toString();
	}
	
	public String getEmail(){
		return user.get("Email").toString();
	}
	
	public String getAltEmail(){
		return user.get("Email2").toString();
	}
	
	public int getGenderID(){
		return Integer.parseInt(((HashMap<String, Object>)user.get("GenderSex")).get("GenderID").toString());
	}
	
	public String getGenderName(){
		return ((HashMap<String, Object>)user.get("GenderSex")).get("GenderName").toString();		
	}
	
	public String getPhone(){
		return user.get("Phone").toString();
	}
	
	public String getAltPhone(){
		return user.get("Phone2").toString();
	}
	
	public String getMobile(){
		return user.get("Mobile").toString();
	}
	
	public String getBirthdate(OnlineGateway og){
		return og.dJsonizeDate(user.get("DOB").toString());
	}
	
	public String getAddress(){
		return user.get("Address").toString();
	}
	
	public String getCity(){
		return user.get("TownCity").toString();
	}
	
	public String getState(){
		return user.get("CountyState").toString();
	}
	
	public String getPostCode(){
		return user.get("Postcode").toString();
	}
	
	public String getCountryID(){
		return ((HashMap<String, Object>)user.get("Country")).get("CountryID").toString();
	}
	
	public String getCountryName(){
		return ((HashMap<String, Object>)user.get("Country")).get("CountryName").toString();
	}
	
	public void setCountry(int countryID, String countryName){
		HashMap<String, Object> country = (HashMap<String, Object>)user.get("Country");
		country.put("CountryID", countryID);
		country.put("CountryName", countryName);
		country.put("Country2", "XX");
		country.put("Country3", "XXX");
		country.put("DialCode", "");
		country.put("GMT", 0);
		country.put("RegionID", 0);
		country.put("RegionName", "");
		user.put("CountryID", countryID);
	}
	
	public String getLinkedIn(){
		return user.get("LinkedIn").toString();
	}
	
	public String getTwitter(){
		return user.get("Twitter").toString();
	}
	
	public String getSkype(){
		return user.get("Skype").toString();
	}
	
	public boolean isEUAuthorized(){
		return Boolean.parseBoolean(user.get("EUAuthorised").toString());
	}
	
	public String getUniversity(){
		return user.get("UniversityAttended").toString();
	}
	
	public String getSubject(){
		return user.get("AcademicSubject").toString();
	}
	
	public String getYearGraduated(){
		return user.get("GraduationYear").toString();
	}
	
	public int getEducationID(){
		return Integer.parseInt(((HashMap<String, Object>)user.get("Education")).get("EducationID").toString());
	}
	
	public int getDrivingLicenseID(){
		return Integer.parseInt(((HashMap<String, Object>)user.get("DrivingLicense")).get("StatusID").toString());
	}
	
	public int getNationalityID(){
		return Integer.parseInt(user.get("NationalityID").toString());
	}
	
	public int getEthnicityID(){
		return Integer.parseInt(((HashMap<String, Object>)user.get("Ethnicity")).get("EthnicityID").toString());
	}
	
	public Integer getReferrerID(){
		return Integer.parseInt(user.get("ReferrerID").toString());
	}
	
	public int getMaritalStatusID(){
		return Integer.parseInt(((HashMap<String, Object>)user.get("MaritalStatus")).get("StatusID").toString());
	}
	
	public ArrayList<String> getPreferredLocations(){
		return new ArrayList<String>(Arrays.asList(user.get("PreferredLocation").toString().split(",")));
	}
	
	public void updatePreferredLocations(ArrayList<String> preferredLocations){
		StringBuilder locPrefsStr = new StringBuilder();
		for(String preferredLocation:preferredLocations)
			locPrefsStr.append(preferredLocation+",");
		
		System.out.println(locPrefsStr);
		if(locPrefsStr.length()>0)
			user.put("PreferredLocation", locPrefsStr.substring(0, locPrefsStr.length()-1));
	}
	
	public void updatePreferredLanguages(ArrayList<String> preferredLanguages){
		StringBuilder locPrefsStr = new StringBuilder();
		for(String preferredLanguage:preferredLanguages)
			locPrefsStr.append(preferredLanguage+",");
		
		System.out.println(locPrefsStr);
		if(locPrefsStr.length()>0)
			user.put("Languages", locPrefsStr.substring(0, locPrefsStr.length()-1));
	}	
	
	public int getRelocationID(){
		return Integer.parseInt(((HashMap<String, Object>)user.get("Relocate")).get("StatusID").toString());
	}
	
	public int getNoticePeriodID(){
		return Integer.parseInt(((HashMap<String, Object>)user.get("Availability")).get("AvailabilityID").toString());
	}
	
	public String getAvailableFrom(OnlineGateway og){
		return og.dJsonizeDate(user.get("AvailableFrom").toString());
	}
	
	public ArrayList<String> getLanguages(){
		return new ArrayList<String>(Arrays.asList(user.get("Languages").toString().split(",")));
	}
	
	public boolean isAlertAllowed(){
		return Boolean.parseBoolean(user.get("SendEmails").toString());
	}
	
	public boolean isPermanent(){
		return Boolean.parseBoolean(user.get("Permanent").toString());
	}

	public boolean isTemporary(){
		return Boolean.parseBoolean(user.get("Temporary").toString());
	}
	
	public boolean isContract(){
		return Boolean.parseBoolean(user.get("Contract").toString());
	}

	public boolean isPartTime(){
		return Boolean.parseBoolean(user.get("PartTime").toString());
	}
	
	public String getJobTitles(){
		return user.get("PreferredJobTitles").toString();
	}
	
	public String getCurrency(){
		return ((HashMap<String, Object>)user.get("Salary")).get("SalaryCurrency").toString();
	}
	
	public String getSalaryFrom(){
		return ((HashMap<String, Object>)user.get("Salary")).get("SalaryFrom").toString();
	}
	
	public String getSalaryTo(){
		return ((HashMap<String, Object>)user.get("Salary")).get("SalaryTo").toString();
	}
	
	public int getSalaryTypeID(){
		return Integer.parseInt(((HashMap<String, Object>)((HashMap<String, Object>)user.get("Salary")).get("SalaryType")).get("SalaryTypeID").toString());
	}
	
	public String getMainSkills(){
		return user.get("MainSkills").toString();
	}
	
	public String getJSON(	String fname, String lname, String email, String altEmail, int genderID, String gender, String phone, String altPhone,
							String mobile, String bday, String address, String city, String state, String postCode, 
							String linkedIn, String twitter, String skype, boolean isEu, String university, int licenseID, String license,
							String subject, int yearGraduated, int educationID, String education, int natID, String nationality, int ethnicityID, 
							String ethnicity, int referrerID, int marStatID, String marStat, 
							int relocateID, String relocate, int noticePeriodID, String noticePeriod, String availableFrom, boolean isAlertAllowed,
							boolean isPermanent, boolean isTemporary, boolean isContract, boolean isPartTime, String jobTitles, String curThree,
							long salaryFrom, long salaryTo, int salaryTypeID, String salaryType, String mainSkills, ArcApplication app) throws Exception{

		user.put("FirstName", fname);
		user.put("LastName", lname);
		user.put("FullName", fname+" "+lname);
		user.put("Email", email);
		user.put("Email2", altEmail);
		user.put("Phone", phone);
		user.put("Phone2", altPhone);
		user.put("Mobile", mobile);
		user.put("DOB", app.onlineGateway.jsonizeDate(bday));
		user.put("Address", address);
		user.put("TownCity", city);
		user.put("CountyState", state);
		user.put("Postcode", postCode);
		user.put("LinkedIn", linkedIn);
		user.put("Twitter", twitter);
		user.put("Skype", skype);
		user.put("UniversityAttended", university);
		user.put("AcademicSubject", subject);
		user.put("GraduationYear", yearGraduated);
		user.put("AvailableFrom", app.onlineGateway.jsonizeDate(availableFrom));
		user.put("PreferredJobTitles", jobTitles);
		user.put("MainSkills", mainSkills);
		
		user.put("EUAuthorised", isEu);
		user.put("SendEmails", isAlertAllowed);
		user.put("SendSMS", isAlertAllowed);
		user.put("Permanent", isPermanent);
		user.put("Temporary", isTemporary);
		user.put("Contract", isContract);
		user.put("PartTime", isPartTime);
		
		HashMap<String, Object> genderMap = (HashMap<String, Object>)user.get("GenderSex");
		genderMap.put("Gender", genderID);
		genderMap.put("GenderID", genderID);
		genderMap.put("GenderName", gender);
		user.put("GenderSex", genderMap);
		user.put("GenderSexID", genderID);
		HashMap<String, Object> educMap = (HashMap<String, Object>)user.get("Education");
		educMap.put("EducationID", educationID);
		educMap.put("Description", education);
		user.put("Education", educMap);
		user.put("EducationID", educationID);
		HashMap<String, Object> licenseMap = (HashMap<String, Object>)user.get("DrivingLicense");
		licenseMap.put("Status", licenseID);
		licenseMap.put("StatusID", licenseID);
		licenseMap.put("StatusName", license);
		user.put("DrivingLicense", licenseMap);
		user.put("DrivingLicenseID", licenseID);
		user.put("NationalityID", natID);
		HashMap<String, Object> ethMap = (HashMap<String, Object>)user.get("Ethnicity");
		ethMap.put("EthnicityID", ethnicityID);
		ethMap.put("EthnicityName", ethnicity);
		user.put("Ethnicity", ethMap);
		user.put("EthnicityID", ethnicityID);
		user.put("ReferrerID", referrerID);
		HashMap<String, Object> marStatMap = (HashMap<String, Object>)user.get("MaritalStatus");
		marStatMap.put("Status", marStatID);
		marStatMap.put("StatusID", marStatID);
		marStatMap.put("StatusName", marStat);
		user.put("MaritalStatus", marStatMap);
		user.put("MaritalStatusID", marStatID);
		HashMap<String, Object> relocateMap = (HashMap<String, Object>)user.get("Relocate");
		relocateMap.put("Status", relocateID);
		relocateMap.put("StatusID", relocateID);
		relocateMap.put("StatusName", relocate);
		user.put("Relocate", relocateMap);
		user.put("RelocateID", relocateID);
		HashMap<String, Object> availMap = (HashMap<String, Object>)user.get("Availability");
		availMap.put("Availability", noticePeriodID);
		availMap.put("AvailabilityID", noticePeriodID);
		availMap.put("AvailabilityName", noticePeriod);
		user.put("Availability", availMap);
		user.put("SalaryCurrency", curThree);
		HashMap<String, Object> salaryMap = (HashMap<String, Object>)user.get("Salary");
		salaryMap.put("SalaryCurrency", curThree);
		salaryMap.put("SalaryFrom", salaryFrom);
		salaryMap.put("SalaryTo", salaryTo);
		HashMap<String, Object> salaryTypeMap = (HashMap<String, Object>)salaryMap.get("SalaryType");
		salaryTypeMap.put("SalaryType", salaryTypeID);
		salaryTypeMap.put("SalaryTypeID", salaryTypeID);
		salaryMap.put("SalaryType", salaryTypeMap);
		user.put("SalaryFrom", salaryFrom);
		user.put("SalaryTo", salaryTo);
		user.put("SalaryTypeID", salaryTypeID);
		return app.gson.toJson(user, app.types.hashmapOfStringObject);
	}
}
