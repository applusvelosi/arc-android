package applusvelosi.projects.android.arc.adapters.lists;

import java.util.ArrayList;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;
import applusvelosi.projects.android.arc.R;
import applusvelosi.projects.android.arc.models.Document;
import applusvelosi.projects.android.arc.models.Employment;
import applusvelosi.projects.android.arc.views.SidebarActivity;

public class EmpListAdapter extends BaseAdapter{
	private SidebarActivity activity;
	private ArrayList<Employment> emps;
	
	public EmpListAdapter(SidebarActivity activity, ArrayList<Employment> emps){
		this.activity = activity;
		this.emps = emps;
	}

	@Override
	public View getView(int pos, View v, ViewGroup parent) {
		Holder holder;
		if(v == null){
			holder = new Holder();
			v = LayoutInflater.from(activity).inflate(R.layout.cell_employment, null);
			holder.tvJobTitle = (TextView)v.findViewById(R.id.tviews_profile_emp_jobtitle);
			holder.tvEmployer = (TextView)v.findViewById(R.id.tviews_profile_emp_employer);
			holder.tvDates = (TextView)v.findViewById(R.id.tviews_profile_emp_dates);
			v.setTag(holder);
		}
		
		holder = (Holder)v.getTag();
		Employment emp = emps.get(pos);
		holder.tvJobTitle.setText(emp.getJobTitle());
		holder.tvEmployer.setText(emp.getEmployer());
		holder.tvDates.setText(emp.getDateStart()+" - "+emp.getDateEnd());
		
		return v;
	}
	
	@Override
	public int getCount() {
		return emps.size();
	}

	@Override
	public Object getItem(int pos) {
		return emps.get(pos);
	}

	@Override
	public long getItemId(int pos) {
		// TODO Auto-generated method stub
		return 0;
	}

	private class Holder{
		private TextView tvJobTitle, tvEmployer, tvDates;
	}
}
