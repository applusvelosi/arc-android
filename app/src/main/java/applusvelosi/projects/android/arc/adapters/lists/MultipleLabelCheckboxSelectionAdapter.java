package applusvelosi.projects.android.arc.adapters.lists;

import java.util.ArrayList;

import android.support.v4.app.FragmentActivity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.TextView;
import applusvelosi.projects.android.arc.R;

public class MultipleLabelCheckboxSelectionAdapter extends BaseAdapter{
	MultipleLabelCheckboxSelector selector;
	ArrayList<String> titles;
	ArrayList<String> selecteds;
	
	public MultipleLabelCheckboxSelectionAdapter(MultipleLabelCheckboxSelector selector, ArrayList<String> titles, ArrayList<String> selecteds){
		this.selector = selector;
		this.titles = titles;
		this.selecteds = selecteds;
	}
	
	@Override
	public View getView(int pos, View recyclableView, ViewGroup parent) {
		MultiSelectionLabelCheckboxSelectionAdapterHolder holder;
		View v= recyclableView;
		
		if(v == null){
			holder = new MultiSelectionLabelCheckboxSelectionAdapterHolder();
			v = selector.getActivity().getLayoutInflater().inflate(R.layout.cell_labelandcbox, null);
			holder.tv = (TextView)v.findViewById(R.id.tviews_cells_labelandcbox);
			holder.cb = (CheckBox)v.findViewById(R.id.cboxs_cells_labelandcbox);
			v.setTag(holder);
		}
		
		String title = titles.get(pos);
		holder = (MultiSelectionLabelCheckboxSelectionAdapterHolder)v.getTag();
		holder.tv.setText(title);
		holder.cb.setChecked(selecteds.contains(title));
		v.setTag(holder);
		holder.cb.setOnCheckedChangeListener(selector.getCheckChangeListener());
		
		return v;
	}

	@Override
	public int getCount() {
		return titles.size();
	}
	
	@Override
	public Object getItem(int pos) {
		return titles.get(pos);
	}
	
	@Override
	public long getItemId(int pos) {
		return 0;
	}
	
	public class MultiSelectionLabelCheckboxSelectionAdapterHolder{
		public TextView tv;
		public CheckBox cb;
	}
	
	public interface MultipleLabelCheckboxSelector{
		public FragmentActivity getActivity();
		public OnCheckedChangeListener getCheckChangeListener();
	}
}
