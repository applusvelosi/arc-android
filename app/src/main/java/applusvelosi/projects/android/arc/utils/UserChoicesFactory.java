package applusvelosi.projects.android.arc.utils;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.LinkedHashMap;

import applusvelosi.projects.android.arc.models.Referrer;

public class UserChoicesFactory {

	private final String USER_SALARYTYPE_NOTSELECTED = "";
	private final String USER_SALARYTYPE_ANNUALLY = "Annually";
	private final String USER_SALARYTYPE_MONTHLY = "Monthly";
	private final String USER_SALARYTYPE_WEEKLY = "Weekly";
	private final String USER_SALARYTYPE_DAILY = "Daily";
	private final String USER_SALARYTYPE_HOURLY = "Hourly";

	private final String USER_AVAIL_NOTSELECTED = "";
	private final String USER_AVAIL_1W = "1 Week Notice";
	private final String USER_AVAIL_2W = "2 Weeks Notice";
	private final String USER_AVAIL_3W = "3 Weeks Notice";
	private final String USER_AVAIL_4W = "4 Weeks Notice";
	private final String USER_AVAIL_1M = "1 Month Notice";
	private final String USER_AVAIL_2M = "2 Months Notice";
	private final String USER_AVAIL_3MPLUS = "3+ Months Notice";
	private final String USER_AVAIL_NOW = "Available Now";

	private final String USER_EDUC_NONE = "";
	private final String USER_EDUC_NOFORMAL = "No Formal Qualifications";
	private final String USER_EDUC_ALEVEL = "School Qualification";
	private final String USER_EDUC_VOCATIONAL = "Vocational";
	private final String USER_EDUC_APPRENTICE = "Apprenticship";
	private final String USER_EDUC_TRADE = "Trade Certification";
	private final String USER_EDUC_COLLEGE = "College Diploma/Certificate";
	private final String USER_EDUC_HIGHERCERT = "Higher National Certificate";
	private final String USER_EDUC_HIGHERDIPL = "Higher National Diploma";
	private final String USER_EDUC_PASSBACHELORS = "Pass Bachelors";
	private final String USER_EDUC_CLASS3BACHELORS = "3rd Class Bachelors";
	private final String USER_EDUC_CLASS2LBACHERLORS = "Lower 2nd Class Bachelors";
	private final String USER_EDUC_CLASS2UBACHELORS = "Upper 2nd Class Bachelors";
	private final String USER_EDUC_CLASS1BACHELORS = "1st Class Bachelors";
	private final String USER_EDUC_DEGREEH = "Highest Degree";
	private final String USER_EDUC_DEGREEM = "Masters Degree";
	private final String USER_EDUC_PROFESSIONAL = "Professional/Chartered";
	private final String USER_EDUC_MBA = "MBA";
	private final String USER_EDUC_DOCTORATE = "Doctorate/PhD";

	private final String USER_GENDER_U = "Unknown";
	private final String USER_GENDER_M = "Male";
	private final String USER_GENDER_F = "Female";

	private final String USER_MS_UNKNOWN = "Unknown";
	private final String USER_MS_SINGLE = "Single";
	private final String USER_MS_MARRIED = "Married";
	private final String USER_MS_DIVORCED = "Divorced";
	private final String USER_MS_WIDOWED = "Widowed";
	private final String USER_MS_COHABITING = "Cohabiting";
	private final String USER_MS_CIVILUNION = "Civil Union";
	private final String USER_MS_PARTNERSHIP = "Domestic Partnership";

	private final String USER_NAT_NONE = "";
	private final String USER_NAT_ALGERIAN = "Algerian"; 
	private final String USER_NAT_AMERICAN = "American";
	private final String USER_NAT_ANGOLIAN = "Angolian";
	private final String USER_NAT_ARABIC = "Arabic";
	private final String USER_NAT_ARGENTINIAN = "Argentinean"; 
	private final String USER_NAT_AUSTRALIAN = "Australian";
	private final String USER_NAT_BELGIAN = "Belgian";
	private final String USER_NAT_BOSNIAN = "Bosnian";
	private final String USER_NAT_BRITISH = "British";
	private final String USER_NAT_BULGARIAN = "Bulgarian";
	private final String USER_NAT_CANADIAN = "Canadian";
	private final String USER_NAT_CHILEAN = "Chilean";
	private final String USER_NAT_CHINESE = "Chinese";
	private final String USER_NAT_COLUMBIAN = "Columbian";
	private final String USER_NAT_CROATIAN = "Croatian";
	private final String USER_NAT_DANISH = "Danish";
	private final String USER_NAT_DUTCH = "Dutch";
	private final String USER_NAT_EGYPTIAN = "Egyptian";
	private final String USER_NAT_FILIPINO = "Filipino";
	private final String USER_NAT_FRENCH = "French";
	private final String USER_NAT_GERMAN = "German";
	private final String USER_NAT_GHANADIAN = "Ghanadian";
	private final String USER_NAT_GREEK = "Greek";
	private final String USER_NAT_INDIAN = "Indian";
	private final String USER_NAT_INDONESIAN = "Indonesian";
	private final String USER_NAT_IRANIAN = "Iranian";
	private final String USER_NAT_IRISH = "Irish";
	private final String USER_NAT_ITALIAN = "Italian";
	private final String USER_NAT_KAZAKH = "Kazakh";
	private final String USER_NAT_KOREAN = "Korean";
	private final String USER_NAT_KUWAITI = "Kuwaiti";
	private final String USER_NAT_KYRGYZSTANI = "Kyrgyzstani";
	private final String USER_NAT_LITHUANIAN = "Lithuanian";
	private final String USER_NAT_MALAYSIAN = "Malaysian";
	private final String USER_NAT_MAURITIAN = "Mauritian";
	private final String USER_NAT_MEXICAN = "Mexican";
	private final String USER_NAT_MONGOLIAN = "Mongolian";
	private final String USER_NAT_NEWZEALAND = "New Zealand";
	private final String USER_NAT_NIGERIAN = "Nigerian";
	private final String USER_NAT_NORWEGIAN = "Norwegian";
	private final String USER_NAT_OTHER = "Other";
	private final String USER_NAT_PAKISTANI = "Pakistani";
	private final String USER_NAT_PAPUANEWGUINEAN = "Papua New Guinean";
	private final String USER_NAT_PERUVIAN = "Peruvian";
	private final String USER_NAT_POLISH = "Polish";
	private final String USER_NAT_PORTUGUESE = "Portuguese";
	private final String USER_NAT_ROMANIAN = "Romanian";
	private final String USER_NAT_RUSSIAN = "Russian";
	private final String USER_NAT_SAUDIARABIAN = "Saudi Arabian";
	private final String USER_NAT_SCOTTISH = "Scottish";
	private final String USER_NAT_SERBIAN = "Serbian";
	private final String USER_NAT_SOUTHAFRICAN = "South African";
	private final String USER_NAT_SOUTHAMERICAN = "South American";
	private final String USER_NAT_SPANISH = "Spanish";
	private final String USER_NAT_SRILANKAN = "Sri Lankan";
	private final String USER_NAT_SWEDISH = "Swedish";
	private final String USER_NAT_SWISS = "Swiss";
	private final String USER_NAT_SYRIAN = "Syrian";
	private final String USER_NAT_THAI = "Thai";
	private final String USER_NAT_TURKISH = "Turkish";
	private final String USER_NAT_VENEZUELAN = "Venezuelan";
	private final String USER_NAT_WELSH = "Welsh";

	private final String USER_NO = "No";
	private final String USER_YES = "Yes";
	private final String USER_UNKNOWN = "Unknown";
	private final String USER_MAYBE = "Maybe";

	private final String USER_ETH_NONE = "";
	private final String USER_ETH_WHITEENGLISH = "White English";
	private final String USER_ETH_WHITEIRISH = "White Irish";
	private final String USER_ETH_ANYOTHERWHITEBG = "Any Other White Background";
	private final String USER_ETH_WHITEANDBLACKCARIBBEAN = "White and Black Caribbean";
	private final String USER_ETH_WHITEANDBLACKAFRICAN = "White and Black African";
	private final String USER_ETH_WHITEANDASIAN = "White and Asian";
	private final String USER_ETH_ANYOTHERMIXEDBG = "Any Other Mixed Background";
	private final String USER_ETH_INDIAN = "Indian";
	private final String USER_ETH_PAKISTANI = "Pakistani";
	private final String USER_ETH_AFRICAN = "African";
 	private final String USER_ETH_ANYOTHERBLACKBG = "Any Other Black Background";
	private final String USER_ETH_CHINESE = "Chinese";
	private final String USER_ETH_ANYOTHERETHNIC = "Any Other Ethnic Group";

	private final String USER_LANG_AFRIKAANS = "Afrikaans";
	private final String USER_LANG_ALGERIAN = "Algerian";
	private final String USER_LANG_ARABIC = "Arabic";
	private final String USER_LANG_AWADHI = "Awadhi";
	private final String USER_LANG_AZERBAIJANI = "Azerbaijani";
	private final String USER_LANG_BENGALI = "Bengali";
	private final String USER_LANG_BHOJPURI = "Bhojpuri";
	private final String USER_LANG_BOSNIAN = "Bosnian";
	private final String USER_LANG_BURMESE = "Burmese";
	private final String USER_LANG_CHILEAN = "Chilean";
	private final String USER_LANG_CHINESE = "Chinese";
	private final String USER_LANG_CROATIAN = "Croatian";
	private final String USER_LANG_DANISH = "Danish";
	private final String USER_LANG_DUTCH = "Dutch";
	private final String USER_LANG_ENGLISH = "English";
	private final String USER_LANG_FILIPINO = "Filipino";
	private final String USER_LANG_FRENCH = "French";
	private final String USER_LANG_GERMAN = "German";
	private final String USER_LANG_GREEK = "Greek";
	private final String USER_LANG_GUJARATI = "Gujarati";
	private final String USER_LANG_HAUSA = "Hausa";
	private final String USER_LANG_HINDI = "Hindi";
	private final String USER_LANG_INDONESIANBAHASA = "Indonesian Bahasa";
	private final String USER_LANG_ITALIAN = "Italian";
	private final String USER_LANG_JAPANESE = "Japanese";
	private final String USER_LANG_JAVANESE = "Javanese";
	private final String USER_LANG_KANNADA = "Kannada";
	private final String USER_LANG_KAZAKH = "Kazakh";
	private final String USER_LANG_KOREAN = "Korean";
	private final String USER_LANG_MAITHILI = "Maithili";
	private final String USER_LANG_MALAYALAM = "Malayalam";
	private final String USER_LANG_MALAYSIANBAHASA = "Malaysian Bahasa";
	private final String USER_LANG_MARATHI = "Marathi";
	private final String USER_LANG_MONGOLIAN = "Mongolian";
	private final String USER_LANG_ORIYA = "Oriya";
	private final String USER_LANG_PANJABI = "Panjabi";
	private final String USER_LANG_PAPUANLANGUAGES = "Papuan Languages";
	private final String USER_LANG_PERSIAN = "Persian";
	private final String USER_LANG_POLISH = "Polish";
	private final String USER_LANG_PORTUGUESE = "Portuguese";
	private final String USER_LANG_ROMANIAN = "Romanian";
	private final String USER_LANG_RUSSIAN = "Russian";
	private final String USER_LANG_SERBIAN = "Serbian";
	private final String USER_LANG_SINDHI = "Sindhi";
	private final String USER_LANG_SPANISH = "Spanishh";
	private final String USER_LANG_SUNDA = "Sunda";
	private final String USER_LANG_TAMIL = "Tamil";
	private final String USER_LANG_TELUGU = "Telugu";
	private final String USER_LANG_THAI = "Thai";
	private final String USER_LANG_TOKPISIN = "Tik Pisin";
	private final String USER_LANG_TURKISH = "Turkish";
	private final String USER_LANG_UKRAINIAN = "Ukrainian";
	private final String USER_LANG_URDU = "Urdu";
	private final String USER_LANG_VIETNAMESE = "Vietnamese";
	private final String USER_LANG_YORUBA = "Yoruba";
	
	private final String CURRENCY_UAEDIRHAM = "UAE Dirham";
	private final String CURRENCY_ALBANIANLEK = "Albanian Lek";
	private final String CURRENCY_NETHANTILLESGUILDER = "Neth Antilles Guilder";
	private final String CURRENCY_ARGENTINEPESO = "Argentine Peso";
	private final String CURRENCY_AUSTRALIANDOLLAR = "Australian Dollar";
	private final String CURRENCY_ARUBAFLORIN = "Aruba Florin";
	private final String CURRENCY_BARBADOSDOLLAR = "Barbados Dollar";
	private final String CURRENCY_BANGLADESHTAKA = "Bangladesh Taka";
	private final String CURRENCY_BULGARIANLEV = "Bulgarian Lev";
	private final String CURRENCY_BAHRAINIDINAR = "Bahraini Dinar";
	private final String CURRENCY_BURUNDIFRANC = "Burundi Franc";
	private final String CURRENCY_BERMUDADOLLAR = "Bermuda Dollar";
	private final String CURRENCY_BRUNEIDOLLAR = "Brunei Dollar";
	private final String CURRENCY_BOLIVIANBOLIVIANO = "Bolivian Boliviano";
	private final String CURRENCY_BRAZILIANREAL = "Brazilian Real";
	private final String CURRENCY_BAHAMIANDOLLAR = "Bahamian Dollar";
	private final String CURRENCY_BHUTANNGULTRUM = "Bhutan Ngultrum";
	private final String CURRENCY_BOTSWANAPULA = "Botswana Pula";
	private final String CURRENCY_BELARUSRUBLE = "Belarus Ruble";
	private final String CURRENCY_BELIZEDOLLAR = "Belize Dollar";
	private final String CURRENCY_CANADIANDOLLAR = "Canadian Dollar";
	private final String CURRENCY_SWISSFRANC = "Swiss Franc";
	private final String CURRENCY_CHILEANPESO = "Chilean Peso";
	private final String CURRENCY_CHINESEYUAN = "Chinese Yuan";
	private final String CURRENCY_COLOMBIANPESO = "Colombian Peso";
	private final String CURRENCY_COSTARICACOLON = "Costa Rica Colon";
	private final String CURRENCY_CUBANPESO = "Cuban Peso";
	private final String CURRENCY_CAPEVERDEESCUDO = "Cape Verde Escudo";
	private final String CURRENCY_CZECHKORUNA = "Czech Koruna";
	private final String CURRENCY_DIJIBOUTIFRANC = "Dijibouti Franc";
	private final String CURRENCY_DANISHKRONE = "Danish Krone";
	private final String CURRENCY_DOMINICANPESO = "Dominican Peso";
	private final String CURRENCY_ALGERIANDINAR = "Algerian Dinar";
	private final String CURRENCY_ESTONIANKROON = "Estonian Kroon";
	private final String CURRENCY_EGYPTIANPOUND = "Egyptian Pound";
	private final String CURRENCY_ETHIOPIANBIRR = "Ethiopian Birr";
	private final String CURRENCY_EURO = "Euro";
	private final String CURRENCY_FIJIDOLLAR = "Fiji Dollar";
	private final String CURRENCY_FALKLANDISLANDSPOUND = "Falkland Islands Pound";
	private final String CURRENCY_BRITISHPOUND = "British Pound";
	private final String CURRENCY_GIBRALTARPOUND = "Gibraltar Pound";
	private final String CURRENCY_GAMBIANDALASI = "Gambian Dalasi";
	private final String CURRENCY_GUINEAFRANC = "Guinea Franc";
	private final String CURRENCY_GUATEMALAQUETZAL = "Guatemala Quetzal";
	private final String CURRENCY_GUYANADOLLAR = "Guyana Dollar";
	private final String CURRENCY_HONGKONGDOLLAR = "Hong Kong Dollar";
	private final String CURRENCY_HONDURASLEMPIRA = "Honduras Lempira";
	private final String CURRENCY_CROATIANKUNA = "Croatian Kuna";
	private final String CURRENCY_HAITIGOURDE = "Haiti Gourde";
	private final String CURRENCY_HUNGARIANFORINT = "Hungarian Forint";
	private final String CURRENCY_INDONESIANRUPIAH = "Indonesian Rupiah";
	private final String CURRENCY_ISRAELISHEKEL = "Israeli Shekel";
	private final String CURRENCY_INDIANRUPEE = "Indian Rupee";
	private final String CURRENCY_IRAQIDINAR = "Iraqi Dinar";
	private final String CURRENCY_IRANRIAL = "Iran Rial";
	private final String CURRENCY_ICELANDKRONA = "Iceland Krona";
	private final String CURRENCY_JAMAICANDOLLAR = "Jamaican Dollar";
	private final String CURRENCY_JORDANIANDINAR = "Jordanian Dinar";
	private final String CURRENCY_JAPANESEYEN = "Japanese Yen";
	private final String CURRENCY_KENYANSHILLING = "Kenyan Shilling";
	private final String CURRENCY_CAMBODIARIEL = "Cambodia Riel";
	private final String CURRENCY_COMOROSFRANC = "Comoros Franc";
	private final String CURRENCY_NORTHKOREANWON = "North Korean Won";
	private final String CURRENCY_KOREANWON = "Korean Won";
	private final String CURRENCY_KUWAITIDINAR = "Kuwaiti Dinar";
	private final String CURRENCY_CAYMANISLANDSDOLLAR = "Cayman Islands Dollar";
	private final String CURRENCY_KAZAKHSTANTENGE = "Kazakhstan Tenge";
	private final String CURRENCY_LAOKIP = "Lao Kip";
	private final String CURRENCY_LEBANESEPOUND = "Lebanese Pound";
	private final String CURRENCY_SRILANKARUPEE = "Sri Lanka Rupee";
	private final String CURRENCY_LIBERIANDOLLAR = "Liberian Dollar";
	private final String CURRENCY_LESOTHOLOTI = "Lesotho Loti";
	private final String CURRENCY_LITHUANIANLITA = "Lithuanian Lita";
	private final String CURRENCY_LATVIANLAT = "Latvian Lat";
	private final String CURRENCY_LIBYANDINAR = "Libyan Dinar";
	private final String CURRENCY_MOROCCANDIRHAM = "Moroccan Dirham";
	private final String CURRENCY_MOLDOVANLEU = "Moldovan Leu";
	private final String CURRENCY_MACEDONIANDENAR = "Macedonian Denar";
	private final String CURRENCY_MYANMARKYAT = "Myanmar Kyat";
	private final String CURRENCY_MONGOLIANTUGRIK = "Mongolian Tugrik";
	private final String CURRENCY_MACAUPATACA = "Macau Pataca";
	private final String CURRENCY_MAURITANIAOUGULYA = "Mauritania Ougulya";
	private final String CURRENCY_MAURITIUSRUPEE = "Mauritius Rupee";
	private final String CURRENCY_MALDIVESRUFIYAA = "Maldives Rufiyaa";
	private final String CURRENCY_MALAWIKWACHA = "Malawi Kwacha";
	private final String CURRENCY_MEXICANPESO = "Mexican Peso";
	private final String CURRENCY_MALAYSIANRINGGIT = "Malaysian Ringgit";
	private final String CURRENCY_NAMIBIANDOLLAR = "Namibian Dollar";
	private final String CURRENCY_NIGERIANNAIRA = "Nigerian Naira";
	private final String CURRENCY_NICARAGUACORDOBA = "Nicaragua Cordoba";
	private final String CURRENCY_NORWEGIANKRONE = "Norwegian Krone";
	private final String CURRENCY_NEPALESERUPEE = "Nepalese Rupee";
	private final String CURRENCY_NEWZEALANDDOLLAR = "New Zealand Dollar";
	private final String CURRENCY_OMANIRIAL = "Omani Rial";
	private final String CURRENCY_PANAMABALBOA = "Panama Balboa";
	private final String CURRENCY_PERUVIANNUEVOSOL = "Peruvian Nuevo Sol";
	private final String CURRENCY_PAPUANEWGUINEAKINA = "Papua New Guinea Kina";
	private final String CURRENCY_PHILIPPINEPESO = "Philippine Peso";
	private final String CURRENCY_PAKISTANIRUPEE = "Pakistani Rupee";
	private final String CURRENCY_POLISHZLOTY = "Polish Zloty";
	private final String CURRENCY_PARAGUAYANGUARANI = "Paraguayan Guarani";
	private final String CURRENCY_QATARRIAL = "Qatar Rial";
	private final String CURRENCY_ROMANIANNEWLEU = "Romanian New Leu";
	private final String CURRENCY_RUSSIANROUBLE = "Russian Rouble";
	private final String CURRENCY_RWANDAFRANC = "Rwanda Franc";
	private final String CURRENCY_SAUDIARABIANRIYAL = "Saudi Arabian Riyal";
	private final String CURRENCY_SOLOMONISLANDSDOLLAR = "Solomon Islands Dollar";
	private final String CURRENCY_SEYCHELLESRUPEE = "Seychelles Rupee";
	private final String CURRENCY_SWEDISHKRONA = "Swedish Krona";
	private final String CURRENCY_SINGAPOREDOLLAR = "Singapore Dollar";
	private final String CURRENCY_STHELENAPOUND = "St Helena Pound";
	private final String CURRENCY_SLOVAKKORUNA = "Slovak Koruna";
	private final String CURRENCY_SIERRALEONELEONE = "Sierra Leone Leone";
	private final String CURRENCY_SOMALISHILLING = "Somali Shilling";
	private final String CURRENCY_SAOTOMEDOBRA = "Sao Tome Dobra";
	private final String CURRENCY_ELSALVADORCOLON = "El Salvador Colon";
	private final String CURRENCY_SYRIANPOUND = "Syrian Pound";
	private final String CURRENCY_SWAZILANDLILAGENI = "Swaziland Lilageni";
	private final String CURRENCY_THAIBAHT = "Thai Baht";
	private final String CURRENCY_TUNISIANDINAR = "Tunisian Dinar";
	private final String CURRENCY_TONGAPAANGA = "Tonga Pa'anga";
	private final String CURRENCY_TURKISHLIRA = "Turkish Lira";
	private final String CURRENCY_TRINIDADTOBAGODOLLAR = "Trinidad & Tobago Dollar";
	private final String CURRENCY_TAIWANDOLLAR = "Taiwan Dollar";
	private final String CURRENCY_TANZANIANSHILLING = "Tanzanian Shilling";
	private final String CURRENCY_UKRAINEHRYVNIA = "Ukraine Hryvnia";
	private final String CURRENCY_UGANDANSHILLING = "Ugandan Shilling";
	private final String CURRENCY_USDOLLAR = "U.S. Dollar";
	private final String CURRENCY_URUGUAYANNEWPESO = "Uruguayan New Peso";
	private final String CURRENCY_VIETNAMDONG = "Vietnam Dong";
	private final String CURRENCY_VANUATUVATU = "Vanuatu Vatu";
	private final String CURRENCY_SAMOATALA = "Samoa Tala";
	private final String CURRENCY_CFAFRANC = "CFA Franc";
	private final String CURRENCY_SILVEROUNCES = "Silver Ounces";
	private final String CURRENCY_GOLDOUNCES = "Gold Ounces";
	private final String CURRENCY_EASTCARIBBEANDOLLAR = "East Caribbean Dollar";
	private final String CURRENCY_COPPERPOUNDS = "Copper Pounds";
	private final String CURRENCY_PALLADIUMOUNCES = "Palladium Ounces";
	private final String CURRENCY_PACIFICFRANC = "Pacific Franc";
	private final String CURRENCY_PLATINUMOUNCES = "Platinum  Ounces";
	private final String CURRENCY_YEMENRIYAL = "Yemen Riyal";
	private final String CURRENCY_SOUTHAFRICANRAND = "South African Rand";
	private final String CURRENCY_ZAMBIANKWACHA = "Zambian Kwacha";
	private final String CURRENCY_ZIMBABWEDOLLAR = "Zimbabwe Dollar";

	private final String CURRENCYSYM_AED = "AED";
	private final String CURRENCYSYM_ALL = "ALL";
	private final String CURRENCYSYM_ANG = "ANG";
	private final String CURRENCYSYM_ARS = "ARS";
	private final String CURRENCYSYM_AUD = "AUD";
	private final String CURRENCYSYM_AWG = "AWG";
	private final String CURRENCYSYM_BBD = "BBD";
	private final String CURRENCYSYM_BDT = "BDT";
	private final String CURRENCYSYM_BGN = "BGN";
	private final String CURRENCYSYM_BHD = "BHD";
	private final String CURRENCYSYM_BIF = "BIF";
	private final String CURRENCYSYM_BMD = "BMD";
	private final String CURRENCYSYM_BND = "BND";
	private final String CURRENCYSYM_BOB = "BOB";
	private final String CURRENCYSYM_BRL = "BRL";
	private final String CURRENCYSYM_BSD = "BSD";
	private final String CURRENCYSYM_BTN = "BTN";
	private final String CURRENCYSYM_BWP = "BWP";
	private final String CURRENCYSYM_BYR = "BYR";
	private final String CURRENCYSYM_BZD = "BZD";
	private final String CURRENCYSYM_CAD = "CAD";
	private final String CURRENCYSYM_CHF = "CHF";
	private final String CURRENCYSYM_CLP = "CLP";
	private final String CURRENCYSYM_CNY = "CNY";
	private final String CURRENCYSYM_COP = "COP";
	private final String CURRENCYSYM_CRC = "CRC";
	private final String CURRENCYSYM_CUP = "CUP";
	private final String CURRENCYSYM_CVE = "CVE";
	private final String CURRENCYSYM_CZK = "CZK";
	private final String CURRENCYSYM_DJF = "DJF";
	private final String CURRENCYSYM_DKK = "DKK";
	private final String CURRENCYSYM_DOP = "DOP";
	private final String CURRENCYSYM_DZD = "DZD";
	private final String CURRENCYSYM_EEK = "EEK";
	private final String CURRENCYSYM_EGP = "EGP";
	private final String CURRENCYSYM_ETB = "ETB";
	private final String CURRENCYSYM_EUR = "EUR";
	private final String CURRENCYSYM_FJD = "FJD";
	private final String CURRENCYSYM_FKP = "FKP";
	private final String CURRENCYSYM_GBP = "GBP";
	private final String CURRENCYSYM_GIP = "GIP";
	private final String CURRENCYSYM_GMD = "GMD";
	private final String CURRENCYSYM_GNF = "GNF";
	private final String CURRENCYSYM_GTQ = "GTQ";
	private final String CURRENCYSYM_GYD = "GYD";
	private final String CURRENCYSYM_HKD = "HKD";
	private final String CURRENCYSYM_HNL = "HNL";
	private final String CURRENCYSYM_HRK = "HRK";
	private final String CURRENCYSYM_HTG = "HTG";
	private final String CURRENCYSYM_HUF = "HUF";
	private final String CURRENCYSYM_IDR = "IDR";
	private final String CURRENCYSYM_ILS = "ILS";
	private final String CURRENCYSYM_INR = "INR";
	private final String CURRENCYSYM_IQD = "IQD";
	private final String CURRENCYSYM_IRR = "IRR";
	private final String CURRENCYSYM_ISK = "ISK";
	private final String CURRENCYSYM_JMD = "JMD";
	private final String CURRENCYSYM_JOD = "JOD";
	private final String CURRENCYSYM_JPY = "JPY";
	private final String CURRENCYSYM_KES = "KES";
	private final String CURRENCYSYM_KHR = "KHR";
	private final String CURRENCYSYM_KMF = "KMF";
	private final String CURRENCYSYM_KPW = "KPW";
	private final String CURRENCYSYM_KRW = "KRW";
	private final String CURRENCYSYM_KWD = "KWD";
	private final String CURRENCYSYM_KYD = "KYD";
	private final String CURRENCYSYM_KZT = "KZT";
	private final String CURRENCYSYM_LAK = "LAK";
	private final String CURRENCYSYM_LBP = "LBP";
	private final String CURRENCYSYM_LKR = "LKR";
	private final String CURRENCYSYM_LRD = "LRD";
	private final String CURRENCYSYM_LSL = "LSL";
	private final String CURRENCYSYM_LTL = "LTL";
	private final String CURRENCYSYM_LVL = "LVL";
	private final String CURRENCYSYM_LYD = "LYD";
	private final String CURRENCYSYM_MAD = "MAD";
	private final String CURRENCYSYM_MDL = "MDL";
	private final String CURRENCYSYM_MKD = "MKD";
	private final String CURRENCYSYM_MMK = "MMK";
	private final String CURRENCYSYM_MNT = "MNT";
	private final String CURRENCYSYM_MOP = "MOP";
	private final String CURRENCYSYM_MRO = "MRO";
	private final String CURRENCYSYM_MUR = "MUR";
	private final String CURRENCYSYM_MVR = "MVR";
	private final String CURRENCYSYM_MWK = "MWK";
	private final String CURRENCYSYM_MXN = "MXN";
	private final String CURRENCYSYM_MYR = "MYR";
	private final String CURRENCYSYM_NAD = "NAD";
	private final String CURRENCYSYM_NGN = "NGN";
	private final String CURRENCYSYM_NIO = "NIO";
	private final String CURRENCYSYM_NOK = "NOK";
	private final String CURRENCYSYM_NPR = "NPR";
	private final String CURRENCYSYM_NZD = "NZD";
	private final String CURRENCYSYM_OMR = "OMR";
	private final String CURRENCYSYM_PAB = "PAB";
	private final String CURRENCYSYM_PEN = "PEN";
	private final String CURRENCYSYM_PGK = "PGK";
	private final String CURRENCYSYM_PHP = "PHP";
	private final String CURRENCYSYM_PKR = "PKR";
	private final String CURRENCYSYM_PLN = "PLN";
	private final String CURRENCYSYM_PYG = "PYG";
	private final String CURRENCYSYM_QAR = "QAR";
	private final String CURRENCYSYM_RON = "RON";
	private final String CURRENCYSYM_RUB = "RUB";
	private final String CURRENCYSYM_RWF = "RWF";
	private final String CURRENCYSYM_SAR = "SAR";
	private final String CURRENCYSYM_SBD = "SBD";
	private final String CURRENCYSYM_SCR = "SCR";
	private final String CURRENCYSYM_SEK = "SEK";
	private final String CURRENCYSYM_SGD = "SGD";
	private final String CURRENCYSYM_SHP = "SHP";
	private final String CURRENCYSYM_SKK = "SKK";
	private final String CURRENCYSYM_SLL = "SLL";
	private final String CURRENCYSYM_SOS = "SOS";
	private final String CURRENCYSYM_STD = "STD";
	private final String CURRENCYSYM_SVC = "SVC";
	private final String CURRENCYSYM_SYP = "SYP";
	private final String CURRENCYSYM_SZL = "SZL";
	private final String CURRENCYSYM_THB = "THB";
	private final String CURRENCYSYM_TND = "TND";
	private final String CURRENCYSYM_TOP = "TOP";
	private final String CURRENCYSYM_TRY = "TRY";
	private final String CURRENCYSYM_TTD = "TTD";
	private final String CURRENCYSYM_TWD = "TWD";
	private final String CURRENCYSYM_TZS = "TZS";
	private final String CURRENCYSYM_UAH = "UAH";
	private final String CURRENCYSYM_UGX = "UGX";
	private final String CURRENCYSYM_USD = "USD";
	private final String CURRENCYSYM_UYU = "UYU";
	private final String CURRENCYSYM_VND = "VND";
	private final String CURRENCYSYM_VUV = "VUV";
	private final String CURRENCYSYM_WST = "WST";
	private final String CURRENCYSYM_XAF = "XAF";
	private final String CURRENCYSYM_XAG = "XAG";
	private final String CURRENCYSYM_XAU = "XAU";
	private final String CURRENCYSYM_XCD = "XCD";
	private final String CURRENCYSYM_XCP = "XCP";
	private final String CURRENCYSYM_XOF = "XOF";
	private final String CURRENCYSYM_XPD = "XPD";
	private final String CURRENCYSYM_XPF = "XPF";
	private final String CURRENCYSYM_XPT = "XPT";
	private final String CURRENCYSYM_YER = "YER";
	private final String CURRENCYSYM_ZAR = "ZAR";
	private final String CURRENCYSYM_ZMK = "ZMK";
	private final String CURRENCYSYM_ZWD = "ZWD";
	
	//TODO
	//TODO
	//TODO
	public ArrayList<String> listSalaryTypes, listAvailability, listEducation, listGender, listMaritalStatuses, listYearGraduated,
							 listNationality, listLicense, listRelocate, listEthnicity, listLanguages, listCurrency;
	public LinkedHashMap<Integer, String> referrers;
	
	public UserChoicesFactory(){
		referrers = new LinkedHashMap<Integer, String>();
		listSalaryTypes = new ArrayList<String>();
		listSalaryTypes.add(USER_SALARYTYPE_NOTSELECTED);
		listSalaryTypes.add(USER_SALARYTYPE_ANNUALLY);
		listSalaryTypes.add(USER_SALARYTYPE_MONTHLY);
		listSalaryTypes.add(USER_SALARYTYPE_WEEKLY);
		listSalaryTypes.add(USER_SALARYTYPE_DAILY);
		listSalaryTypes.add(USER_SALARYTYPE_HOURLY);
		
		listAvailability = new ArrayList<String>();
		listAvailability.add(USER_AVAIL_NOTSELECTED);
		listAvailability.add(USER_AVAIL_NOW);
		listAvailability.add(USER_AVAIL_1W);
		listAvailability.add(USER_AVAIL_2W);
		listAvailability.add(USER_AVAIL_3W);
		listAvailability.add(USER_AVAIL_4W);
		listAvailability.add(USER_AVAIL_1M);
		listAvailability.add(USER_AVAIL_2M);
		listAvailability.add(USER_AVAIL_3MPLUS);
		
		listEducation = new ArrayList<String>();
		listEducation.add(USER_EDUC_NONE);
		listEducation.add(USER_EDUC_NOFORMAL);
		listEducation.add(USER_EDUC_ALEVEL);
		listEducation.add(USER_EDUC_VOCATIONAL);
		listEducation.add(USER_EDUC_APPRENTICE);
		listEducation.add(USER_EDUC_TRADE);
		listEducation.add(USER_EDUC_COLLEGE);
		listEducation.add(USER_EDUC_HIGHERCERT);
		listEducation.add(USER_EDUC_HIGHERDIPL);
		listEducation.add(USER_EDUC_PASSBACHELORS);
		listEducation.add(USER_EDUC_CLASS3BACHELORS);
		listEducation.add(USER_EDUC_CLASS2LBACHERLORS);
		listEducation.add(USER_EDUC_CLASS2UBACHELORS);
		listEducation.add(USER_EDUC_CLASS1BACHELORS);
		listEducation.add(USER_EDUC_DEGREEH);
		listEducation.add(USER_EDUC_DEGREEM);
		listEducation.add(USER_EDUC_PROFESSIONAL);
		listEducation.add(USER_EDUC_MBA);
		listEducation.add(USER_EDUC_DOCTORATE);
		
		listGender = new ArrayList<String>();
		listGender.add(USER_GENDER_U);
		listGender.add(USER_GENDER_M);
		listGender.add(USER_GENDER_F);
		
		listMaritalStatuses = new ArrayList<String>();
		listMaritalStatuses.add(USER_MS_UNKNOWN);
		listMaritalStatuses.add(USER_MS_SINGLE);
		listMaritalStatuses.add(USER_MS_MARRIED);
		listMaritalStatuses.add(USER_MS_DIVORCED);
		listMaritalStatuses.add(USER_MS_WIDOWED);
		listMaritalStatuses.add(USER_MS_COHABITING);
		listMaritalStatuses.add(USER_MS_CIVILUNION);
		listMaritalStatuses.add(USER_MS_PARTNERSHIP);
		
		listYearGraduated = new ArrayList<String>();
		SimpleDateFormat yearFormat = new SimpleDateFormat("yyyy");
		for(int i=1940; i<Integer.parseInt(yearFormat.format(new Date())); i++)
			listYearGraduated.add(String.valueOf(i));
		
		listNationality = new ArrayList<String>();
		listNationality.add(USER_NAT_NONE); 
		listNationality.add(USER_NAT_ALGERIAN); 
		listNationality.add(USER_NAT_AMERICAN); 
		listNationality.add(USER_NAT_ANGOLIAN); 
		listNationality.add(USER_NAT_ARABIC); 
		listNationality.add(USER_NAT_ARGENTINIAN); 
		listNationality.add(USER_NAT_AUSTRALIAN); 
		listNationality.add(USER_NAT_BELGIAN); 
		listNationality.add(USER_NAT_BOSNIAN); 
		listNationality.add(USER_NAT_BRITISH); 
		listNationality.add(USER_NAT_BULGARIAN); 
		listNationality.add(USER_NAT_CANADIAN); 
		listNationality.add(USER_NAT_CHILEAN); 
		listNationality.add(USER_NAT_CHINESE); 
		listNationality.add(USER_NAT_COLUMBIAN); 
		listNationality.add(USER_NAT_CROATIAN); 
		listNationality.add(USER_NAT_DANISH); 
		listNationality.add(USER_NAT_DUTCH); 
		listNationality.add(USER_NAT_EGYPTIAN); 
		listNationality.add(USER_NAT_FILIPINO); 
		listNationality.add(USER_NAT_FRENCH); 
		listNationality.add(USER_NAT_GERMAN); 
		listNationality.add(USER_NAT_GHANADIAN); 
		listNationality.add(USER_NAT_GREEK); 
		listNationality.add(USER_NAT_INDIAN); 
		listNationality.add(USER_NAT_INDONESIAN); 
		listNationality.add(USER_NAT_IRANIAN); 
		listNationality.add(USER_NAT_IRISH); 
		listNationality.add(USER_NAT_ITALIAN); 
		listNationality.add(USER_NAT_KAZAKH); 
		listNationality.add(USER_NAT_KOREAN); 
		listNationality.add(USER_NAT_KUWAITI); 
		listNationality.add(USER_NAT_KYRGYZSTANI); 
		listNationality.add(USER_NAT_LITHUANIAN); 
		listNationality.add(USER_NAT_MALAYSIAN); 
		listNationality.add(USER_NAT_MAURITIAN); 
		listNationality.add(USER_NAT_MEXICAN); 
		listNationality.add(USER_NAT_MONGOLIAN); 
		listNationality.add(USER_NAT_NEWZEALAND); 
		listNationality.add(USER_NAT_NIGERIAN); 
		listNationality.add(USER_NAT_NORWEGIAN); 
		listNationality.add(USER_NAT_OTHER); 
		listNationality.add(USER_NAT_PAKISTANI); 
		listNationality.add(USER_NAT_PAPUANEWGUINEAN); 
		listNationality.add(USER_NAT_PERUVIAN); 
		listNationality.add(USER_NAT_POLISH); 
		listNationality.add(USER_NAT_PORTUGUESE); 
		listNationality.add(USER_NAT_ROMANIAN); 
		listNationality.add(USER_NAT_RUSSIAN); 
		listNationality.add(USER_NAT_SAUDIARABIAN); 
		listNationality.add(USER_NAT_SCOTTISH); 
		listNationality.add(USER_NAT_SERBIAN); 
		listNationality.add(USER_NAT_SOUTHAFRICAN); 
		listNationality.add(USER_NAT_SOUTHAMERICAN);
		listNationality.add(USER_NAT_SPANISH); 
		listNationality.add(USER_NAT_SRILANKAN); 
		listNationality.add(USER_NAT_SWEDISH); 
		listNationality.add(USER_NAT_SWISS); 
		listNationality.add(USER_NAT_SYRIAN); 
		listNationality.add(USER_NAT_THAI); 
		listNationality.add(USER_NAT_TURKISH); 
		listNationality.add(USER_NAT_VENEZUELAN); 
		listNationality.add(USER_NAT_WELSH);
		
		listLicense = new ArrayList<String>();
		listLicense.add(USER_UNKNOWN);
		listLicense.add(USER_NO);
		listLicense.add(USER_YES);
		
		listRelocate = new ArrayList<String>();
		listRelocate.add(USER_UNKNOWN);
		listRelocate.add(USER_NO);
		listRelocate.add(USER_YES);
		listRelocate.add(USER_MAYBE);
		
		listEthnicity = new ArrayList<String>();
		listEthnicity.add(USER_ETH_NONE);
		listEthnicity.add(USER_ETH_WHITEENGLISH);
		listEthnicity.add(USER_ETH_WHITEIRISH);
		listEthnicity.add(USER_ETH_ANYOTHERWHITEBG);
		listEthnicity.add(USER_ETH_WHITEANDBLACKCARIBBEAN);
		listEthnicity.add(USER_ETH_WHITEANDBLACKAFRICAN);
		listEthnicity.add(USER_ETH_WHITEANDASIAN);
		listEthnicity.add(USER_ETH_ANYOTHERMIXEDBG);
		listEthnicity.add(USER_ETH_INDIAN);
		listEthnicity.add(USER_ETH_PAKISTANI);
		listEthnicity.add(USER_ETH_AFRICAN);
		listEthnicity.add(USER_ETH_ANYOTHERBLACKBG);
		listEthnicity.add(USER_ETH_CHINESE);
		listEthnicity.add(USER_ETH_ANYOTHERETHNIC);
		
		listLanguages = new ArrayList<String>();
		listLanguages.add(USER_LANG_AFRIKAANS); 
		listLanguages.add(USER_LANG_ALGERIAN); 
		listLanguages.add(USER_LANG_ARABIC); 
		listLanguages.add(USER_LANG_AWADHI); 
		listLanguages.add(USER_LANG_AZERBAIJANI); 
		listLanguages.add(USER_LANG_BENGALI); 
		listLanguages.add(USER_LANG_BHOJPURI); 
		listLanguages.add(USER_LANG_BOSNIAN); 
		listLanguages.add(USER_LANG_BURMESE); 
		listLanguages.add(USER_LANG_CHILEAN); 
		listLanguages.add(USER_LANG_CHINESE); 
		listLanguages.add(USER_LANG_CROATIAN); 
		listLanguages.add(USER_LANG_DANISH); 
		listLanguages.add(USER_LANG_DUTCH); 
		listLanguages.add(USER_LANG_ENGLISH); 
		listLanguages.add(USER_LANG_FILIPINO); 
		listLanguages.add(USER_LANG_FRENCH); 
		listLanguages.add(USER_LANG_GERMAN); 
		listLanguages.add(USER_LANG_GREEK); 
		listLanguages.add(USER_LANG_GUJARATI); 
		listLanguages.add(USER_LANG_HAUSA); 
		listLanguages.add(USER_LANG_HINDI); 
		listLanguages.add(USER_LANG_INDONESIANBAHASA); 
		listLanguages.add(USER_LANG_ITALIAN); 
		listLanguages.add(USER_LANG_JAPANESE); 
		listLanguages.add(USER_LANG_JAVANESE); 
		listLanguages.add(USER_LANG_KANNADA); 
		listLanguages.add(USER_LANG_KAZAKH); 
		listLanguages.add(USER_LANG_KOREAN); 
		listLanguages.add(USER_LANG_MAITHILI); 
		listLanguages.add(USER_LANG_MALAYALAM); 
		listLanguages.add(USER_LANG_MALAYSIANBAHASA);
		listLanguages.add(USER_LANG_MARATHI); 
		listLanguages.add(USER_LANG_MONGOLIAN); 
		listLanguages.add(USER_LANG_ORIYA); 
		listLanguages.add(USER_LANG_PANJABI); 
		listLanguages.add(USER_LANG_PAPUANLANGUAGES); 
		listLanguages.add(USER_LANG_PERSIAN); 
		listLanguages.add(USER_LANG_POLISH); 
		listLanguages.add(USER_LANG_PORTUGUESE); 
		listLanguages.add(USER_LANG_ROMANIAN); 
		listLanguages.add(USER_LANG_RUSSIAN); 
		listLanguages.add(USER_LANG_SERBIAN); 
		listLanguages.add(USER_LANG_SINDHI); 
		listLanguages.add(USER_LANG_SPANISH); 
		listLanguages.add(USER_LANG_SUNDA); 
		listLanguages.add(USER_LANG_TAMIL); 
		listLanguages.add(USER_LANG_TELUGU); 
		listLanguages.add(USER_LANG_THAI); 
		listLanguages.add(USER_LANG_TOKPISIN); 
		listLanguages.add(USER_LANG_TURKISH); 
		listLanguages.add(USER_LANG_UKRAINIAN);
		listLanguages.add(USER_LANG_URDU); 
		listLanguages.add(USER_LANG_VIETNAMESE); 
		listLanguages.add(USER_LANG_YORUBA);
		
		listCurrency = new ArrayList<String>();
		listCurrency.add(CURRENCY_UAEDIRHAM); 
		listCurrency.add(CURRENCY_ALBANIANLEK); 
		listCurrency.add(CURRENCY_NETHANTILLESGUILDER); 
		listCurrency.add(CURRENCY_ARGENTINEPESO); 
		listCurrency.add(CURRENCY_AUSTRALIANDOLLAR); 
		listCurrency.add(CURRENCY_ARUBAFLORIN); 
		listCurrency.add(CURRENCY_BARBADOSDOLLAR); 
		listCurrency.add(CURRENCY_BANGLADESHTAKA); 
		listCurrency.add(CURRENCY_BULGARIANLEV); 
		listCurrency.add(CURRENCY_BAHRAINIDINAR); 
		listCurrency.add(CURRENCY_BURUNDIFRANC); 
		listCurrency.add(CURRENCY_BERMUDADOLLAR); 
		listCurrency.add(CURRENCY_BRUNEIDOLLAR); 
		listCurrency.add(CURRENCY_BOLIVIANBOLIVIANO); 
		listCurrency.add(CURRENCY_BRAZILIANREAL); 
		listCurrency.add(CURRENCY_BAHAMIANDOLLAR); 
		listCurrency.add(CURRENCY_BHUTANNGULTRUM); 
		listCurrency.add(CURRENCY_BOTSWANAPULA); 
		listCurrency.add(CURRENCY_BELARUSRUBLE); 
		listCurrency.add(CURRENCY_BELIZEDOLLAR); 
		listCurrency.add(CURRENCY_CANADIANDOLLAR); 
		listCurrency.add(CURRENCY_SWISSFRANC); 
		listCurrency.add(CURRENCY_CHILEANPESO); 
		listCurrency.add(CURRENCY_CHINESEYUAN); 
		listCurrency.add(CURRENCY_COLOMBIANPESO); 
		listCurrency.add(CURRENCY_COSTARICACOLON); 
		listCurrency.add(CURRENCY_CUBANPESO); 
		listCurrency.add(CURRENCY_CAPEVERDEESCUDO); 
		listCurrency.add(CURRENCY_CZECHKORUNA); 
		listCurrency.add(CURRENCY_DIJIBOUTIFRANC); 
		listCurrency.add(CURRENCY_DANISHKRONE); 
		listCurrency.add(CURRENCY_DOMINICANPESO); 
		listCurrency.add(CURRENCY_ALGERIANDINAR); 
		listCurrency.add(CURRENCY_ESTONIANKROON); 
		listCurrency.add(CURRENCY_EGYPTIANPOUND); 
		listCurrency.add(CURRENCY_ETHIOPIANBIRR); 
		listCurrency.add(CURRENCY_EURO); 
		listCurrency.add(CURRENCY_FIJIDOLLAR); 
		listCurrency.add(CURRENCY_FALKLANDISLANDSPOUND); 
		listCurrency.add(CURRENCY_BRITISHPOUND); 
		listCurrency.add(CURRENCY_GIBRALTARPOUND); 
		listCurrency.add(CURRENCY_GAMBIANDALASI); 
		listCurrency.add(CURRENCY_GUINEAFRANC); 
		listCurrency.add(CURRENCY_GUATEMALAQUETZAL); 
		listCurrency.add(CURRENCY_GUYANADOLLAR); 
		listCurrency.add(CURRENCY_HONGKONGDOLLAR); 
		listCurrency.add(CURRENCY_HONDURASLEMPIRA); 
		listCurrency.add(CURRENCY_CROATIANKUNA); 
		listCurrency.add(CURRENCY_HAITIGOURDE); 
		listCurrency.add(CURRENCY_HUNGARIANFORINT); 
		listCurrency.add(CURRENCY_INDONESIANRUPIAH); 
		listCurrency.add(CURRENCY_ISRAELISHEKEL); 
		listCurrency.add(CURRENCY_INDIANRUPEE); 
		listCurrency.add(CURRENCY_IRAQIDINAR); 
		listCurrency.add(CURRENCY_IRANRIAL); 
		listCurrency.add(CURRENCY_ICELANDKRONA); 
		listCurrency.add(CURRENCY_JAMAICANDOLLAR); 
		listCurrency.add(CURRENCY_JORDANIANDINAR); 
		listCurrency.add(CURRENCY_JAPANESEYEN); 
		listCurrency.add(CURRENCY_KENYANSHILLING); 
		listCurrency.add(CURRENCY_CAMBODIARIEL); 
		listCurrency.add(CURRENCY_COMOROSFRANC); 
		listCurrency.add(CURRENCY_NORTHKOREANWON); 
		listCurrency.add(CURRENCY_KOREANWON); 
		listCurrency.add(CURRENCY_KUWAITIDINAR); 
		listCurrency.add(CURRENCY_CAYMANISLANDSDOLLAR); 
		listCurrency.add(CURRENCY_KAZAKHSTANTENGE); 
		listCurrency.add(CURRENCY_LAOKIP); 
		listCurrency.add(CURRENCY_LEBANESEPOUND); 
		listCurrency.add(CURRENCY_SRILANKARUPEE); 
		listCurrency.add(CURRENCY_LIBERIANDOLLAR); 
		listCurrency.add(CURRENCY_LESOTHOLOTI); 
		listCurrency.add(CURRENCY_LITHUANIANLITA); 
		listCurrency.add(CURRENCY_LATVIANLAT); 
		listCurrency.add(CURRENCY_LIBYANDINAR); 
		listCurrency.add(CURRENCY_MOROCCANDIRHAM); 
		listCurrency.add(CURRENCY_MOLDOVANLEU); 
		listCurrency.add(CURRENCY_MACEDONIANDENAR); 
		listCurrency.add(CURRENCY_MYANMARKYAT); 
		listCurrency.add(CURRENCY_MONGOLIANTUGRIK); 
		listCurrency.add(CURRENCY_MACAUPATACA); 
		listCurrency.add(CURRENCY_MAURITANIAOUGULYA);
		listCurrency.add(CURRENCY_MAURITIUSRUPEE); 
		listCurrency.add(CURRENCY_MALDIVESRUFIYAA); 
		listCurrency.add(CURRENCY_MALAWIKWACHA); 
		listCurrency.add(CURRENCY_MEXICANPESO); 
		listCurrency.add(CURRENCY_MALAYSIANRINGGIT); 
		listCurrency.add(CURRENCY_NAMIBIANDOLLAR); 
		listCurrency.add(CURRENCY_NIGERIANNAIRA);
		listCurrency.add(CURRENCY_NICARAGUACORDOBA); 
		listCurrency.add(CURRENCY_NORWEGIANKRONE); 
		listCurrency.add(CURRENCY_NEPALESERUPEE); 
		listCurrency.add(CURRENCY_NEWZEALANDDOLLAR); 
		listCurrency.add(CURRENCY_OMANIRIAL); 
		listCurrency.add(CURRENCY_PANAMABALBOA); 
		listCurrency.add(CURRENCY_PERUVIANNUEVOSOL); 
		listCurrency.add(CURRENCY_PAPUANEWGUINEAKINA); 
		listCurrency.add(CURRENCY_PHILIPPINEPESO); 
		listCurrency.add(CURRENCY_PAKISTANIRUPEE); 
		listCurrency.add(CURRENCY_POLISHZLOTY); 
		listCurrency.add(CURRENCY_PARAGUAYANGUARANI); 
		listCurrency.add(CURRENCY_QATARRIAL); 
		listCurrency.add(CURRENCY_ROMANIANNEWLEU); 
		listCurrency.add(CURRENCY_RUSSIANROUBLE); 
		listCurrency.add(CURRENCY_RWANDAFRANC); 
		listCurrency.add(CURRENCY_SAUDIARABIANRIYAL); 
		listCurrency.add(CURRENCY_SOLOMONISLANDSDOLLAR); 
		listCurrency.add(CURRENCY_SEYCHELLESRUPEE); 
		listCurrency.add(CURRENCY_SWEDISHKRONA); 
		listCurrency.add(CURRENCY_SINGAPOREDOLLAR); 
		listCurrency.add(CURRENCY_STHELENAPOUND); 
		listCurrency.add(CURRENCY_SLOVAKKORUNA); 
		listCurrency.add(CURRENCY_SIERRALEONELEONE); 
		listCurrency.add(CURRENCY_SOMALISHILLING); 
		listCurrency.add(CURRENCY_SAOTOMEDOBRA); 
		listCurrency.add(CURRENCY_ELSALVADORCOLON); 
		listCurrency.add(CURRENCY_SYRIANPOUND); 
		listCurrency.add(CURRENCY_SWAZILANDLILAGENI); 
		listCurrency.add(CURRENCY_THAIBAHT); 
		listCurrency.add(CURRENCY_TUNISIANDINAR); 
		listCurrency.add(CURRENCY_TONGAPAANGA); 
		listCurrency.add(CURRENCY_TURKISHLIRA); 
		listCurrency.add(CURRENCY_TRINIDADTOBAGODOLLAR); 
		listCurrency.add(CURRENCY_TAIWANDOLLAR); 
		listCurrency.add(CURRENCY_TANZANIANSHILLING); 
		listCurrency.add(CURRENCY_UKRAINEHRYVNIA); 
		listCurrency.add(CURRENCY_UGANDANSHILLING); 
		listCurrency.add(CURRENCY_USDOLLAR); 
		listCurrency.add(CURRENCY_URUGUAYANNEWPESO); 
		listCurrency.add(CURRENCY_VIETNAMDONG); 
		listCurrency.add(CURRENCY_VANUATUVATU); 
		listCurrency.add(CURRENCY_SAMOATALA); 
		listCurrency.add(CURRENCY_SILVEROUNCES); 
		listCurrency.add(CURRENCY_GOLDOUNCES); 
		listCurrency.add(CURRENCY_EASTCARIBBEANDOLLAR); 
		listCurrency.add(CURRENCY_COPPERPOUNDS); 
		listCurrency.add(CURRENCY_CFAFRANC); 
		listCurrency.add(CURRENCY_PALLADIUMOUNCES); 
		listCurrency.add(CURRENCY_PACIFICFRANC); 
		listCurrency.add(CURRENCY_PLATINUMOUNCES); 
		listCurrency.add(CURRENCY_YEMENRIYAL); 
		listCurrency.add(CURRENCY_SOUTHAFRICANRAND); 
		listCurrency.add(CURRENCY_ZAMBIANKWACHA); 
		listCurrency.add(CURRENCY_ZIMBABWEDOLLAR);
	}
	
	public void setReferrers(LinkedHashMap<Integer, String> referrers){
		this.referrers.clear();
		this.referrers.putAll(referrers);
	}
	
	public LinkedHashMap<Integer, String> getReferrers(){
		return referrers;
	}
		
	public ArrayList<String> getSalaryTypes(){
		return listSalaryTypes;
	}
	
	public int getSalaryKeyForDesc(String desc){
		if(desc.equals(USER_SALARYTYPE_ANNUALLY)) return 1;
		else if(desc.equals(USER_SALARYTYPE_DAILY)) return 2;
		else if(desc.equals(USER_SALARYTYPE_HOURLY)) return 3;
		else if(desc.equals(USER_SALARYTYPE_WEEKLY)) return 4;
		else if(desc.equals(USER_SALARYTYPE_MONTHLY)) return 5;
		else return 0;
	}
	
	public String getSalaryDescForKey(int key){
		if(key == 1) return USER_SALARYTYPE_ANNUALLY;
		else if (key == 2) return USER_SALARYTYPE_DAILY;
		else if(key == 3) return USER_SALARYTYPE_HOURLY;
		else if(key == 4) return USER_SALARYTYPE_WEEKLY;
		else if(key == 5)return USER_SALARYTYPE_MONTHLY;
		else return USER_SALARYTYPE_NOTSELECTED;
	}
	
	public int getSalaryPosForKey(int key){
		return listSalaryTypes.indexOf(getSalaryDescForKey(key));
	}
	
	public ArrayList<String> getAvailabilityList(){
		return listAvailability;
	}
	
	public int getAvailKeyForDesc(String desc){
		if(desc.equals(USER_AVAIL_1W)) return 1;
		else if(desc.equals(USER_AVAIL_2W)) return 2;
		else if(desc.equals(USER_AVAIL_3W)) return 3;
		else if(desc.equals(USER_AVAIL_4W)) return 4;
		else if(desc.equals(USER_AVAIL_1M)) return 11;
		else if(desc.equals(USER_AVAIL_2M)) return 12;
		else if(desc.equals(USER_AVAIL_3MPLUS)) return 13;
		else if(desc.equals(USER_AVAIL_NOW)) return 99;
		else return 0;
	}
	
	public String getAvailDescForKey(int key){
		if(key == 1) return USER_AVAIL_1W;
		else if(key == 2) return USER_AVAIL_2W;
		else if(key == 3) return USER_AVAIL_3W;
		else if(key == 4) return USER_AVAIL_4W;
		else if(key == 11) return USER_AVAIL_1M;
		else if(key == 12) return USER_AVAIL_2M;
		else if(key == 13) return USER_AVAIL_3MPLUS;
		else if(key == 99) return USER_AVAIL_NOW;
		else return USER_AVAIL_NOTSELECTED;		
	}
	
	public int getAvailPosForKey(int key){
		return listAvailability.indexOf(getAvailDescForKey(key));
	}
	
	public ArrayList<String> getEducationList(){
		return listEducation;
	}
	
	public int getEducKeyForDesc(String desc){
		if(desc.equals(USER_EDUC_NOFORMAL)) return 1;
		else if(desc.equals(USER_EDUC_ALEVEL)) return 2;
		else if(desc.equals(USER_EDUC_VOCATIONAL)) return 3;
		else if(desc.equals(USER_EDUC_APPRENTICE)) return 4;
		else if(desc.equals(USER_EDUC_TRADE)) return 5;
		else if(desc.equals(USER_EDUC_COLLEGE)) return 6;
		else if(desc.equals(USER_EDUC_HIGHERCERT)) return 7;
		else if(desc.equals(USER_EDUC_HIGHERDIPL)) return 8;
		else if(desc.equals(USER_EDUC_PASSBACHELORS)) return 9;
		else if(desc.equals(USER_EDUC_CLASS3BACHELORS)) return 10;
		else if(desc.equals(USER_EDUC_CLASS2LBACHERLORS)) return 11;
		else if(desc.equals(USER_EDUC_CLASS2UBACHELORS)) return 12;
		else if(desc.equals(USER_EDUC_CLASS1BACHELORS)) return 13;
		else if(desc.equals(USER_EDUC_DEGREEH)) return 14;
		else if(desc.equals(USER_EDUC_DEGREEM)) return 15;
		else if(desc.equals(USER_EDUC_PROFESSIONAL)) return 16;
		else if(desc.equals(USER_EDUC_MBA)) return 17;
		else if(desc.equals(USER_EDUC_DOCTORATE)) return 18;	
		else return 0;
	}
	
	public String getEducDescForKey(int key){
		if(key == 1) return USER_EDUC_NOFORMAL;
		else if(key == 2) return USER_EDUC_ALEVEL;
		else if(key == 3) return USER_EDUC_VOCATIONAL;
		else if(key == 4) return USER_EDUC_APPRENTICE;
		else if(key == 5) return USER_EDUC_TRADE;
		else if(key == 6) return USER_EDUC_COLLEGE;
		else if(key == 7) return USER_EDUC_HIGHERCERT;
		else if(key == 8) return USER_EDUC_HIGHERDIPL;
		else if(key == 9) return USER_EDUC_PASSBACHELORS;
		else if(key == 10) return USER_EDUC_CLASS3BACHELORS;
		else if(key == 11) return USER_EDUC_CLASS2LBACHERLORS;
		else if(key == 12) return USER_EDUC_CLASS2UBACHELORS;
		else if(key == 13) return USER_EDUC_CLASS1BACHELORS;
		else if(key == 14) return USER_EDUC_DEGREEH;
		else if(key == 15) return USER_EDUC_DEGREEM;
		else if(key == 16) return USER_EDUC_PROFESSIONAL;
		else if(key == 17) return USER_EDUC_MBA;
		else if(key == 18) return USER_EDUC_DOCTORATE;
		else return USER_EDUC_NONE;
	}
	
	public int getEducPositionWithKey(int key){
		return listEducation.indexOf(getEducDescForKey(key));
	}
		
	public ArrayList<String> getGenderList(){
		return listGender;
	}
	
	public int getGenderPosForKey(int key){
		return listGender.indexOf(getGenderDescForKey(key));
	}
	
	public int getGenderKeyForDesc(String desc){
		if(desc.equals(USER_GENDER_M)) return 1;
		else if(desc.equals(USER_GENDER_F)) return 2;
		else return 9;
	}
	
	public String getGenderDescForKey(int key){
		if(key == 1) return USER_GENDER_M;
		else if(key == 2) return USER_GENDER_F;
		else return USER_GENDER_U;
	}
	
	public ArrayList<String> getMaritalStatuses(){
		return listMaritalStatuses;
	}
	
	public int getMarStatKeyForDesc(String desc){
		if(desc.equals(USER_MS_SINGLE)) return 1; 
		else if(desc.equals(USER_MS_MARRIED)) return 2; 
		else if(desc.equals(USER_MS_DIVORCED)) return 3; 
		else if(desc.equals(USER_MS_WIDOWED)) return 4; 
		else if(desc.equals(USER_MS_COHABITING)) return 5; 
		else if(desc.equals(USER_MS_CIVILUNION)) return 6; 
		else if(desc.equals(USER_MS_PARTNERSHIP)) return 7;
		else return 0;
	}
	
	public String getMarStatDescForKey(int key){
		if(key == 1) return USER_MS_SINGLE;
		else if(key == 2) return USER_MS_MARRIED;
		else if(key == 3) return USER_MS_DIVORCED;
		else if(key == 4) return USER_MS_WIDOWED;
		else if(key == 5) return USER_MS_COHABITING;
		else if(key == 6) return USER_MS_CIVILUNION;
		else if(key == 7) return USER_MS_PARTNERSHIP;
		else return USER_MS_UNKNOWN;
	}
	
	public int getMarStatPosForKey(int key){
		return listMaritalStatuses.indexOf(getMarStatDescForKey(key));
	}
	
	public ArrayList<String> getYearGraduatedList(){
		return listYearGraduated;
	}
	
	public ArrayList<String> getNationalities(){
		return listNationality;
	}
	
	public int getNatKeyForDesc(String desc){
		if(desc.equals(USER_NAT_NONE)) return 0;
		if(desc.equals(USER_NAT_ALGERIAN)) return 3;
        else if(desc.equals(USER_NAT_AMERICAN)) return 4;
        else if(desc.equals(USER_NAT_ANGOLIAN)) return 54;
        else if(desc.equals(USER_NAT_ARABIC)) return 27;
        else if(desc.equals(USER_NAT_ARGENTINIAN)) return 28;
        else if(desc.equals(USER_NAT_AUSTRALIAN)) return 5;
        else if(desc.equals(USER_NAT_BELGIAN)) return 6;
        else if(desc.equals(USER_NAT_BOSNIAN)) return 58;
        else if(desc.equals(USER_NAT_BRITISH)) return 7;
        else if(desc.equals(USER_NAT_BULGARIAN)) return 43;
        else if(desc.equals(USER_NAT_CANADIAN)) return 8;
        else if(desc.equals(USER_NAT_CHILEAN)) return 56;
        else if(desc.equals(USER_NAT_CHINESE)) return 9;
        else if(desc.equals(USER_NAT_COLUMBIAN)) return 29;
        else if(desc.equals(USER_NAT_CROATIAN)) return 54;
        else if(desc.equals(USER_NAT_DANISH)) return 55;
        else if(desc.equals(USER_NAT_DUTCH)) return 10;
        else if(desc.equals(USER_NAT_EGYPTIAN)) return 11;
        else if(desc.equals(USER_NAT_FILIPINO)) return 40;
        else if(desc.equals(USER_NAT_FRENCH)) return 12;
        else if(desc.equals(USER_NAT_GERMAN)) return 13;
        else if(desc.equals(USER_NAT_GHANADIAN)) return 49;
        else if(desc.equals(USER_NAT_GREEK)) return 14;
        else if(desc.equals(USER_NAT_INDIAN)) return 15;
        else if(desc.equals(USER_NAT_INDONESIAN)) return 50;
        else if(desc.equals(USER_NAT_IRANIAN)) return 30;
        else if(desc.equals(USER_NAT_IRISH)) return 16;
        else if(desc.equals(USER_NAT_ITALIAN)) return 17;
        else if(desc.equals(USER_NAT_KAZAKH)) return 60;
        else if(desc.equals(USER_NAT_KOREAN)) return 64;
        else if(desc.equals(USER_NAT_KUWAITI)) return 31;
        else if(desc.equals(USER_NAT_KYRGYZSTANI)) return 63;
        else if(desc.equals(USER_NAT_LITHUANIAN)) return 18;
        else if(desc.equals(USER_NAT_MALAYSIAN)) return 32;
        else if(desc.equals(USER_NAT_MAURITIAN)) return 39;
        else if(desc.equals(USER_NAT_MEXICAN)) return 41;
        else if(desc.equals(USER_NAT_MONGOLIAN)) return 61;
        else if(desc.equals(USER_NAT_NEWZEALAND)) return 37;
        else if(desc.equals(USER_NAT_NIGERIAN)) return 44;
        else if(desc.equals(USER_NAT_NORWEGIAN)) return 19;
        else if(desc.equals(USER_NAT_PAKISTANI)) return 33;
        else if(desc.equals(USER_NAT_PAPUANEWGUINEAN)) return 62;
        else if(desc.equals(USER_NAT_PERUVIAN)) return 34;
        else if(desc.equals(USER_NAT_POLISH)) return 20;
        else if(desc.equals(USER_NAT_PORTUGUESE)) return 47;
        else if(desc.equals(USER_NAT_ROMANIAN)) return 21;
        else if(desc.equals(USER_NAT_RUSSIAN)) return 46;
        else if(desc.equals(USER_NAT_SAUDIARABIAN)) return 52;
        else if(desc.equals(USER_NAT_SCOTTISH)) return 22;
        else if(desc.equals(USER_NAT_SERBIAN)) return 59;
        else if(desc.equals(USER_NAT_SOUTHAFRICAN)) return 23;
        else if(desc.equals(USER_NAT_SOUTHAMERICAN)) return 35;
        else if(desc.equals(USER_NAT_SPANISH)) return 24;
        else if(desc.equals(USER_NAT_SRILANKAN)) return 38;
        else if(desc.equals(USER_NAT_SWEDISH)) return 36;
        else if(desc.equals(USER_NAT_SWISS)) return 25;
        else if(desc.equals(USER_NAT_SYRIAN)) return 48;
        else if(desc.equals(USER_NAT_THAI)) return 45;
        else if(desc.equals(USER_NAT_TURKISH)) return 51;
        else if(desc.equals(USER_NAT_VENEZUELAN)) return 42;
        else if(desc.equals(USER_NAT_WELSH)) return 26;
        else return 99;
	}
	
	public String getNatDescForKey(int key){
		if(key == 0) return USER_NAT_NONE;
		else if(key == 3) return USER_NAT_ALGERIAN;
        else if(key == 4) return USER_NAT_AMERICAN;
        else if(key == 54) return USER_NAT_ANGOLIAN;
        else if(key == 27) return USER_NAT_ARABIC;
        else if(key == 28) return USER_NAT_ARGENTINIAN;
        else if(key == 5) return USER_NAT_AUSTRALIAN;
        else if(key == 6) return USER_NAT_BELGIAN;
        else if(key == 58) return USER_NAT_BOSNIAN;
        else if(key == 7) return USER_NAT_BRITISH;
        else if(key == 43) return USER_NAT_BULGARIAN;
        else if(key == 8) return USER_NAT_CANADIAN;
        else if(key == 56) return USER_NAT_CHILEAN;
        else if(key == 9) return USER_NAT_CHINESE;
        else if(key == 29) return USER_NAT_COLUMBIAN;
        else if(key == 54) return USER_NAT_CROATIAN;
        else if(key == 55) return USER_NAT_DANISH;
        else if(key == 10) return USER_NAT_DUTCH;
        else if(key == 11) return USER_NAT_EGYPTIAN;
        else if(key == 40) return USER_NAT_FILIPINO;
        else if(key == 12) return USER_NAT_FRENCH;
        else if(key == 13) return USER_NAT_GERMAN;
        else if(key == 49) return USER_NAT_GHANADIAN;
        else if(key == 14) return USER_NAT_GREEK;
        else if(key == 15) return USER_NAT_INDIAN;
        else if(key == 50) return USER_NAT_INDONESIAN;
        else if(key == 30) return USER_NAT_IRANIAN;
        else if(key == 16) return USER_NAT_IRISH;
        else if(key == 17) return USER_NAT_ITALIAN;
        else if(key == 60) return USER_NAT_KAZAKH;
        else if(key == 64) return USER_NAT_KOREAN;
        else if(key == 31) return USER_NAT_KUWAITI;
        else if(key == 63) return USER_NAT_KYRGYZSTANI;
        else if(key == 18) return USER_NAT_LITHUANIAN;
        else if(key == 32) return USER_NAT_MALAYSIAN;
        else if(key == 39) return USER_NAT_MAURITIAN;
        else if(key == 41) return USER_NAT_MEXICAN;
        else if(key == 61) return USER_NAT_MONGOLIAN;
        else if(key == 37) return USER_NAT_NEWZEALAND;
        else if(key == 44) return USER_NAT_NIGERIAN;
        else if(key == 19) return USER_NAT_NORWEGIAN;
        else if(key == 33) return USER_NAT_PAKISTANI;
        else if(key == 62) return USER_NAT_PAPUANEWGUINEAN;
        else if(key == 34) return USER_NAT_PERUVIAN;
        else if(key == 20) return USER_NAT_POLISH;
        else if(key == 47) return USER_NAT_PORTUGUESE;
        else if(key == 21) return USER_NAT_ROMANIAN;
        else if(key == 46) return USER_NAT_RUSSIAN;
        else if(key == 52) return USER_NAT_SAUDIARABIAN;
        else if(key == 22) return USER_NAT_SCOTTISH;
        else if(key == 59) return USER_NAT_SERBIAN;
        else if(key == 23) return USER_NAT_SOUTHAFRICAN;
        else if(key == 35) return USER_NAT_SOUTHAMERICAN;
        else if(key == 24) return USER_NAT_SPANISH;
        else if(key == 38) return USER_NAT_SRILANKAN;
        else if(key == 36) return USER_NAT_SWEDISH;
        else if(key == 25) return USER_NAT_SWISS;
        else if(key == 48) return USER_NAT_SYRIAN;
        else if(key == 45) return USER_NAT_THAI;
        else if(key == 51) return USER_NAT_TURKISH;
        else if(key == 42) return USER_NAT_VENEZUELAN;
        else if(key == 26) return USER_NAT_WELSH;
        else return USER_NAT_OTHER;
	}
	
	public int getNationalityPosForKey(int key){
		return listNationality.indexOf(getNatDescForKey(key));
	}
	
	public ArrayList<String> getLicense(){
		return listLicense;
	}
	
	public int getLicenseKeyForDesc(String desc){
		if(desc.equals(USER_NO)) return 0;
		else if(desc.equals(USER_YES)) return 1;
		else return 9;
	}
	
	public String getLicenseDescForKey(int key){
		if(key == 0) return USER_NO;
		else if(key == 1) return USER_YES;
		else return USER_UNKNOWN;
	}
	
	public int getLicensePosForKey(int key){
		return listLicense.indexOf(getLicenseDescForKey(key));
	}
	
	public ArrayList<String> getRelocateList(){
		return listRelocate;
	}

	public int getRelocateKeyForDesc(String desc){
		if(desc.equals(USER_NO)) return 0;
		else if(desc.equals(USER_YES)) return 1;
		else if(desc.equals(USER_MAYBE)) return 2;
		else return 9;
	}
	
	public String getRelocateDescForKey(int key){
		if(key == 0) return USER_NO;
		else if(key == 1) return USER_YES;
		else if(key == 2) return USER_MAYBE;
		else return USER_UNKNOWN;
	}
	
	public int getRelocatePosForKey(int key){
		return listRelocate.indexOf(getRelocateDescForKey(key));
	}
	
	public ArrayList<String> getEthnicities(){
		return listEthnicity;
	}
	
	public int getEthnicityKeyForDesc(String desc){
		if(desc.equals(USER_ETH_NONE)) return 0;
		else if(desc.equals(USER_ETH_WHITEENGLISH)) return 1;
		else if(desc.equals(USER_ETH_WHITEIRISH)) return 2;
		else if(desc.equals(USER_ETH_ANYOTHERWHITEBG)) return 3;
		else if(desc.equals(USER_ETH_WHITEANDBLACKCARIBBEAN)) return 4;
		else if(desc.equals(USER_ETH_WHITEANDBLACKAFRICAN)) return 5;
		else if(desc.equals(USER_ETH_WHITEANDASIAN)) return 6;
		else if(desc.equals(USER_ETH_ANYOTHERMIXEDBG)) return 7;
		else if(desc.equals(USER_ETH_INDIAN)) return 8;
		else if(desc.equals(USER_ETH_PAKISTANI)) return 9;
		else if(desc.equals(USER_ETH_AFRICAN)) return 13;
		else if(desc.equals(USER_ETH_ANYOTHERBLACKBG)) return 15;
		else if(desc.equals(USER_ETH_CHINESE)) return 14;
		else return 16;		
	}
	
	public String getEthnicityDescForKey(int key){
		if(key == 0) return USER_ETH_NONE;
		else if(key == 1) return USER_ETH_WHITEENGLISH;
		else if(key == 2) return USER_ETH_WHITEIRISH;
		else if(key == 3) return USER_ETH_ANYOTHERWHITEBG;
		else if(key == 4) return USER_ETH_WHITEANDBLACKCARIBBEAN;
		else if(key == 5) return USER_ETH_WHITEANDBLACKAFRICAN;
		else if(key == 6) return USER_ETH_WHITEANDASIAN;
		else if(key == 7) return USER_ETH_ANYOTHERMIXEDBG;
		else if(key == 8) return USER_ETH_INDIAN;
		else if(key == 9) return USER_ETH_PAKISTANI;
		else if(key == 13) return USER_ETH_AFRICAN;
		else if(key == 15) return USER_ETH_ANYOTHERBLACKBG;
		else if(key == 14) return USER_ETH_CHINESE;
		else return USER_ETH_ANYOTHERETHNIC;
	}
	
	public int getEthPosForKey(int key){
		return listEthnicity.indexOf(getEthnicityDescForKey(key));
	}
	
	public ArrayList<String> getLanguages(){
		return listLanguages;
	}
	
	public ArrayList<String> getCurrencyList(){
		return listCurrency;
	}
	
	public String getCurrencyThreeForDesc(String desc){
        if(desc.equals(CURRENCY_UAEDIRHAM)) return CURRENCYSYM_AED;
        else if(desc.equals(CURRENCY_ALBANIANLEK)) return CURRENCYSYM_ALL;
        else if(desc.equals(CURRENCY_NETHANTILLESGUILDER)) return CURRENCYSYM_ANG;
        else if(desc.equals(CURRENCY_ARGENTINEPESO)) return CURRENCYSYM_ARS;
        else if(desc.equals(CURRENCY_AUSTRALIANDOLLAR)) return CURRENCYSYM_AUD;
        else if(desc.equals(CURRENCY_ARUBAFLORIN)) return CURRENCYSYM_AWG;
        else if(desc.equals(CURRENCY_BARBADOSDOLLAR)) return CURRENCYSYM_BBD;
        else if(desc.equals(CURRENCY_BANGLADESHTAKA)) return CURRENCYSYM_BDT;
        else if(desc.equals(CURRENCY_BULGARIANLEV)) return CURRENCYSYM_BGN;
        else if(desc.equals(CURRENCY_BAHRAINIDINAR)) return CURRENCYSYM_BHD;
        else if(desc.equals(CURRENCY_BURUNDIFRANC)) return CURRENCYSYM_BIF;
        else if(desc.equals(CURRENCY_BERMUDADOLLAR)) return CURRENCYSYM_BMD;
        else if(desc.equals(CURRENCY_BRUNEIDOLLAR)) return CURRENCYSYM_BND;
        else if(desc.equals(CURRENCY_BOLIVIANBOLIVIANO)) return CURRENCYSYM_BOB;
        else if(desc.equals(CURRENCY_BRAZILIANREAL)) return CURRENCYSYM_BRL;
        else if(desc.equals(CURRENCY_BAHAMIANDOLLAR)) return CURRENCYSYM_BSD;
        else if(desc.equals(CURRENCY_BHUTANNGULTRUM)) return CURRENCYSYM_BTN;
        else if(desc.equals(CURRENCY_BOTSWANAPULA)) return CURRENCYSYM_BWP;
        else if(desc.equals(CURRENCY_BELARUSRUBLE)) return CURRENCYSYM_BYR;
        else if(desc.equals(CURRENCY_BELIZEDOLLAR)) return CURRENCYSYM_BZD;
        else if(desc.equals(CURRENCY_CANADIANDOLLAR)) return CURRENCYSYM_CAD;
        else if(desc.equals(CURRENCY_SWISSFRANC)) return CURRENCYSYM_CHF;
        else if(desc.equals(CURRENCY_CHILEANPESO)) return CURRENCYSYM_CLP;
        else if(desc.equals(CURRENCY_CHINESEYUAN)) return CURRENCYSYM_CNY;
        else if(desc.equals(CURRENCY_COLOMBIANPESO)) return CURRENCYSYM_COP;
        else if(desc.equals(CURRENCY_COSTARICACOLON)) return CURRENCYSYM_CRC;
        else if(desc.equals(CURRENCY_CUBANPESO)) return CURRENCYSYM_CUP;
        else if(desc.equals(CURRENCY_CAPEVERDEESCUDO)) return CURRENCYSYM_CVE;
        else if(desc.equals(CURRENCY_CZECHKORUNA)) return CURRENCYSYM_CZK;
        else if(desc.equals(CURRENCY_DIJIBOUTIFRANC)) return CURRENCYSYM_DJF;
        else if(desc.equals(CURRENCY_DANISHKRONE)) return CURRENCYSYM_DKK;
        else if(desc.equals(CURRENCY_DOMINICANPESO)) return CURRENCYSYM_DOP;
        else if(desc.equals(CURRENCY_ALGERIANDINAR)) return CURRENCYSYM_DZD;
        else if(desc.equals(CURRENCY_ESTONIANKROON)) return CURRENCYSYM_EEK;
        else if(desc.equals(CURRENCY_EGYPTIANPOUND)) return CURRENCYSYM_EGP;
        else if(desc.equals(CURRENCY_ETHIOPIANBIRR)) return CURRENCYSYM_ETB;
        else if(desc.equals(CURRENCY_EURO)) return CURRENCYSYM_EUR;
        else if(desc.equals(CURRENCY_FIJIDOLLAR)) return CURRENCYSYM_FJD;
        else if(desc.equals(CURRENCY_FALKLANDISLANDSPOUND)) return CURRENCYSYM_FKP;
        else if(desc.equals(CURRENCY_BRITISHPOUND)) return CURRENCYSYM_GBP;
        else if(desc.equals(CURRENCY_GIBRALTARPOUND)) return CURRENCYSYM_GIP;
        else if(desc.equals(CURRENCY_GAMBIANDALASI)) return CURRENCYSYM_GMD;
        else if(desc.equals(CURRENCY_GUINEAFRANC)) return CURRENCYSYM_GNF;
        else if(desc.equals(CURRENCY_GUATEMALAQUETZAL)) return CURRENCYSYM_GTQ;
        else if(desc.equals(CURRENCY_GUYANADOLLAR)) return CURRENCYSYM_GYD;
        else if(desc.equals(CURRENCY_HONGKONGDOLLAR)) return CURRENCYSYM_HKD;
        else if(desc.equals(CURRENCY_HONDURASLEMPIRA)) return CURRENCYSYM_HNL;
        else if(desc.equals(CURRENCY_CROATIANKUNA)) return CURRENCYSYM_HRK;
        else if(desc.equals(CURRENCY_HAITIGOURDE)) return CURRENCYSYM_HTG;
        else if(desc.equals(CURRENCY_HUNGARIANFORINT)) return CURRENCYSYM_HUF;
        else if(desc.equals(CURRENCY_INDONESIANRUPIAH)) return CURRENCYSYM_IDR;
        else if(desc.equals(CURRENCY_ISRAELISHEKEL)) return CURRENCYSYM_ILS;
        else if(desc.equals(CURRENCY_INDIANRUPEE)) return CURRENCYSYM_INR;
        else if(desc.equals(CURRENCY_IRAQIDINAR)) return CURRENCYSYM_IQD;
        else if(desc.equals(CURRENCY_IRANRIAL)) return CURRENCYSYM_IRR;
        else if(desc.equals(CURRENCY_ICELANDKRONA)) return CURRENCYSYM_ISK;
        else if(desc.equals(CURRENCY_JAMAICANDOLLAR)) return CURRENCYSYM_JMD;
        else if(desc.equals(CURRENCY_JORDANIANDINAR)) return CURRENCYSYM_JOD;
        else if(desc.equals(CURRENCY_JAPANESEYEN)) return CURRENCYSYM_JPY;
        else if(desc.equals(CURRENCY_KENYANSHILLING)) return CURRENCYSYM_KES;
        else if(desc.equals(CURRENCY_CAMBODIARIEL)) return CURRENCYSYM_KHR;
        else if(desc.equals(CURRENCY_COMOROSFRANC)) return CURRENCYSYM_KMF;
        else if(desc.equals(CURRENCY_NORTHKOREANWON)) return CURRENCYSYM_KPW;
        else if(desc.equals(CURRENCY_KOREANWON)) return CURRENCYSYM_KRW;
        else if(desc.equals(CURRENCY_KUWAITIDINAR)) return CURRENCYSYM_KWD;
        else if(desc.equals(CURRENCY_CAYMANISLANDSDOLLAR)) return CURRENCYSYM_KYD;
        else if(desc.equals(CURRENCY_KAZAKHSTANTENGE)) return CURRENCYSYM_KZT;
        else if(desc.equals(CURRENCY_LAOKIP)) return CURRENCYSYM_LAK;
        else if(desc.equals(CURRENCY_LEBANESEPOUND)) return CURRENCYSYM_LBP;
        else if(desc.equals(CURRENCY_SRILANKARUPEE)) return CURRENCYSYM_LKR;
        else if(desc.equals(CURRENCY_LIBERIANDOLLAR)) return CURRENCYSYM_LRD;
        else if(desc.equals(CURRENCY_LESOTHOLOTI)) return CURRENCYSYM_LSL;
        else if(desc.equals(CURRENCY_LITHUANIANLITA)) return CURRENCYSYM_LTL;
        else if(desc.equals(CURRENCY_LATVIANLAT)) return CURRENCYSYM_LVL;
        else if(desc.equals(CURRENCY_LIBYANDINAR)) return CURRENCYSYM_LYD;
        else if(desc.equals(CURRENCY_MOROCCANDIRHAM)) return CURRENCYSYM_MAD;
        else if(desc.equals(CURRENCY_MOLDOVANLEU)) return CURRENCYSYM_MDL;
        else if(desc.equals(CURRENCY_MACEDONIANDENAR)) return CURRENCYSYM_MKD;
        else if(desc.equals(CURRENCY_MYANMARKYAT)) return CURRENCYSYM_MMK;
        else if(desc.equals(CURRENCY_MONGOLIANTUGRIK)) return CURRENCYSYM_MNT;
        else if(desc.equals(CURRENCY_MACAUPATACA)) return CURRENCYSYM_MOP;
        else if(desc.equals(CURRENCY_MAURITANIAOUGULYA)) return CURRENCYSYM_MRO;
        else if(desc.equals(CURRENCY_MAURITIUSRUPEE)) return CURRENCYSYM_MUR;
        else if(desc.equals(CURRENCY_MALDIVESRUFIYAA)) return CURRENCYSYM_MVR;
        else if(desc.equals(CURRENCY_MALAWIKWACHA)) return CURRENCYSYM_MWK;
        else if(desc.equals(CURRENCY_MEXICANPESO)) return CURRENCYSYM_MXN;
        else if(desc.equals(CURRENCY_MALAYSIANRINGGIT)) return CURRENCYSYM_MYR;
        else if(desc.equals(CURRENCY_NAMIBIANDOLLAR)) return CURRENCYSYM_NAD;
        else if(desc.equals(CURRENCY_NIGERIANNAIRA)) return CURRENCYSYM_NGN;
        else if(desc.equals(CURRENCY_NICARAGUACORDOBA)) return CURRENCYSYM_NIO;
        else if(desc.equals(CURRENCY_NORWEGIANKRONE)) return CURRENCYSYM_NOK;
        else if(desc.equals(CURRENCY_NEPALESERUPEE)) return CURRENCYSYM_NPR;
        else if(desc.equals(CURRENCY_NEWZEALANDDOLLAR)) return CURRENCYSYM_NZD;
        else if(desc.equals(CURRENCY_OMANIRIAL)) return CURRENCYSYM_OMR;
        else if(desc.equals(CURRENCY_PANAMABALBOA)) return CURRENCYSYM_PAB;
        else if(desc.equals(CURRENCY_PERUVIANNUEVOSOL)) return CURRENCYSYM_PEN;
        else if(desc.equals(CURRENCY_PAPUANEWGUINEAKINA)) return CURRENCYSYM_PGK;
        else if(desc.equals(CURRENCY_PHILIPPINEPESO)) return CURRENCYSYM_PHP;
        else if(desc.equals(CURRENCY_PAKISTANIRUPEE)) return CURRENCYSYM_PKR;
        else if(desc.equals(CURRENCY_POLISHZLOTY)) return CURRENCYSYM_PLN;
        else if(desc.equals(CURRENCY_PARAGUAYANGUARANI)) return CURRENCYSYM_PYG;
        else if(desc.equals(CURRENCY_QATARRIAL)) return CURRENCYSYM_QAR;
        else if(desc.equals(CURRENCY_ROMANIANNEWLEU)) return CURRENCYSYM_RON;
        else if(desc.equals(CURRENCY_RUSSIANROUBLE)) return CURRENCYSYM_RUB;
        else if(desc.equals(CURRENCY_RWANDAFRANC)) return CURRENCYSYM_RWF;
        else if(desc.equals(CURRENCY_SAUDIARABIANRIYAL)) return CURRENCYSYM_SAR;
        else if(desc.equals(CURRENCY_SOLOMONISLANDSDOLLAR)) return CURRENCYSYM_SBD;
        else if(desc.equals(CURRENCY_SEYCHELLESRUPEE)) return CURRENCYSYM_SCR;
        else if(desc.equals(CURRENCY_SWEDISHKRONA)) return CURRENCYSYM_SEK;
        else if(desc.equals(CURRENCY_SINGAPOREDOLLAR)) return CURRENCYSYM_SGD;
        else if(desc.equals(CURRENCY_STHELENAPOUND)) return CURRENCYSYM_SHP;
        else if(desc.equals(CURRENCY_SLOVAKKORUNA)) return CURRENCYSYM_SKK;
        else if(desc.equals(CURRENCY_SIERRALEONELEONE)) return CURRENCYSYM_SLL;
        else if(desc.equals(CURRENCY_SOMALISHILLING)) return CURRENCYSYM_SOS;
        else if(desc.equals(CURRENCY_SAOTOMEDOBRA)) return CURRENCYSYM_STD;
        else if(desc.equals(CURRENCY_ELSALVADORCOLON)) return CURRENCYSYM_SVC;
        else if(desc.equals(CURRENCY_SYRIANPOUND)) return CURRENCYSYM_SYP;
        else if(desc.equals(CURRENCY_SWAZILANDLILAGENI)) return CURRENCYSYM_SZL;
        else if(desc.equals(CURRENCY_THAIBAHT)) return CURRENCYSYM_THB;
        else if(desc.equals(CURRENCY_TUNISIANDINAR)) return CURRENCYSYM_TND;
        else if(desc.equals(CURRENCY_TONGAPAANGA)) return CURRENCYSYM_TOP;
        else if(desc.equals(CURRENCY_TURKISHLIRA)) return CURRENCYSYM_TRY;
        else if(desc.equals(CURRENCY_TRINIDADTOBAGODOLLAR)) return CURRENCYSYM_TTD;
        else if(desc.equals(CURRENCY_TAIWANDOLLAR)) return CURRENCYSYM_TWD;
        else if(desc.equals(CURRENCY_TANZANIANSHILLING)) return CURRENCYSYM_TZS;
        else if(desc.equals(CURRENCY_UKRAINEHRYVNIA)) return CURRENCYSYM_UAH;
        else if(desc.equals(CURRENCY_UGANDANSHILLING)) return CURRENCYSYM_UGX;
        else if(desc.equals(CURRENCY_USDOLLAR)) return CURRENCYSYM_USD;
        else if(desc.equals(CURRENCY_URUGUAYANNEWPESO)) return CURRENCYSYM_UYU;
        else if(desc.equals(CURRENCY_VIETNAMDONG)) return CURRENCYSYM_VND;
        else if(desc.equals(CURRENCY_VANUATUVATU)) return CURRENCYSYM_VUV;
        else if(desc.equals(CURRENCY_SAMOATALA)) return CURRENCYSYM_WST;
        else if(desc.equals(CURRENCY_CFAFRANC)) return CURRENCYSYM_XAF;
        else if(desc.equals(CURRENCY_SILVEROUNCES)) return CURRENCYSYM_XAG;
        else if(desc.equals(CURRENCY_GOLDOUNCES)) return CURRENCYSYM_XAU;
        else if(desc.equals(CURRENCY_EASTCARIBBEANDOLLAR)) return CURRENCYSYM_XCD;
        else if(desc.equals(CURRENCY_COPPERPOUNDS)) return CURRENCYSYM_XCP;
        else if(desc.equals(CURRENCY_CFAFRANC)) return CURRENCYSYM_XOF;
        else if(desc.equals(CURRENCY_PALLADIUMOUNCES)) return CURRENCYSYM_XPD;
        else if(desc.equals(CURRENCY_PACIFICFRANC)) return CURRENCYSYM_XPF;
        else if(desc.equals(CURRENCY_PLATINUMOUNCES)) return CURRENCYSYM_XPT;
        else if(desc.equals(CURRENCY_YEMENRIYAL)) return CURRENCYSYM_YER;
        else if(desc.equals(CURRENCY_SOUTHAFRICANRAND)) return CURRENCYSYM_ZAR;
        else if(desc.equals(CURRENCY_ZAMBIANKWACHA)) return CURRENCYSYM_ZMK;
        else return CURRENCYSYM_ZWD;		
	}	
	
	public String gedetCurrencyDescForThree(String three){
        if(three.equals(CURRENCYSYM_AED)) return CURRENCY_UAEDIRHAM;
        else if(three.equals(CURRENCYSYM_ALL)) return CURRENCY_ALBANIANLEK;
        else if(three.equals(CURRENCYSYM_ANG)) return CURRENCY_NETHANTILLESGUILDER;
        else if(three.equals(CURRENCYSYM_ARS)) return CURRENCY_ARGENTINEPESO;
        else if(three.equals(CURRENCYSYM_AUD)) return CURRENCY_AUSTRALIANDOLLAR;
        else if(three.equals(CURRENCYSYM_AWG)) return CURRENCY_ARUBAFLORIN;
        else if(three.equals(CURRENCYSYM_BBD)) return CURRENCY_BARBADOSDOLLAR;
        else if(three.equals(CURRENCYSYM_BDT)) return CURRENCY_BANGLADESHTAKA;
        else if(three.equals(CURRENCYSYM_BGN)) return CURRENCY_BULGARIANLEV;
        else if(three.equals(CURRENCYSYM_BHD)) return CURRENCY_BAHRAINIDINAR;
        else if(three.equals(CURRENCYSYM_BIF)) return CURRENCY_BURUNDIFRANC;
        else if(three.equals(CURRENCYSYM_BMD)) return CURRENCY_BERMUDADOLLAR;
        else if(three.equals(CURRENCYSYM_BND)) return CURRENCY_BRUNEIDOLLAR;
        else if(three.equals(CURRENCYSYM_BOB)) return CURRENCY_BOLIVIANBOLIVIANO;
        else if(three.equals(CURRENCYSYM_BRL)) return CURRENCY_BRAZILIANREAL;
        else if(three.equals(CURRENCYSYM_BSD)) return CURRENCY_BAHAMIANDOLLAR;
        else if(three.equals(CURRENCYSYM_BTN)) return CURRENCY_BHUTANNGULTRUM;
        else if(three.equals(CURRENCYSYM_BWP)) return CURRENCY_BOTSWANAPULA;
        else if(three.equals(CURRENCYSYM_BYR)) return CURRENCY_BELARUSRUBLE;
        else if(three.equals(CURRENCYSYM_BZD)) return CURRENCY_BELIZEDOLLAR;
        else if(three.equals(CURRENCYSYM_CAD)) return CURRENCY_CANADIANDOLLAR;
        else if(three.equals(CURRENCYSYM_CHF)) return CURRENCY_SWISSFRANC;
        else if(three.equals(CURRENCYSYM_CLP)) return CURRENCY_CHILEANPESO;
        else if(three.equals(CURRENCYSYM_CNY)) return CURRENCY_CHINESEYUAN;
        else if(three.equals(CURRENCYSYM_COP)) return CURRENCY_COLOMBIANPESO;
        else if(three.equals(CURRENCYSYM_CRC)) return CURRENCY_COSTARICACOLON;
        else if(three.equals(CURRENCYSYM_CUP)) return CURRENCY_CUBANPESO;
        else if(three.equals(CURRENCYSYM_CVE)) return CURRENCY_CAPEVERDEESCUDO;
        else if(three.equals(CURRENCYSYM_CZK)) return CURRENCY_CZECHKORUNA;
        else if(three.equals(CURRENCYSYM_DJF)) return CURRENCY_DIJIBOUTIFRANC;
        else if(three.equals(CURRENCYSYM_DKK)) return CURRENCY_DANISHKRONE;
        else if(three.equals(CURRENCYSYM_DOP)) return CURRENCY_DOMINICANPESO;
        else if(three.equals(CURRENCYSYM_DZD)) return CURRENCY_ALGERIANDINAR;
        else if(three.equals(CURRENCYSYM_EEK)) return CURRENCY_ESTONIANKROON;
        else if(three.equals(CURRENCYSYM_EGP)) return CURRENCY_EGYPTIANPOUND;
        else if(three.equals(CURRENCYSYM_ETB)) return CURRENCY_ETHIOPIANBIRR;
        else if(three.equals(CURRENCYSYM_EUR)) return CURRENCY_EURO;
        else if(three.equals(CURRENCYSYM_FJD)) return CURRENCY_FIJIDOLLAR;
        else if(three.equals(CURRENCYSYM_FKP)) return CURRENCY_FALKLANDISLANDSPOUND;
        else if(three.equals(CURRENCYSYM_GBP)) return CURRENCY_BRITISHPOUND;
        else if(three.equals(CURRENCYSYM_GIP)) return CURRENCY_GIBRALTARPOUND;
        else if(three.equals(CURRENCYSYM_GMD)) return CURRENCY_GAMBIANDALASI;
        else if(three.equals(CURRENCYSYM_GNF)) return CURRENCY_GUINEAFRANC;
        else if(three.equals(CURRENCYSYM_GTQ)) return CURRENCY_GUATEMALAQUETZAL;
        else if(three.equals(CURRENCYSYM_GYD)) return CURRENCY_GUYANADOLLAR;
        else if(three.equals(CURRENCYSYM_HKD)) return CURRENCY_HONGKONGDOLLAR;
        else if(three.equals(CURRENCYSYM_HNL)) return CURRENCY_HONDURASLEMPIRA;
        else if(three.equals(CURRENCYSYM_HRK)) return CURRENCY_CROATIANKUNA;
        else if(three.equals(CURRENCYSYM_HTG)) return CURRENCY_HAITIGOURDE;
        else if(three.equals(CURRENCYSYM_HUF)) return CURRENCY_HUNGARIANFORINT;
        else if(three.equals(CURRENCYSYM_IDR)) return CURRENCY_INDONESIANRUPIAH;
        else if(three.equals(CURRENCYSYM_ILS)) return CURRENCY_ISRAELISHEKEL;
        else if(three.equals(CURRENCYSYM_INR)) return CURRENCY_INDIANRUPEE;
        else if(three.equals(CURRENCYSYM_IQD)) return CURRENCY_IRAQIDINAR;
        else if(three.equals(CURRENCYSYM_IRR)) return CURRENCY_IRANRIAL;
        else if(three.equals(CURRENCYSYM_ISK)) return CURRENCY_ICELANDKRONA;
        else if(three.equals(CURRENCYSYM_JMD)) return CURRENCY_JAMAICANDOLLAR;
        else if(three.equals(CURRENCYSYM_JOD)) return CURRENCY_JORDANIANDINAR;
        else if(three.equals(CURRENCYSYM_JPY)) return CURRENCY_JAPANESEYEN;
        else if(three.equals(CURRENCYSYM_KES)) return CURRENCY_KENYANSHILLING;
        else if(three.equals(CURRENCYSYM_KHR)) return CURRENCY_CAMBODIARIEL;
        else if(three.equals(CURRENCYSYM_KMF)) return CURRENCY_COMOROSFRANC;
        else if(three.equals(CURRENCYSYM_KPW)) return CURRENCY_NORTHKOREANWON;
        else if(three.equals(CURRENCYSYM_KRW)) return CURRENCY_KOREANWON;
        else if(three.equals(CURRENCYSYM_KWD)) return CURRENCY_KUWAITIDINAR;
        else if(three.equals(CURRENCYSYM_KYD)) return CURRENCY_CAYMANISLANDSDOLLAR;
        else if(three.equals(CURRENCYSYM_KZT)) return CURRENCY_KAZAKHSTANTENGE;
        else if(three.equals(CURRENCYSYM_LAK)) return CURRENCY_LAOKIP;
        else if(three.equals(CURRENCYSYM_LBP)) return CURRENCY_LEBANESEPOUND;
        else if(three.equals(CURRENCYSYM_LKR)) return CURRENCY_SRILANKARUPEE;
        else if(three.equals(CURRENCYSYM_LRD)) return CURRENCY_LIBERIANDOLLAR;
        else if(three.equals(CURRENCYSYM_LSL)) return CURRENCY_LESOTHOLOTI;
        else if(three.equals(CURRENCYSYM_LTL)) return CURRENCY_LITHUANIANLITA;
        else if(three.equals(CURRENCYSYM_LVL)) return CURRENCY_LATVIANLAT;
        else if(three.equals(CURRENCYSYM_LYD)) return CURRENCY_LIBYANDINAR;
        else if(three.equals(CURRENCYSYM_MAD)) return CURRENCY_MOROCCANDIRHAM;
        else if(three.equals(CURRENCYSYM_MDL)) return CURRENCY_MOLDOVANLEU;
        else if(three.equals(CURRENCYSYM_MKD)) return CURRENCY_MACEDONIANDENAR;
        else if(three.equals(CURRENCYSYM_MMK)) return CURRENCY_MYANMARKYAT;
        else if(three.equals(CURRENCYSYM_MNT)) return CURRENCY_MONGOLIANTUGRIK;
        else if(three.equals(CURRENCYSYM_MOP)) return CURRENCY_MACAUPATACA;
        else if(three.equals(CURRENCYSYM_MRO)) return CURRENCY_MAURITANIAOUGULYA;
        else if(three.equals(CURRENCYSYM_MUR)) return CURRENCY_MAURITIUSRUPEE;
        else if(three.equals(CURRENCYSYM_MVR)) return CURRENCY_MALDIVESRUFIYAA;
        else if(three.equals(CURRENCYSYM_MWK)) return CURRENCY_MALAWIKWACHA;
        else if(three.equals(CURRENCYSYM_MXN)) return CURRENCY_MEXICANPESO;
        else if(three.equals(CURRENCYSYM_MYR)) return CURRENCY_MALAYSIANRINGGIT;
        else if(three.equals(CURRENCYSYM_NAD)) return CURRENCY_NAMIBIANDOLLAR;
        else if(three.equals(CURRENCYSYM_NGN)) return CURRENCY_NIGERIANNAIRA;
        else if(three.equals(CURRENCYSYM_NIO)) return CURRENCY_NICARAGUACORDOBA;
        else if(three.equals(CURRENCYSYM_NOK)) return CURRENCY_NORWEGIANKRONE;
        else if(three.equals(CURRENCYSYM_NPR)) return CURRENCY_NEPALESERUPEE;
        else if(three.equals(CURRENCYSYM_NZD)) return CURRENCY_NEWZEALANDDOLLAR;
        else if(three.equals(CURRENCYSYM_OMR)) return CURRENCY_OMANIRIAL;
        else if(three.equals(CURRENCYSYM_PAB)) return CURRENCY_PANAMABALBOA;
        else if(three.equals(CURRENCYSYM_PEN)) return CURRENCY_PERUVIANNUEVOSOL;
        else if(three.equals(CURRENCYSYM_PGK)) return CURRENCY_PAPUANEWGUINEAKINA;
        else if(three.equals(CURRENCYSYM_PHP)) return CURRENCY_PHILIPPINEPESO;
        else if(three.equals(CURRENCYSYM_PKR)) return CURRENCY_PAKISTANIRUPEE;
        else if(three.equals(CURRENCYSYM_PLN)) return CURRENCY_POLISHZLOTY;
        else if(three.equals(CURRENCYSYM_PYG)) return CURRENCY_PARAGUAYANGUARANI;
        else if(three.equals(CURRENCYSYM_QAR)) return CURRENCY_QATARRIAL;
        else if(three.equals(CURRENCYSYM_RON)) return CURRENCY_ROMANIANNEWLEU;
        else if(three.equals(CURRENCYSYM_RUB)) return CURRENCY_RUSSIANROUBLE;
        else if(three.equals(CURRENCYSYM_RWF)) return CURRENCY_RWANDAFRANC;
        else if(three.equals(CURRENCYSYM_SAR)) return CURRENCY_SAUDIARABIANRIYAL;
        else if(three.equals(CURRENCYSYM_SBD)) return CURRENCY_SOLOMONISLANDSDOLLAR;
        else if(three.equals(CURRENCYSYM_SCR)) return CURRENCY_SEYCHELLESRUPEE;
        else if(three.equals(CURRENCYSYM_SEK)) return CURRENCY_SWEDISHKRONA;
        else if(three.equals(CURRENCYSYM_SGD)) return CURRENCY_SINGAPOREDOLLAR;
        else if(three.equals(CURRENCYSYM_SHP)) return CURRENCY_STHELENAPOUND;
        else if(three.equals(CURRENCYSYM_SKK)) return CURRENCY_SLOVAKKORUNA;
        else if(three.equals(CURRENCYSYM_SLL)) return CURRENCY_SIERRALEONELEONE;
        else if(three.equals(CURRENCYSYM_SOS)) return CURRENCY_SOMALISHILLING;
        else if(three.equals(CURRENCYSYM_STD)) return CURRENCY_SAOTOMEDOBRA;
        else if(three.equals(CURRENCYSYM_SVC)) return CURRENCY_ELSALVADORCOLON;
        else if(three.equals(CURRENCYSYM_SYP)) return CURRENCY_SYRIANPOUND;
        else if(three.equals(CURRENCYSYM_SZL)) return CURRENCY_SWAZILANDLILAGENI;
        else if(three.equals(CURRENCYSYM_THB)) return CURRENCY_THAIBAHT;
        else if(three.equals(CURRENCYSYM_TND)) return CURRENCY_TUNISIANDINAR;
        else if(three.equals(CURRENCYSYM_TOP)) return CURRENCY_TONGAPAANGA;
        else if(three.equals(CURRENCYSYM_TRY)) return CURRENCY_TURKISHLIRA;
        else if(three.equals(CURRENCYSYM_TTD)) return CURRENCY_TRINIDADTOBAGODOLLAR;
        else if(three.equals(CURRENCYSYM_TWD)) return CURRENCY_TAIWANDOLLAR;
        else if(three.equals(CURRENCYSYM_TZS)) return CURRENCY_TANZANIANSHILLING;
        else if(three.equals(CURRENCYSYM_UAH)) return CURRENCY_UKRAINEHRYVNIA;
        else if(three.equals(CURRENCYSYM_UGX)) return CURRENCY_UGANDANSHILLING;
        else if(three.equals(CURRENCYSYM_USD)) return CURRENCY_USDOLLAR;
        else if(three.equals(CURRENCYSYM_UYU)) return CURRENCY_URUGUAYANNEWPESO;
        else if(three.equals(CURRENCYSYM_VND)) return CURRENCY_VIETNAMDONG;
        else if(three.equals(CURRENCYSYM_VUV)) return CURRENCY_VANUATUVATU;
        else if(three.equals(CURRENCYSYM_WST)) return CURRENCY_SAMOATALA;
        else if(three.equals(CURRENCYSYM_XAF)) return CURRENCY_CFAFRANC;
        else if(three.equals(CURRENCYSYM_XAG)) return CURRENCY_SILVEROUNCES;
        else if(three.equals(CURRENCYSYM_XAU)) return CURRENCY_GOLDOUNCES;
        else if(three.equals(CURRENCYSYM_XCD)) return CURRENCY_EASTCARIBBEANDOLLAR;
        else if(three.equals(CURRENCYSYM_XCP)) return CURRENCY_COPPERPOUNDS;
        else if(three.equals(CURRENCYSYM_XOF)) return CURRENCY_CFAFRANC;
        else if(three.equals(CURRENCYSYM_XPD)) return CURRENCY_PALLADIUMOUNCES;
        else if(three.equals(CURRENCYSYM_XPF)) return CURRENCY_PACIFICFRANC;
        else if(three.equals(CURRENCYSYM_XPT)) return CURRENCY_PLATINUMOUNCES;
        else if(three.equals(CURRENCYSYM_YER)) return CURRENCY_YEMENRIYAL;
        else if(three.equals(CURRENCYSYM_ZAR)) return CURRENCY_SOUTHAFRICANRAND;
        else if(three.equals(CURRENCYSYM_ZMK)) return CURRENCY_ZAMBIANKWACHA;
        else return CURRENCY_ZAMBIANKWACHA;		
	}		
}