package applusvelosi.projects.android.arc.views.fragments;

import java.util.ArrayList;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import applusvelosi.projects.android.arc.R;
import applusvelosi.projects.android.arc.adapters.lists.SimpleAdapter;
import applusvelosi.projects.android.arc.utils.SaltProgressDialog;

public class SearchLocationFragment extends ActionbarFragment implements OnItemClickListener{
	private static SearchLocationFragment instance;
	private View actionbarBackButton, actionbarRefereshButton;
	private TextView actionbarTitle;
	
	private TextView etSearch;
	private RelativeLayout buttonSearch;
	private ListView lv;
	private ArrayList<String> locations;
	private SimpleAdapter adapter;
	private SearchLocationInterface inf;
	private SaltProgressDialog pd;
	
	public static SearchLocationFragment getInstance(){
		if(instance == null)
			instance = new SearchLocationFragment();
		
		return instance;
	}
	
	@Override
	protected RelativeLayout setupActionbar() {
		RelativeLayout actionbarLayout = (RelativeLayout)LayoutInflater.from(activity).inflate(R.layout.actionbar_backrefresh, null);
		actionbarBackButton = actionbarLayout.findViewById(R.id.buttons_actionbar_back);
		actionbarRefereshButton = actionbarLayout.findViewById(R.id.buttons_actionbar_refresh);
		actionbarTitle = (TextView)actionbarLayout.findViewById(R.id.tviews_actionbar_title);
		actionbarTitle.setText("Select Location");

		actionbarTitle.setOnClickListener(this);
		actionbarBackButton.setOnClickListener(this);
		actionbarRefereshButton.setVisibility(View.GONE);
		
		return actionbarLayout;
	}

	@Override
	protected View createView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		View v = inflater.inflate(R.layout.fragment_searchlocation, null);
		lv = (ListView)v.findViewById(R.id.lists_lv);
		
		locations = new ArrayList<String>();
		locations.add("Any");
		adapter = new SimpleAdapter(activity, locations);
		lv.setAdapter(adapter);
		lv.setOnItemClickListener(this);
				
		etSearch = (EditText)v.findViewById(R.id.etexts_search);
		buttonSearch = (RelativeLayout)v.findViewById(R.id.buttons_searchlocation_search);
		buttonSearch.setOnClickListener(this);
		
		return v;
	}

	@Override
	public void onClick(View v) {
		if(v==actionbarBackButton || v==actionbarTitle){
			activity.onBackPressed();
		}else if(v == buttonSearch){
			if(pd == null)
				pd = new SaltProgressDialog(activity);
			pd.show();
			
			new Thread(new Runnable(){

				@Override
				public void run() {
					Object tempResult;
					try{
		            	tempResult = app.onlineGateway.getLocationSuggestions(etSearch.getText().toString());
					}catch(Exception e){
						e.printStackTrace();
						tempResult = e.getMessage();
					}
					final Object result = tempResult;
					
					new Handler(Looper.getMainLooper()).post(new Runnable() {
						
						@Override
						public void run() {
							pd.dismiss();
							if(result instanceof String)
								app.showMessageDialog(activity, result.toString());
							else{
								locations.clear();
								locations.add("Any");
								locations.addAll((ArrayList<String>)result);
								adapter.notifyDataSetChanged();
							}							
						}
					});
				}
				
			}).start();
		}
	}

	@Override
	public void onItemClick(AdapterView<?> parent, View view, int pos, long id) {
		inf.onLocationSelected(adapter.getItem(pos).toString());
		activity.onBackPressed();
	}
	
	public void setSearchLocationInterface(SearchLocationInterface inf){
		this.inf = inf;
	}
	
	public interface SearchLocationInterface{
		
		public void onLocationSelected(String location);
	}
	
}
