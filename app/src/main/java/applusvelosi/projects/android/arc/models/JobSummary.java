package applusvelosi.projects.android.arc.models;

import org.json.JSONObject;

import applusvelosi.projects.android.arc.utils.OnlineGateway;

public class JobSummary {

	private int jobID;
	private String title, ref, country, dateAdded, desc;
	
	public JobSummary(JSONObject jsonJobSummary, OnlineGateway onlineGateway) throws Exception{
		jobID = jsonJobSummary.getInt("VacancyID");
		title = jsonJobSummary.getString("Title");
		ref = jsonJobSummary.getString("VacancyRef");
		country = jsonJobSummary.getJSONObject("Country").getString("CountryName");
		dateAdded = onlineGateway.dJsonizeDate(jsonJobSummary.getString("DateCreated"));
		desc = jsonJobSummary.getString("Description");		
	}
	
	public int getJobID(){
		return jobID;
	}
	
	public String getTitle(){
		return title;
	}
	
	public String getReference(){
		return ref;
	}
	
	public String getCountry(){
		return country;
	}
	
	public String getDateAdded(){
		return dateAdded;
	}
	
	public String getDescription(){
		return desc;
	}
}
