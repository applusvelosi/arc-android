package applusvelosi.projects.android.arc.adapters.lists;

import java.util.ArrayList;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.TextView;
import applusvelosi.projects.android.arc.ArcApplication;
import applusvelosi.projects.android.arc.R;
import applusvelosi.projects.android.arc.models.Application;
import applusvelosi.projects.android.arc.models.Document;
import applusvelosi.projects.android.arc.models.SavedSearch;
import applusvelosi.projects.android.arc.views.SidebarActivity;
import applusvelosi.projects.android.arc.views.fragments.profile.UserSaveSearchesFragment;
import applusvelosi.projects.android.arc.views.fragments.profile.UserSavedSearchDetailFragment;

public class SavedSearchAdapter extends BaseAdapter{
	private UserSaveSearchesFragment frag;
	private ArrayList<SavedSearch> sss;
	
	public SavedSearchAdapter(UserSaveSearchesFragment frag, ArrayList<SavedSearch> sss){
		this.frag = frag;
		this.sss = sss;
	}

	@Override
	public View getView(int pos, View v, ViewGroup parent) {
		Holder holder;
		if(v == null){
			holder = new Holder();
			v = LayoutInflater.from(frag.getActivity()).inflate(R.layout.cell_savedsearch, null);
			holder.tvTitle = (TextView)v.findViewById(R.id.tviews_profile_savedsearch_title);
			holder.tvAddedOn = (TextView)v.findViewById(R.id.tviews_profile_savedsearch_dateadded);
			holder.cbIsEnabled = (CheckBox)v.findViewById(R.id.cboxs_profile_savedsearch_isenabled);
			v.setTag(holder);
		}
		
		holder = (Holder)v.getTag();
		SavedSearch ss = sss.get(pos);
		holder.tvTitle.setText(ss.getName());
		holder.tvAddedOn.setText("Added on "+ss.getDateAdded(frag.app.onlineGateway));
		holder.cbIsEnabled.setTag(pos);
		holder.cbIsEnabled.setOnCheckedChangeListener(null);
		holder.cbIsEnabled.setChecked(ss.willAlert());
		holder.cbIsEnabled.setOnCheckedChangeListener(frag);

		return v;
	}
	
	@Override
	public int getCount() {
		return sss.size();
	}

	@Override
	public Object getItem(int pos) {
		return null;
	}

	@Override
	public long getItemId(int pos) {
		return 0;
	}

	private class Holder{
		private TextView tvTitle, tvAddedOn;
		private CheckBox cbIsEnabled;
	}
}
