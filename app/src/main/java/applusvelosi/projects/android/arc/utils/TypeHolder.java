package applusvelosi.projects.android.arc.utils;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.HashMap;

import com.google.gson.reflect.TypeToken;

public class TypeHolder {

	public Type hashmapOfStringObject = new TypeToken<HashMap<String, Object>>(){}.getType();
	public Type hashmapOfStringString = new TypeToken<HashMap<String, String>>(){}.getType();
}
