package applusvelosi.projects.android.arc.views.fragments;

import java.util.ArrayList;
import java.util.Date;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import applusvelosi.projects.android.arc.R;
import applusvelosi.projects.android.arc.adapters.lists.JobSummaryAdapter;
import applusvelosi.projects.android.arc.models.JobSummary;
import applusvelosi.projects.android.arc.models.SavedSearch;
import applusvelosi.projects.android.arc.utils.SaltProgressDialog;
import applusvelosi.projects.android.arc.utils.enums.JobTypes;
import applusvelosi.projects.android.arc.utils.enums.SearchTypes;

public class JobSummaryFragment extends ActionbarFragment implements OnItemClickListener{
	private static final String KEY_SEARCHED = "keyname";
	private static final String KEY_TYPE = "keytype";
	private static final String KEY_LOC = "keylocation";
	private static final String KEY_RAD = "keyradius";
	private static final String KEY_JOBTYPE = "keyjobtype";
	private static final String KEY_COUNTRYID = "keycountryid";
	private static final String KEY_DAYSID ="keypostedwithin";
	
	private View actionbarBackButton, actionbarRefereshButton, actionbarSaveSearch;
	private TextView actionbarTitle;
	
	private ListView lv;
	private ArrayList<JobSummary> jobs;
	private JobSummaryAdapter adapter;
	private SaltProgressDialog pd;
	
	private AlertDialog dialogSaveSearch;
	private EditText etSaveSearchName;
	private CheckBox cbSaveSearchAlert;
	
	public static JobSummaryFragment newInstance(String searched, String type, String location, int radius, String jobType, int countryID, int postedWithinID){
		JobSummaryFragment frag = new JobSummaryFragment();
		Bundle b = new Bundle();
		b.putString(KEY_SEARCHED, searched);
		b.putString(KEY_TYPE, type);
		b.putString(KEY_LOC, location);
		b.putInt(KEY_RAD, radius);
		b.putString(KEY_JOBTYPE, jobType);
		b.putInt(KEY_COUNTRYID, countryID);
		b.putInt(KEY_DAYSID, postedWithinID);
		frag.setArguments(b);
		
		return frag;
	}
	
	@Override
	protected RelativeLayout setupActionbar() {
		RelativeLayout actionbarLayout = (RelativeLayout)LayoutInflater.from(activity).inflate(R.layout.actionbar_jobsummary, null);
		actionbarBackButton = actionbarLayout.findViewById(R.id.buttons_actionbar_back);
		actionbarRefereshButton = actionbarLayout.findViewById(R.id.buttons_actionbar_refresh);
		actionbarSaveSearch = actionbarLayout.findViewById(R.id.buttons_actionbar_savesearch);
		actionbarTitle = (TextView)actionbarLayout.findViewById(R.id.tviews_actionbar_title);
		actionbarTitle.setText("Job Summary");

		actionbarTitle.setOnClickListener(this);
		actionbarSaveSearch.setOnClickListener(this);
		actionbarBackButton.setOnClickListener(this);
		actionbarRefereshButton.setOnClickListener(this);
		
		return actionbarLayout;
	}

	@Override
	protected View createView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		View v = inflater.inflate(R.layout.fragment_lv, null);
		lv = (ListView)v.findViewById(R.id.lists_lv);
		if(jobs == null){
			jobs = new ArrayList<JobSummary>();
			pd = new SaltProgressDialog(activity);
			pd.show();
			new Thread(new AdvanceSearcher()).start();
		}
		
		adapter = new JobSummaryAdapter(activity, jobs);
		lv.setAdapter(adapter);
		lv.setOnItemClickListener(this);
		
		LinearLayout builderView = (LinearLayout)LayoutInflater.from(activity).inflate(R.layout.dialog_savesearch, null);
		etSaveSearchName = (EditText)builderView.findViewById(R.id.etexts_dialogs_savesearch);
		cbSaveSearchAlert = (CheckBox)builderView.findViewById(R.id.cboxs_dialogs_savesearch);
		dialogSaveSearch = new AlertDialog.Builder(activity).setTitle("Save Search").setView(builderView)
										  .setPositiveButton("Save", new DialogInterface.OnClickListener() {
											
											@Override
											public void onClick(DialogInterface dialog, int which) {
												dialog.dismiss();
												pd.show();
												new Thread(new SearchSaver()).start();
											}
										  })
										  .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
											
											@Override
											public void onClick(DialogInterface dialog, int which) {
												dialog.dismiss();
											}
										}).create();
		
		return v;
	}

	@Override
	public void onClick(View v) {
		if(v == actionbarBackButton || v == actionbarTitle){
			activity.onBackPressed();
		}else if(v == actionbarRefereshButton){
			pd.show();
			new Thread(new AdvanceSearcher()).start();			
		}else if(v == actionbarSaveSearch){
			dialogSaveSearch.show();
		}
	}

	@Override
	public void onItemClick(AdapterView<?> arg0, View arg1, int arg2, long arg3) {
		activity.changeChildPage(JobDetailFragment.newInstance(jobs.get(arg2).getJobID(), true));
	}
	
	private class AdvanceSearcher implements Runnable{
		
		@Override
		public void run() {
			Object tempResult;
			try{				
				
				String searched = getArguments().getString(KEY_SEARCHED);
				String typeID = String.valueOf(SearchTypes.getKeyForValue(getArguments().getString(KEY_TYPE)));
				String location = getArguments().getString(KEY_LOC);
				String radius = String.valueOf(getArguments().getInt(KEY_RAD));
				String jobTypeID = String.valueOf(JobTypes.getKeyForValue(getArguments().getString(KEY_JOBTYPE)));
				String countryID = String.valueOf(getArguments().getInt(KEY_COUNTRYID));
				String postedWithinID = String.valueOf(getArguments().getInt(KEY_DAYSID));
				
				tempResult = app.onlineGateway.getAdvanceSearchResults(searched, typeID, location, radius, jobTypeID, countryID, postedWithinID);
			}catch(Exception e){
				e.printStackTrace();
				tempResult = e.getMessage();
			}
			
			final Object result = tempResult;
			new Handler(Looper.getMainLooper()).post(new Runnable() {
				
				@Override
				public void run() {
					pd.dismiss();
					if(result instanceof String){
						app.showMessageDialog(activity, result.toString());
					}else{
						jobs.clear();
						jobs.addAll((ArrayList<JobSummary>)result);
						adapter.notifyDataSetChanged();
					}
				}
			});
		}	
	}
	
	private class SearchSaver implements Runnable{

		@Override
		public void run() {
			String tempResult;
			try{
				String name = etSaveSearchName.getText().toString();
				int candidateID = Integer.parseInt(app.offlineGateway.getUserID());
				String dateToday = app.onlineGateway.jsonizeDate(new Date());
				String searchFor = getArguments().getString(KEY_SEARCHED);
				int typeID = SearchTypes.getKeyForValue(getArguments().getString(KEY_TYPE));
				String type = getArguments().getString(KEY_TYPE);
				String location = getArguments().getString(KEY_LOC);
				int radius = getArguments().getInt(KEY_RAD);
				int jobTypeID = JobTypes.getKeyForValue(getArguments().getString(KEY_JOBTYPE));
				String jobType = getArguments().getString(KEY_JOBTYPE);
				int countryID = getArguments().getInt(KEY_COUNTRYID);
				int postedWithinID = getArguments().getInt(KEY_DAYSID);
				boolean willAlert = cbSaveSearchAlert.isChecked();
				
				tempResult = app.onlineGateway.saveSearch(SavedSearch.getJSONFromNewSavedSearch(name, candidateID, dateToday, searchFor, typeID, type, location, 0, 0, countryID, radius, jobTypeID, jobType, postedWithinID, willAlert, app));
			}catch(Exception e){
				e.printStackTrace();
				tempResult = e.getMessage();
			}
			final String result = tempResult;
			new Handler(Looper.getMainLooper()).post(new Runnable() {
				
				@Override
				public void run() {
					pd.dismiss();
					app.showMessageDialog(activity, (result==null)?"Saved Successfully!":result);
				}
			});
		}
		
	}

}
