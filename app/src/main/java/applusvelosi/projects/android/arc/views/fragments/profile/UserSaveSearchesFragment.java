package applusvelosi.projects.android.arc.views.fragments.profile;

import java.util.ArrayList;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemLongClickListener;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.AdapterView.OnItemClickListener;
import applusvelosi.projects.android.arc.R;
import applusvelosi.projects.android.arc.adapters.lists.SavedSearchAdapter;
import applusvelosi.projects.android.arc.models.SavedSearch;
import applusvelosi.projects.android.arc.utils.SaltProgressDialog;
import applusvelosi.projects.android.arc.utils.interfaces.RootFragment;
import applusvelosi.projects.android.arc.views.fragments.ActionbarFragment;

public class UserSaveSearchesFragment extends ActionbarFragment implements RootFragment, OnItemClickListener, OnCheckedChangeListener, OnItemLongClickListener{
	private static UserSaveSearchesFragment instance;
	private View actionbarButtonMenu, actionbarButtonRefresh, actionbarButtonSubscribe, actionbarButtonUnsubscribe;
	
	private SaltProgressDialog pd;
	private ListView lv;
	private SavedSearchAdapter adapter;
	private ArrayList<SavedSearch> sss;
	private int subscribableSearchesCnt = 0;
	
	public static UserSaveSearchesFragment getInstance(){
		if(instance == null)
			instance = new UserSaveSearchesFragment();
		
		return instance;
	}
	
	public static void destroyInstance(){
		instance = null;
	}
	
	@Override
	protected RelativeLayout setupActionbar() {
		RelativeLayout actionbar = (RelativeLayout)LayoutInflater.from(activity).inflate(R.layout.actionbar_savedsearches, null);
		actionbarButtonMenu = actionbar.findViewById(R.id.buttons_actionbar_menu);
		actionbarButtonRefresh = actionbar.findViewById(R.id.buttons_actionbar_refresh);
		actionbarButtonSubscribe = actionbar.findViewById(R.id.buttons_actionbar_subscribe);
		actionbarButtonUnsubscribe = actionbar.findViewById(R.id.buttons_actionbar_unsubscribe);
		((TextView)actionbar.findViewById(R.id.tviews_actionbar_title)).setText("Saved Searches");
		
		actionbarButtonMenu.setOnClickListener(this);
		actionbarButtonSubscribe.setOnClickListener(this);
		actionbarButtonUnsubscribe.setOnClickListener(this);
		actionbarButtonRefresh.setOnClickListener(this);
		return actionbar;
	}

	@Override
	protected View createView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		View v = inflater.inflate(R.layout.fragment_lv, null);
		lv = (ListView)v.findViewById(R.id.lists_lv);
		pd = new SaltProgressDialog(activity);
		if(sss == null){
			sss = new ArrayList<SavedSearch>();
			pd.show();
			new Thread(new SavedSearchListLoader()).start();
		}
		
		LinearLayout headerView = (LinearLayout)inflater.inflate(R.layout.tv_listview_header, null);
		((TextView)headerView.getChildAt(0)).setText("Below are your saves job searches, you can enable them individually, or in bulk using the envelope above");
		lv.addHeaderView(headerView, null, false);
		
		adapter = new SavedSearchAdapter(this, sss);
		lv.setAdapter(adapter);
		lv.setOnItemClickListener(this);
		lv.setOnItemLongClickListener(this);
		
		return v;
	}
	
	public ArrayList<SavedSearch> getSSS(UserSavedSearchDetailFragment key){
		return sss;
	}

	@Override
	public void onClick(View v) {
		if(v == actionbarButtonMenu){
			v.setEnabled(false);
			activity.toggleSidebar(v);
		}else if(v == actionbarButtonRefresh){
			pd.show();
			new Thread(new SavedSearchListLoader()).start();
		}else if(v == actionbarButtonSubscribe){
			pd.show();
			new Thread(new AllSaveSearchStatusChanger(true)).start();
		}else if(v == actionbarButtonUnsubscribe){
			pd.show();
			new Thread(new AllSaveSearchStatusChanger(false)).start();
		}
	}

	@Override
	public void disableUserInteractionsOnSidebarShown() {
		lv.setEnabled(false);
	}

	@Override
	public void enableUserInteractionsOnSidebarHidden() {
		lv.setEnabled(true);
	}
	
	public class SavedSearchListLoader implements Runnable{

		@Override
		public void run() {
			Object tempResult;
			try{
				tempResult = app.onlineGateway.getSavedSearches();
			}catch(Exception e){
				e.printStackTrace();
				tempResult = e.getMessage();
			}
			final Object result = tempResult;
			new Handler(Looper.getMainLooper()).post(new Runnable() {
				
				@Override
				public void run() {
					pd.dismiss();
					if(result instanceof String)
						app.showMessageDialog(activity, result.toString());
					else{
						sss.clear();
						sss.addAll((ArrayList<SavedSearch>) result);
						adapter.notifyDataSetChanged();
		
						updateActionbarSubscribeButtons();
					}
				}
			});
		}
		
		
	}

	@Override
	public void onItemClick(AdapterView<?> parent, View view, int pos, long id) {
		activity.changeChildPage(UserSavedSearchDetailFragment.newInstance(pos-1));
	}
	
	private void updateActionbarSubscribeButtons(){
		subscribableSearchesCnt = 0;
		for(SavedSearch ss :sss){
			if(ss.willAlert())
				subscribableSearchesCnt++;
		}
	
		if(sss.size() > 0){
			if(subscribableSearchesCnt == sss.size()){
				actionbarButtonUnsubscribe.setVisibility(View.VISIBLE);
				actionbarButtonSubscribe.setVisibility(View.GONE);
			}else if(subscribableSearchesCnt == 0){
				actionbarButtonSubscribe.setVisibility(View.VISIBLE);
				actionbarButtonUnsubscribe.setVisibility(View.GONE);
			}else{
				actionbarButtonSubscribe.setVisibility(View.VISIBLE);
				actionbarButtonUnsubscribe.setVisibility(View.VISIBLE);
			}
		}else{
			actionbarButtonSubscribe.setVisibility(View.GONE);
			actionbarButtonUnsubscribe.setVisibility(View.GONE);
		}
	}

	@Override
	public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
		System.out.println("buttonview tag "+buttonView.getTag());
		if(buttonView.getTag() != null){
			SavedSearch ss = sss.get(Integer.parseInt(buttonView.getTag().toString()));
			ss.changeSubscriptionWillAlert(isChecked);
			pd.show();
			new Thread(new SaveSearchStatusChanger(ss)).start();
		}
	}

	private class AllSaveSearchStatusChanger implements Runnable{
		private boolean willAlert;
		
		public AllSaveSearchStatusChanger(boolean willAlert){
			this.willAlert = willAlert;
		}

		@Override
		public void run() {
			String tempResult;
			try{
				tempResult = app.onlineGateway.changeAllSavedSearchStatusByID(Integer.parseInt(app.offlineGateway.getUserID()), willAlert);
			}catch(Exception e){
				e.printStackTrace();
				tempResult = e.getMessage();
			}
			final String result = tempResult;
			
			new Handler(Looper.getMainLooper()).post(new Runnable() {
				
				@Override
				public void run() {
					pd.dismiss();
					if(result.equals("OK")){
						for(SavedSearch ss :sss)
							ss.changeSubscriptionWillAlert(willAlert);
						adapter.notifyDataSetChanged();
						updateActionbarSubscribeButtons();
						
						app.showMessageDialog(activity, "Changed Succesfully!");																
					}else
						app.showMessageDialog(activity, result);
				}
			});
		}	
	}
	
	private class SaveSearchStatusChanger implements Runnable{
		private SavedSearch ss;
		
		public SaveSearchStatusChanger(SavedSearch ss){
			this.ss = ss;
		}
		
		@Override
		public void run() {
			String tempResult;
			
			try{
				tempResult = app.onlineGateway.saveSearch(ss.jsonize(app));
			}catch(Exception e){
				e.printStackTrace();
				tempResult = e.getMessage();
			}
			final String result = tempResult;
			new Handler(Looper.getMainLooper()).post(new Runnable() {
				
				@Override
				public void run() {
					pd.dismiss();
					if(result == null)
						updateActionbarSubscribeButtons();			
					else
						app.showMessageDialog(activity, result);
				}
			});
		}
		
	}

	@Override
	public boolean onItemLongClick(AdapterView<?> parent, View v, final int pos, long id) {
		final SavedSearch ss = sss.get(pos-1);
		new AlertDialog.Builder(activity).setTitle(null)
										 .setMessage("Delete "+ss.getName())
										 .setPositiveButton("Confirm", new DialogInterface.OnClickListener() {
											
											@Override
											public void onClick(DialogInterface dialog, int which) {
												dialog.dismiss();
												pd.show();
												new Thread(new Runnable() {
													
													@Override
													public void run() {
														String tempResult;
														try{
															tempResult = app.onlineGateway.deleteSavedSearchWithJBEID(ss.getJBEID());
														}catch(Exception e){
															e.printStackTrace();
															tempResult = e.getMessage();
														}
														final String result = tempResult;
														
														new Handler(Looper.getMainLooper()).post(new Runnable() {
															
															@Override
															public void run() {
																pd.dismiss();
																if(result.equals("OK")){
																	app.showMessageDialog(activity, "Deleted Successfully");
																	sss.remove(pos-1);
																	adapter.notifyDataSetChanged();
																}else{
																	app.showMessageDialog(activity, result);																	
																}
															}
														});
													}
												}).start();
											}
										})
										.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
											
											@Override
											public void onClick(DialogInterface dialog, int which) {
												dialog.dismiss();
											}
										}).create().show();
		return true;
	}
}
