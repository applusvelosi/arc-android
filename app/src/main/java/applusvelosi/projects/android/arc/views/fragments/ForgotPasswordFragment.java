package applusvelosi.projects.android.arc.views.fragments;

import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;
import applusvelosi.projects.android.arc.R;
import applusvelosi.projects.android.arc.utils.SaltProgressDialog;
import applusvelosi.projects.android.arc.utils.interfaces.RootFragment;
import applusvelosi.projects.android.arc.views.fragments.ActionbarFragment;

public class ForgotPasswordFragment extends ActionbarFragment{
	private static ForgotPasswordFragment instance;
	private View actionbarButtonBack, actionbarButtonDone;
	private TextView actionbarButtonTitle;
	
	private EditText etEmail;
	private SaltProgressDialog pd;
	
	public static ForgotPasswordFragment getInstance(){
		if(instance == null)
			instance = new ForgotPasswordFragment();
		
		return instance;
	}
	
	@Override
	protected RelativeLayout setupActionbar() {
		RelativeLayout actionbar = (RelativeLayout)LayoutInflater.from(activity).inflate(R.layout.actionbar_backdone, null);
		actionbarButtonBack = actionbar.findViewById(R.id.buttons_actionbar_back);
		actionbarButtonDone = actionbar.findViewById(R.id.buttons_actionbar_done);
		actionbarButtonTitle = (TextView)actionbar.findViewById(R.id.tviews_actionbar_title);
		actionbarButtonTitle.setText("Forgot Password");
		
		actionbarButtonTitle.setOnClickListener(this);
		actionbarButtonBack.setOnClickListener(this);
		actionbarButtonDone.setOnClickListener(this);
		return actionbar;
	}

	@Override
	protected View createView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		View v = inflater.inflate(R.layout.fragment_forgotpassword, null);
		
		etEmail = (EditText)v.findViewById(R.id.etexts_forgotpassword_email);
		
		return v;
	}

	@Override
	public void onClick(View v) {
		if(v == actionbarButtonBack || v == actionbarButtonTitle){
			activity.onBackPressed();
		}else if(v == actionbarButtonDone){
			if(etEmail.length() < 1)
				app.showMessageDialog(activity, "Please enter your email");
			else{
				if(!etEmail.getText().toString().contains("@"))
					app.showMessageDialog(activity, "Please enter a valid email");
				else{
					if(pd == null) pd = new SaltProgressDialog(activity);
					pd.show();
					new Thread(new PasswordGetter()).start();
				}
			}
		}
	}

	private class PasswordGetter implements Runnable{

		@Override
		public void run() {
			String tempResult;
			try{
				tempResult = app.onlineGateway.resetPassword(etEmail.getText().toString());
			}catch(Exception e){
				e.printStackTrace();
				tempResult = e.getMessage();
			}
			final String result = tempResult;
			
			new Handler(Looper.getMainLooper()).post(new Runnable() {
				
				@Override
				public void run() {
					pd.dismiss();
					app.showMessageDialog(activity, (result.equals("OK"))?"Password Resetted Successfully!":result);

				}
			});
		}		
		
	}

}
