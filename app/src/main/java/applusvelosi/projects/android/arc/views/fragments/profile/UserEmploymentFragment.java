package applusvelosi.projects.android.arc.views.fragments.profile;

import java.util.ArrayList;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemLongClickListener;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.AdapterView.OnItemClickListener;
import applusvelosi.projects.android.arc.R;
import applusvelosi.projects.android.arc.adapters.lists.EmpListAdapter;
import applusvelosi.projects.android.arc.models.Document;
import applusvelosi.projects.android.arc.models.Employment;
import applusvelosi.projects.android.arc.models.SavedSearch;
import applusvelosi.projects.android.arc.utils.SaltProgressDialog;
import applusvelosi.projects.android.arc.utils.interfaces.RootFragment;
import applusvelosi.projects.android.arc.views.fragments.ActionbarFragment;

public class UserEmploymentFragment extends ActionbarFragment implements RootFragment, OnItemClickListener, OnItemLongClickListener{
	private static UserEmploymentFragment instance;
	private View actionbarButtonMenu, actionbarButtonRefresh, actionbarButtonNew;
	
	private SaltProgressDialog pd;
	private ListView lv;
	private EmpListAdapter adapter;
	private ArrayList<Employment> emps;
	
	public static UserEmploymentFragment getInstance(){
		if(instance == null)
			instance = new UserEmploymentFragment();
		
		return instance;
	}
	
	public static void destroyInstance(){
		instance = null;
	}
	
	@Override
	protected RelativeLayout setupActionbar() {
		RelativeLayout actionbar = (RelativeLayout)LayoutInflater.from(activity).inflate(R.layout.actionbar_menunewrefresh, null);
		actionbarButtonMenu = actionbar.findViewById(R.id.buttons_actionbar_menu);
		actionbarButtonNew = actionbar.findViewById(R.id.buttons_actionbar_new);
		actionbarButtonRefresh = actionbar.findViewById(R.id.buttons_actionbar_refresh);
		((TextView)actionbar.findViewById(R.id.tviews_actionbar_title)).setText("Employments");
		
		actionbarButtonMenu.setOnClickListener(this);
		actionbarButtonNew.setOnClickListener(this);
		actionbarButtonRefresh.setOnClickListener(this);
		return actionbar;
	}

	@Override
	protected View createView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		View v = inflater.inflate(R.layout.fragment_lv, null);
		lv = (ListView)v.findViewById(R.id.lists_lv);
		pd = new SaltProgressDialog(activity);
		if(emps == null){
			emps = new ArrayList<Employment>();
			pd.show();
			new Thread(new EmpListLoader()).start();
		}
		
		LinearLayout headerView = (LinearLayout)inflater.inflate(R.layout.tv_listview_header, null);
		((TextView)headerView.getChildAt(0)).setText("Please enter / update details of your last 5 jobs");
		lv.addHeaderView(headerView, null, false);
		
		adapter = new EmpListAdapter(activity, emps);
		lv.setAdapter(adapter);
		lv.setOnItemClickListener(this);
		lv.setOnItemLongClickListener(this);

		return v;
	}

	@Override
	public void onClick(View v) {
		if(v == actionbarButtonMenu){
			v.setEnabled(false);
			activity.toggleSidebar(v);
		}else if(v == actionbarButtonRefresh){
			pd.show();
			new Thread(new EmpListLoader()).start();			
		}else if(v == actionbarButtonNew){
			activity.changeChildPage(new UserEmploymentDetailsFragment());
		}
	}

	@Override
	public void disableUserInteractionsOnSidebarShown() {
		lv.setEnabled(false);
	}

	@Override
	public void enableUserInteractionsOnSidebarHidden() {
		lv.setEnabled(true);
	}

	private class EmpListLoader implements Runnable{

		@Override
		public void run() {
			Object tempResult;
			try{
				tempResult = app.onlineGateway.getEmployments();
			}catch(Exception e){
				tempResult = e.getMessage();
			}
			final Object result = tempResult;
			
			new Handler(Looper.getMainLooper()).post(new Runnable() {
				
				@Override
				public void run() {
					pd.dismiss();
					if(result instanceof String){
						app.showMessageDialog(activity, result.toString());
					}else{
						emps.clear();
						emps.addAll((ArrayList<Employment>)result);
						adapter.notifyDataSetChanged();
					}
					
				}
			});
		}
		
	}
	
	public Employment editEmployment(int pos, UserEmploymentDetailsFragment key){
		return emps.get(pos);
	}

	@Override
	public void onItemClick(AdapterView<?> parent, View view, int pos, long id) {
		activity.changeChildPage(UserEmploymentDetailsFragment.newInstance(pos-1));
	}

	@Override
	public boolean onItemLongClick(AdapterView<?> parent, View view, final int pos, long id) {
		final Employment emp = emps.get(pos-1);
		new AlertDialog.Builder(activity).setTitle(null)
										 .setMessage("Delete "+emp.getJobTitle())
										 .setPositiveButton("Confirm", new DialogInterface.OnClickListener() {
											
											@Override
											public void onClick(DialogInterface dialog, int which) {
												dialog.dismiss();
												pd.show();
												new Thread(new Runnable() {
													
													@Override
													public void run() {
														String tempResult;
														try{
															tempResult = app.onlineGateway.deleteEmployment(emp.getEmploymentID());
														}catch(Exception e){
															e.printStackTrace();
															tempResult = e.getMessage();
														}
														final String result = tempResult;
														
														new Handler(Looper.getMainLooper()).post(new Runnable() {
															
															@Override
															public void run() {
																pd.dismiss();
																if(result.equals("OK")){
																	app.showMessageDialog(activity, "Deleted Successfully");
																	emps.remove(pos-1);
																	adapter.notifyDataSetChanged();
																}else{
																	app.showMessageDialog(activity, result);																	
																}
															}
														});
													}
												}).start();
											}
										})
										.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
											
											@Override
											public void onClick(DialogInterface dialog, int which) {
												dialog.dismiss();
											}
										}).create().show();
		return true;
	}
	
}
