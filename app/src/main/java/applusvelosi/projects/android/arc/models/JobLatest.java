package applusvelosi.projects.android.arc.models;

import org.json.JSONObject;

import applusvelosi.projects.android.arc.utils.OnlineGateway;

public class JobLatest {
	private int jobID;
	private String title, ref, location, dateAdded, details, duration, salary;
	
	public JobLatest(JSONObject jsonJob, OnlineGateway gateway) throws Exception{
		jobID = jsonJob.getInt("VacancyID");
		title = jsonJob.getString("Title");
		ref = jsonJob.getString("VacancyRef");
		location = jsonJob.getString("Location");
		dateAdded = gateway.dJsonizeDate(jsonJob.getString("DateCreated"));
		details = jsonJob.getString("Description");
		duration = jsonJob.getString("Duration");
		duration = (duration.equals(""))?"Permanent":duration;
		salary = jsonJob.getString("SalaryOther");
		salary = (salary.equals(""))?"N/A":salary;
	}
	
	public int getJobID(){
		return jobID;
	}
	
	public String getTitle(){
		return title;
	}
	
	public String getReference(){
		return ref;
	}
	
	public String getLocation(){
		return location;
	}
	
	public String getDateAdded(){
		return dateAdded;
	}
	
	public String getDetails(){
		return details;
	}
	
	public String getDuration(){
		return duration;
	}
	
	public String getSalary(){
		return salary;
	}
}
