package applusvelosi.projects.android.arc.views.fragments;

import java.security.PublicKey;
import java.util.ArrayList;
import java.util.LinkedHashMap;

import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnFocusChangeListener;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TableRow;
import android.widget.TextView;
import applusvelosi.projects.android.arc.R;
import applusvelosi.projects.android.arc.adapters.spinners.SimpleSpinnerAdapter;
import applusvelosi.projects.android.arc.utils.SaltProgressDialog;
import applusvelosi.projects.android.arc.utils.enums.JobTypes;
import applusvelosi.projects.android.arc.utils.enums.PostedDays;
import applusvelosi.projects.android.arc.utils.enums.SearchTypes;
import applusvelosi.projects.android.arc.utils.interfaces.RootFragment;
import applusvelosi.projects.android.arc.views.fragments.SingleCountrySelectionFragment.SingleCountrySelectionInterface;
import applusvelosi.projects.android.arc.views.fragments.SearchLocationFragment.SearchLocationInterface;

public class AdvanceSearchFragment extends ActionbarFragment implements RootFragment, SingleCountrySelectionInterface, SearchLocationInterface, OnFocusChangeListener{
	
	private static AdvanceSearchFragment instance;
	private View actionbarButtonMenu;
	private TextView actionbarButtonSearch;
	
	private EditText etSearchFor, etDistance;
	private TextView tvSelectedCountryName, tvSelectedLocation;
	private TableRow trLocation, trCountryRegion;
	private Spinner spSearchTypes, spJobType, spPostedWithin;
	
	private static int selCountryID = 0;
	private static String selCountryName = "Any";
	private static String selLocation = "Any";
	
	public static AdvanceSearchFragment getInstance(){
		if(instance == null)
			instance = new AdvanceSearchFragment();
		
		return instance;
	}
	
	@Override
	protected RelativeLayout setupActionbar() {
		RelativeLayout actionbar = (RelativeLayout)LayoutInflater.from(activity).inflate(R.layout.actionbar_menudone, null);
		actionbarButtonMenu = actionbar.findViewById(R.id.buttons_actionbar_menu);
		actionbarButtonSearch = (TextView)actionbar.findViewById(R.id.buttons_actionbar_done);
		((TextView)actionbar.findViewById(R.id.tviews_actionbar_title)).setText("Advance Search");
		actionbarButtonSearch.setText("Search");
		actionbarButtonMenu.setOnClickListener(this);
		actionbarButtonSearch.setOnClickListener(this);
		return actionbar;
	}

	@Override
	protected View createView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		View v = inflater.inflate(R.layout.fragment_advancesearch, null);
		etSearchFor = (EditText)v.findViewById(R.id.etexts_advancesearch_for);
		spSearchTypes = (Spinner)v.findViewById(R.id.spinners_advancesearch_in);
		trLocation = (TableRow)v.findViewById(R.id.trs_advancesearch_location);
		tvSelectedLocation = (TextView)v.findViewById(R.id.tviews_advancesearch_location);
		trCountryRegion = (TableRow)v.findViewById(R.id.trs_advancesearch_countryregion);
		tvSelectedCountryName = (TextView)v.findViewById(R.id.tviews_advancesearch_countryregion);
		etDistance = (EditText)v.findViewById(R.id.etexts_advancesearch_distance);
		spJobType = (Spinner)v.findViewById(R.id.spinners_advancesearch_jobtype);
		spPostedWithin = (Spinner)v.findViewById(R.id.spinners_advancesearch_postedwithin);
		
		spSearchTypes.setAdapter(new SimpleSpinnerAdapter(activity, SearchTypes.values));
		spJobType.setAdapter(new SimpleSpinnerAdapter(activity, JobTypes.values));
		spPostedWithin.setAdapter(new SimpleSpinnerAdapter(activity, PostedDays.values));
		
		trLocation.setOnClickListener(this);
		trCountryRegion.setOnClickListener(this);
		
		tvSelectedCountryName.setText(selCountryName);
		tvSelectedLocation.setText(selLocation);

		etSearchFor.setOnFocusChangeListener(this);
		etDistance.setOnFocusChangeListener(this);
		
		return v;
	}

	@Override
	public void onClick(View v) {
		if(v == actionbarButtonMenu){
			v.setEnabled(false);
			activity.toggleSidebar(v);
		}else if(v == actionbarButtonSearch){
			String name = etSearchFor.getText().toString();
			String type = spSearchTypes.getSelectedItem().toString(); 
			String location = tvSelectedLocation.getText().toString();
			int radius = Integer.parseInt(etDistance.getText().toString());
			String jobType = spJobType.getSelectedItem().toString();
			int countryID = selCountryID;
			int postedWithinID = PostedDays.getKeyForValue(spPostedWithin.getSelectedItem().toString());

			activity.changeChildPage(JobSummaryFragment.newInstance(name, type, location, radius, jobType, countryID, postedWithinID));
		}else if(v==trLocation){
			SearchLocationFragment.getInstance().setSearchLocationInterface(this);
			activity.changeChildPage(SearchLocationFragment.getInstance());
		}else if(v == trCountryRegion){
			SingleCountrySelectionFragment.getInstance().setSearchCountryInterface(this);
			activity.changeChildPage(SingleCountrySelectionFragment.getInstance());
		}
	}

	@Override
	public void disableUserInteractionsOnSidebarShown() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void enableUserInteractionsOnSidebarHidden() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onCountrySelected(int selCountryID, String selCountryName) {
		System.out.println(selCountryName);
		AdvanceSearchFragment.selCountryID = selCountryID;
		AdvanceSearchFragment.selCountryName = selCountryName;
	}

	@Override
	public void onLocationSelected(String location) {
		AdvanceSearchFragment.selLocation = location;
	}

	@Override
	public void onFocusChange(View v, boolean hasFocus) {
		((TextView)v).setTextColor(activity.getResources().getColor((hasFocus)?R.color.black:R.color.menu_list_separator));
	}

}
