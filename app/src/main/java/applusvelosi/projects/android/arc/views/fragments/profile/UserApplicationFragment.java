package applusvelosi.projects.android.arc.views.fragments.profile;

import java.util.ArrayList;

import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.AdapterView.OnItemClickListener;
import applusvelosi.projects.android.arc.R;
import applusvelosi.projects.android.arc.adapters.lists.AppListAdapter;
import applusvelosi.projects.android.arc.models.Application;
import applusvelosi.projects.android.arc.utils.SaltProgressDialog;
import applusvelosi.projects.android.arc.utils.interfaces.RootFragment;
import applusvelosi.projects.android.arc.views.fragments.ActionbarFragment;
import applusvelosi.projects.android.arc.views.fragments.JobDetailFragment;

public class UserApplicationFragment extends ActionbarFragment implements RootFragment, OnItemClickListener{
	private static UserApplicationFragment instance;
	private View actionbarButtonMenu, actionbarButtonRefresh;
	
	private SaltProgressDialog pd;
	private ListView lv;
	private AppListAdapter adapter;
	private ArrayList<Application> apps;
	
	public static UserApplicationFragment getInstance(){
		if(instance == null)
			instance = new UserApplicationFragment();
		
		return instance;
	}
	
	public static void destroyInstance(){
		instance = null;
	}	
	
	@Override
	protected RelativeLayout setupActionbar() {
		RelativeLayout actionbar = (RelativeLayout)LayoutInflater.from(activity).inflate(R.layout.actionbar_menurefresh, null);
		actionbarButtonMenu = actionbar.findViewById(R.id.buttons_actionbar_menu);
		actionbarButtonRefresh = actionbar.findViewById(R.id.buttons_actionbar_refresh);
		((TextView)actionbar.findViewById(R.id.tviews_actionbar_title)).setText("Applications");
		
		actionbarButtonMenu.setOnClickListener(this);
		actionbarButtonRefresh.setOnClickListener(this);
		return actionbar;
	}

	@Override
	protected View createView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		View v = inflater.inflate(R.layout.fragment_lv, null);
		lv = (ListView)v.findViewById(R.id.lists_lv);
		pd = new SaltProgressDialog(activity);
		if(apps == null){
			apps = new ArrayList<Application>();
			pd.show();
			new Thread(new AppListLoader()).start();
		}
		
		
		LinearLayout headerView = (LinearLayout)inflater.inflate(R.layout.tv_listview_header, null);
		((TextView)headerView.getChildAt(0)).setText("Below are details of the applications you have made with the correct status. If the status is not up to date please contact your local office.");
		lv.addHeaderView(headerView, null, false);
		LinearLayout footerView = (LinearLayout)inflater.inflate(R.layout.tv_listview_footer, null);
		((TextView)footerView.getChildAt(1)).setText("Applied Online\nYou applied for the vacancy via our website or job boards/social media platforms we subscribe to\n\nUnsuitable\nYour qualifications, skills and experience aren't aligned with the job description\n\nResourced\nOur 150+ recuitment consultants worldwide found your CV on our database and feel you are a strong match for the vacancy\n\nShortlisted\nYou have progressed tot he shortlist stage and a recruitment consultant will contact you to (a)gather your appetite in the role (b)obtain your permission for your CV to  be sent to our client\n\nInterested\nPost contact you have expressed that you are interested in the role\n\nCV Submitted\nWe have contacted you and ascertained your interest in the vacancy and obtained permission to send your CV to our client. This will be the date one of our 150+ recruitment consultants worldwide will have send your CV onto our client for consideration\n\nInterview Date\nIf we obtained positive feedback from our client and have been or are awaiting daes for an interview\n\nClient Accepted\nPost interview our client has expressed an interview in you and has or inteds to make an offer of employment\n\nClient Rejected\nPost interview our client found there were stronger candidates available\n\nNot Interested\nEither we have been in contact and you have stated the position is not for you or post interview our client intends to make an offer of employment but you are no longer interested");
		lv.addFooterView(footerView, null, false);
		
		adapter = new AppListAdapter(activity, apps);
		lv.setAdapter(adapter);
		lv.setOnItemClickListener(this);
		return v;
	}

	@Override
	public void onClick(View v) {
		if(v == actionbarButtonMenu){
			v.setEnabled(false);
			activity.toggleSidebar(v);
		}else if(v == actionbarButtonRefresh){
			pd.show();
			new Thread(new AppListLoader()).start();			
		}
	}

	@Override
	public void disableUserInteractionsOnSidebarShown() {
		lv.setEnabled(false);
	}

	@Override
	public void enableUserInteractionsOnSidebarHidden() {
		lv.setEnabled(true);
	}
	
	public class AppListLoader implements Runnable{

		@Override
		public void run() {
			Object tempResult;
			try{
				tempResult = app.onlineGateway.getApplications();
			}catch(Exception e){
				e.printStackTrace();
				tempResult = e.getMessage();
			}
			final Object result = tempResult;
			new Handler(Looper.getMainLooper()).post(new Runnable() {
				
				@Override
				public void run(){
					pd.dismiss();
					if(result instanceof String){
						app.showMessageDialog(activity, result.toString());
					}else{
						apps.clear();
						apps.addAll((ArrayList<Application>) result);
						adapter.notifyDataSetChanged();
					}
				}
			});
			
		}
	}

	@Override
	public void onItemClick(AdapterView<?> parent, View view, int pos, long id) {
		activity.changeChildPage(JobDetailFragment.newInstance(apps.get(pos-1).getVacancyID(), false));
	}

}
