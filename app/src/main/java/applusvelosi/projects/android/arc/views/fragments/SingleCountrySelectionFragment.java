package applusvelosi.projects.android.arc.views.fragments;

import java.util.ArrayList;
import java.util.LinkedHashMap;

import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import applusvelosi.projects.android.arc.ArcApplication;
import applusvelosi.projects.android.arc.R;
import applusvelosi.projects.android.arc.adapters.lists.HomeAdapter;
import applusvelosi.projects.android.arc.adapters.lists.SimpleAdapter;
import applusvelosi.projects.android.arc.models.JobLatest;
import applusvelosi.projects.android.arc.utils.SaltProgressDialog;
import applusvelosi.projects.android.arc.utils.interfaces.RootFragment;
import applusvelosi.projects.android.arc.views.fragments.profile.UserDetailFragment;

public class SingleCountrySelectionFragment extends ActionbarFragment implements OnItemClickListener, TextWatcher{
	private static SingleCountrySelectionFragment instance;
	private View actionbarBackButton, actionbarRefereshButton;
	private TextView actionbarTitle;
	
	private TextView etSearch;
	private ListView lv;
	private static LinkedHashMap<String, String> countries;
	private ArrayList<String> countryNames;
	private SimpleAdapter adapter;
	private SingleCountrySelectionInterface inf;
	
	public static SingleCountrySelectionFragment getInstance(){
		if(instance == null){
			instance = new SingleCountrySelectionFragment();
			countries = new LinkedHashMap<String, String>();
		}
		return instance;
	}
	
	@Override
	protected RelativeLayout setupActionbar() {
		RelativeLayout actionbarLayout = (RelativeLayout)LayoutInflater.from(activity).inflate(R.layout.actionbar_backrefresh, null);
		actionbarBackButton = actionbarLayout.findViewById(R.id.buttons_actionbar_back);
		actionbarRefereshButton = actionbarLayout.findViewById(R.id.buttons_actionbar_refresh);
		actionbarTitle = (TextView)actionbarLayout.findViewById(R.id.tviews_actionbar_title);
		actionbarTitle.setText("Select Country");

		actionbarTitle.setOnClickListener(this);
		actionbarBackButton.setOnClickListener(this);
		actionbarRefereshButton.setVisibility(View.GONE);
		
		return actionbarLayout;
	}

	@Override
	protected View createView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		View v = inflater.inflate(R.layout.fragment_searchcountryregion, null);
		lv = (ListView)v.findViewById(R.id.lists_lv);
		
		countries = new LinkedHashMap<String, String>();
		countries.putAll(ArcApplication.getCountries());
		if(inf instanceof UserDetailFragment)
			countries.remove("Any");

		countryNames = new ArrayList<String>(countries.keySet());
		adapter = new SimpleAdapter(activity, countryNames);
		lv.setAdapter(adapter);
		lv.setOnItemClickListener(this);
				
		etSearch = (EditText)v.findViewById(R.id.etexts_search);
		etSearch.addTextChangedListener(this);
		return v;
	}

	@Override
	public void onClick(View v) {
		if(v == actionbarBackButton || v==actionbarTitle){
			activity.onBackPressed();
		}
	}

	@Override
	public void onItemClick(AdapterView<?> parent, View view, int pos, long id) {
		inf.onCountrySelected(Integer.parseInt(countries.get(adapter.getItem(pos).toString())), adapter.getItem(pos).toString());
		activity.onBackPressed();
	}
	
	@Override
	public void afterTextChanged(Editable s) {
		countryNames.clear();
		ArrayList<String> tempCountryNames = new ArrayList<String>(countries.keySet());
		for(String tempCountryName :tempCountryNames){
			if(tempCountryName.toLowerCase().startsWith(s.toString().toLowerCase()))
				countryNames.add(tempCountryName);
		}
		
		adapter.notifyDataSetChanged();
	}

	@Override
	public void beforeTextChanged(CharSequence s, int start, int count, int after) {
	}

	@Override
	public void onTextChanged(CharSequence s, int start, int before, int count) {
	}
	
	public void setSearchCountryInterface(SingleCountrySelectionInterface inf){
		this.inf = inf;
	}
	
	public interface SingleCountrySelectionInterface{
		
		public void onCountrySelected(int selCountryID, String selCountryName);
	}
}
