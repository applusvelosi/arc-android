package applusvelosi.projects.android.arc.models;

import java.util.HashMap;

import org.json.JSONObject;

import applusvelosi.projects.android.arc.utils.OnlineGateway;

public class JobDetail {

	private HashMap<String, Object> map;
	
	public JobDetail(HashMap<String, Object> map) {
		this.map = map;
	}
	
	public String getTypeDescription(){
		return ((HashMap<String, Object>)map.get("VacancyType")).get("Description").toString();
	}
	
	public String getDuration(){
		return map.get("Duration").toString();
	}
	
	public String getStartDate(){
		return map.get("StartDate").toString();
	}
	
	public String getLocation(){
		return map.get("Location").toString();
	}
	
	public String getSalary(){
		return map.get("SalaryOther").toString();
	}
	
	public String getContactName(){
		return map.get("ContactName").toString();
	}
	
	public String getDescription(){
		return map.get("Description").toString();
	}
	
	public int getJobID(){
		return Integer.parseInt(map.get("VacancyID").toString());
	}
	
	public int getClientID(){
		return Integer.parseInt(map.get("ClientID").toString());
	}
	
	public int getClientGroupID(){
		return Integer.parseInt(map.get("GroupID").toString());
	}
	
	public int getOfficeID(){
		return Integer.parseInt(map.get("OfficeID").toString());
	}
	
	public String getReference(){
		return map.get("VacancyRef").toString();
	}
	
	public String getTitle(){
		return map.get("Title").toString();
	}
	
	public String getClientName(){
		return map.get("ClientName").toString();
	}
	
	public String getDateFrom(){
		return map.get("DateFrom").toString();
	}
	
	public String getDateTo(){
		return map.get("DateTo").toString();
	}
	
}
