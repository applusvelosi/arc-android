package applusvelosi.projects.android.arc.utils;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;

/**
 * Created by Velosi on 16/11/2016.
 */
public class WebRequest {
    public final static int GET = 1;
    public final static int POST = 2;
    private final String AUTHKEY = "3/iQGvD5]cmTu85H(syvsg";

    public String makeWebServiceCall(String urlAddress, int requestMethod, String postBody) {
        System.out.println("URL: "+urlAddress + System.getProperty("line.separator") + "POST DATA: "+postBody);
        URL url;
        StringBuilder response = new StringBuilder();
        HttpURLConnection conn;
        BufferedWriter writer = null;
        BufferedReader reader = null;

        try {
            url = new URL(urlAddress);
            conn = (HttpURLConnection) url.openConnection();
//            conn.setReadTimeout(15000);
            conn.setConnectTimeout(7000);
            conn.setRequestProperty("AuthKey", AUTHKEY);

            if (requestMethod == POST) {
                conn.setDoOutput(true);
                conn.setRequestMethod("POST");
                conn.setRequestProperty("Content-Type", "application/json; charset=UTF-8");
                conn.setRequestProperty("Accept", "application/json");
            } else if (requestMethod == GET) {
                conn.setRequestMethod("GET");
            }

            if (postBody != null) { //posting to URL socket
                OutputStream os = conn.getOutputStream();
                writer = new BufferedWriter(new OutputStreamWriter(os, "UTF-8"));
                writer.write(postBody);
                writer.flush();
                writer.close();
                os.close();
            }
            int responseCode = conn.getResponseCode(); //reading from URL socket
            if (responseCode == HttpURLConnection.HTTP_OK) {
                String line;
                reader = new BufferedReader(new InputStreamReader(conn.getInputStream()));
                while ((line = reader.readLine()) != null)
                    response.append(line);
                reader.close();
            }
            else {
                System.out.println("RESPONSE CODE: " + responseCode);
                response.append(conn.getResponseMessage());
            }
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                if (writer != null)
                    writer.close();
                if (reader != null)
                    reader.close();
            }catch (Exception e){
                e.printStackTrace();
            }
        }
        return response.toString();
    }
}
