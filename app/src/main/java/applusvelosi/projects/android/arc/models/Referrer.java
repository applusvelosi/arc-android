package applusvelosi.projects.android.arc.models;

import org.json.JSONObject;

public class Referrer {

	private int refID;
	private String desc;
	
	public Referrer(JSONObject jsonRef) throws Exception{
		refID = jsonRef.getInt("ReferrerID");
		desc = jsonRef.getString("Description");
	}
	
	public int getRefID(){
		return refID;
	}
	
	public String getDescription(){
		return desc;
	}
}
