package applusvelosi.projects.android.arc.utils.enums;

public enum MonthAbbrs {

	Jan,
	Feb,
	Mar,
	Apr,
	May,
	Jun,
	Jul,
	Aug,
	Sep,
	Oct,
	Nov,
	Dec
	
}
