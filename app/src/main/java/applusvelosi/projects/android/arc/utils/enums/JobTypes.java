package applusvelosi.projects.android.arc.utils.enums;

import java.util.ArrayList;

public class JobTypes{
	
	private static final String ANY = "Any";
	private static final String PERMANENT = "Permanent";
	private static final String CONTRACT = "Contract";
	private static final String TEMPORARY = "Temporary";
	private static final String PARTTIME = "Part time";
	private static final String ADHOC = "Ad hoc";

	public static ArrayList<String> values;
	
	static{
		values = new ArrayList<String>();
		values.add(ANY);
		values.add(PERMANENT);
		values.add(CONTRACT);
		values.add(TEMPORARY);
		values.add(PARTTIME);
		values.add(ADHOC);
	}
	
	public static int getKeyForValue(String val){
		if(val.equals(PERMANENT)) return 1;
		else if(val.equals(CONTRACT)) return 2;
		else if(val.equals(TEMPORARY)) return 3;
		else if(val.equals(PARTTIME)) return 4;
		else if(val.equals(ADHOC)) return 5;
		else return 0;
	}
	
	public static String getValueForKey(int key){
		if(key == 1) return PERMANENT;
		else if(key == 2) return CONTRACT;
		else if(key == 3) return TEMPORARY;
		else if(key == 4) return PARTTIME;
		else if(key == 5) return ADHOC;
		else return ANY;
	}
	
}