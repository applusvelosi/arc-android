package applusvelosi.projects.android.arc.utils.enums;

import java.util.ArrayList;

public class PostedDays{
	private static final String ANY = "Any";
	private static final String DAYS_42 = "42 Days";
	private static final String DAYS_35 = "35 Days";
	private static final String DAYS_28 = "28 Days";
	private static final String DAYS_21 = "21 Days";
	private static final String DAYS_14 = "14 Days";
	private static final String DAYS_7 = "7 Days";
	private static final String DAY_1 = "1 Day";
	
	public static ArrayList<String> values;
	
	static{
		values = new ArrayList<String>();
		values.add(ANY);
		values.add(DAYS_42);
		values.add(DAYS_35);
		values.add(DAYS_28);
		values.add(DAYS_21);
		values.add(DAYS_14);
		values.add(DAYS_7);
		values.add(DAY_1);
	}
	
	public static int getKeyForValue(String val){
		if(val.equals(DAYS_42)) return 42;
		else if(val.equals(DAYS_35)) return 35;
		else if(val.equals(DAYS_28)) return 28;
		else if(val.equals(DAYS_21)) return 21;
		else if(val.equals(DAYS_14)) return 14;
		else if(val.equals(DAYS_7)) return 7;
		else if(val.equals(DAY_1)) return 1;
		else return 0;
	}
	
	public static String getValueForKey(int key){
		if(key == 42) return DAYS_42;
		else if(key == 35) return DAYS_35;
		else if(key == 28) return DAYS_28;
		else if(key == 21) return DAYS_21;
		else if(key == 14) return DAYS_14;
		else if(key == 7) return DAYS_7;
		else if(key == 1) return DAY_1;
		else return ANY;
	}
}