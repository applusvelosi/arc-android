package applusvelosi.projects.android.arc.adapters.lists;

import java.util.ArrayList;

import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;
import applusvelosi.projects.android.arc.R;
import applusvelosi.projects.android.arc.models.Document;
import applusvelosi.projects.android.arc.models.Employment;
import applusvelosi.projects.android.arc.models.JobSummary;
import applusvelosi.projects.android.arc.views.SidebarActivity;

public class JobSummaryAdapter extends BaseAdapter{
	private SidebarActivity activity;
	private ArrayList<JobSummary> jobs;
	
	public JobSummaryAdapter(SidebarActivity activity, ArrayList<JobSummary> jobs){
		this.activity = activity;
		this.jobs = jobs;
	}

	@Override
	public View getView(int pos, View v, ViewGroup parent) {
		Holder holder;
		if(v == null){
			holder = new Holder();
			v = LayoutInflater.from(activity).inflate(R.layout.cell_jobsummary, null);
			holder.tvName = (TextView)v.findViewById(R.id.tviews_jobsummary_name);
			holder.tvRef = (TextView)v.findViewById(R.id.tviews_jobsummary_ref);
			holder.tvLocation = (TextView)v.findViewById(R.id.tviews_jobsummary_location);
			holder.tvDateAdded = (TextView)v.findViewById(R.id.tviews_jobsummary_dateadded);
			holder.tvDesc = (TextView)v.findViewById(R.id.tviews_jobsummary_desc);

			v.setTag(holder);
		}
		
		holder = (Holder)v.getTag();
		JobSummary job = jobs.get(pos);
		holder.tvName.setText(job.getTitle());
		holder.tvRef.setText("Reference: "+job.getReference());
		holder.tvLocation.setText(job.getCountry());
		holder.tvDateAdded.setText("Added on "+job.getDateAdded());
		holder.tvDesc.setText(Html.fromHtml(job.getDescription()));
		
		return v;
	}
	
	@Override
	public int getCount() {
		return jobs.size();
	}

	@Override
	public Object getItem(int pos) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public long getItemId(int pos) {
		// TODO Auto-generated method stub
		return 0;
	}

	private class Holder{
		private TextView tvName, tvRef, tvLocation, tvDateAdded, tvDesc;
	}
}
