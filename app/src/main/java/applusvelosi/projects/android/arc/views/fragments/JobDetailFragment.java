package applusvelosi.projects.android.arc.views.fragments;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;
import applusvelosi.projects.android.arc.R;
import applusvelosi.projects.android.arc.models.Application;
import applusvelosi.projects.android.arc.models.JobDetail;
import applusvelosi.projects.android.arc.utils.SaltProgressDialog;

public class JobDetailFragment extends ActionbarFragment{
	private static final String KEY_JOBID = "keyjobid";
	private static final String KEY_SHOWAPPLYBUTTON = "keyshowapplybutton";
	private View actionbarButtonBack, actionbarButtonApply;
	private TextView actionbarTitle;
		
	private TextView tvRef, tvType, tvDuration, tvDateStart, tvLocation, tvSalary, tvContact;
	private WebView wvDesc;
	private JobDetail jobDetail;
	private SaltProgressDialog pd;
	private AlertDialog ad;
	private EditText etCoverLetter;
	
	public static JobDetailFragment newInstance(int jobID, boolean shouldShowApplyButton){
		JobDetailFragment frag = new JobDetailFragment();
		Bundle b = new Bundle();
		b.putString(KEY_JOBID, String.valueOf(jobID));
		b.putBoolean(KEY_SHOWAPPLYBUTTON, shouldShowApplyButton);
		frag.setArguments(b);
		
		return frag;
	}
	
	@Override
	protected RelativeLayout setupActionbar() {
		RelativeLayout actionbar = (RelativeLayout)LayoutInflater.from(activity).inflate(R.layout.actionbar_jobdetails, null);
		actionbarButtonBack = actionbar.findViewById(R.id.buttons_actionbar_back);
		actionbarButtonApply = actionbar.findViewById(R.id.buttons_actionbar_apply);
		actionbarTitle = (TextView)actionbar.findViewById(R.id.tviews_actionbar_title);
		actionbarTitle.setText("Job Detail");
		
		actionbarTitle.setOnClickListener(this);
		actionbarButtonBack.setOnClickListener(this);
		actionbarButtonApply.setOnClickListener(this);
		
		if(!getArguments().getBoolean(KEY_SHOWAPPLYBUTTON))
			actionbarButtonApply.setVisibility(View.GONE);
		
		return actionbar;
	}

	@Override
	protected View createView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		View v = inflater.inflate(R.layout.fragment_jobdetails, null);
		
		tvRef = (TextView)v.findViewById(R.id.tviews_jobdetail_ref);
		tvType = (TextView)v.findViewById(R.id.tviews_jobdetail_type);
		tvDuration = (TextView)v.findViewById(R.id.tviews_jobdetail_duration);
		tvDateStart = (TextView)v.findViewById(R.id.tviews_jobdetail_datestart);
		tvLocation = (TextView)v.findViewById(R.id.tviews_jobdetail_location);
		tvSalary = (TextView)v.findViewById(R.id.tviews_jobdetail_salary);
		tvContact = (TextView)v.findViewById(R.id.tviews_jobdetail_contact);
		wvDesc = (WebView)v.findViewById(R.id.webviews_jobdetail_desc);
		
		
		pd = new SaltProgressDialog(activity);
		pd.show();
		new Thread(new Runnable() {
			
			@Override
			public void run() {
				Object tempResult;
				try{
					tempResult = app.onlineGateway.getJobDetailByID(getArguments().getString(KEY_JOBID));
				}catch(Exception e){
					e.printStackTrace();
					tempResult = e.getMessage();
				}
				
				final Object result = tempResult;
				new Handler(Looper.getMainLooper()).post(new Runnable() {
					
					@Override
					public void run() {
						pd.dismiss();
						if(result instanceof String){
							app.showMessageDialog(activity, result.toString());
						}else{
							jobDetail = (JobDetail)result;
							actionbarTitle.setText(jobDetail.getTitle());
							tvRef.setText("REF: "+jobDetail.getReference());
							tvType.setText(jobDetail.getTypeDescription());
							tvDuration.setText(jobDetail.getDuration());
							tvDateStart.setText(jobDetail.getStartDate());
							tvLocation.setText(jobDetail.getLocation());
							tvSalary.setText(jobDetail.getSalary());
							tvContact.setText(jobDetail.getContactName());
							wvDesc.loadDataWithBaseURL(null, jobDetail.getDescription(), "text/html", "utf-8", null);						
						}
					}
				});
			}
		}).start();
		
		View builderView = inflater.inflate(R.layout.dialog_edittext, null);
		etCoverLetter = (EditText)builderView.findViewById(R.id.etexts_dialogs_edittext);
		ad = new AlertDialog.Builder(activity).setTitle(null)
											  .setView(builderView)
											  .setPositiveButton("Apply", new DialogInterface.OnClickListener() {
												
												@Override
												public void onClick(DialogInterface dialog, int which) {
													dialog.dismiss();
													pd.show();
													new Thread(new Runnable() {
														
														@Override
														public void run() {
															String tempResult;
															try{
																tempResult = app.onlineGateway.applyJob(Application.getJSONForJobApplication(jobDetail, activity.user, etCoverLetter.getText().toString(), app));
															}catch(Exception e){
																e.printStackTrace();
																tempResult = e.getMessage();
															}
															final String result = tempResult;
															
															new Handler(Looper.getMainLooper()).post(new Runnable() {
																
																@Override
																public void run() {
																	pd.dismiss();
																	app.showMessageDialog(activity, (result.equals("OK"))?"Thank you for your application, it has been recieved and will be reviewed by one of our consultants":result);
																}
															});
															
														}
													}).start();
												}
											}).setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
												
												@Override
												public void onClick(DialogInterface dialog, int which) {
													dialog.dismiss();
												}
											}).create();
		
		return v;
	}

	@Override
	public void onClick(View v) {
		if(v == actionbarButtonBack || v == actionbarTitle)
			activity.onBackPressed();
		else if(v == actionbarButtonApply){
			ad.show();
		}
	}

}
