package applusvelosi.projects.android.arc.utils;

import android.content.SharedPreferences;
import applusvelosi.projects.android.arc.ArcApplication;

public class OfflineGateway {
	//preference key
	private final String KEY_PREFS = "keyprefsuserdata";
	//preference item keys
	private final String KEY_PREFS_STAFFID = "keyprefsuserdatastaffid";
	private final String KEY_PREFS_PREVUSEDUSERNAME = "keyprefsprevusedusernameusername";
	
	private SharedPreferences prefs;
	private SharedPreferences.Editor editor;

	public OfflineGateway(ArcApplication app){
		prefs = app.getSharedPreferences(KEY_PREFS, 0);
		editor = prefs.edit();
	}
	
	public boolean isLoggedIn(){
		return (prefs.contains(KEY_PREFS_STAFFID))?true:false;
	}
	
	public void logout(){
		editor.remove(KEY_PREFS_STAFFID).commit();
	}
	
	public void saveUserDataWithID(String userIDStr){
		editor.putString(KEY_PREFS_STAFFID, userIDStr).commit();
	}
	
	public void updatePrevioulyUsedUsername(String username){
		editor.putString(KEY_PREFS_PREVUSEDUSERNAME, username).commit();
	}
	
	public String getUserID(){
		return prefs.getString(KEY_PREFS_STAFFID, "0");
	}
	
	public String getPreviouslyUsedUsername(){
		return prefs.getString(KEY_PREFS_PREVUSEDUSERNAME, "");
	}
}
