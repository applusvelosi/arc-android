package applusvelosi.projects.android.arc.utils.interfaces;

import android.app.Activity;

public interface DataGetterInterface {

	public Activity getSaltActivity();
	public void updateDataSource() throws Exception;
	public void onSuccess();
}
