package applusvelosi.projects.android.arc.utils.enums;

import java.util.ArrayList;

public class SearchTypes{
	private static final String JOBTITLE_AND_DESCRIPTION = "Job Title and Description";
	private static final String JOBTITLE_ONLY = "Job Title";
	private static final String JOBDESCRIPTION_ONLY = "Job Description";
	
	public static ArrayList<String> values;
	
	static{
		values = new ArrayList<String>();
		values.add(JOBTITLE_AND_DESCRIPTION);
		values.add(JOBTITLE_ONLY);
		values.add(JOBDESCRIPTION_ONLY);
	}
	
	public static int getKeyForValue(String val){
		if(val.equals(JOBTITLE_ONLY)) return 1;
		else if(val.equals(JOBDESCRIPTION_ONLY)) return 2;
		else return 0;
	}
	
	public static String getValueForKey(int key){
		if(key == 1) return JOBTITLE_ONLY;
		else if(key == 2) return JOBDESCRIPTION_ONLY;
		else return JOBTITLE_AND_DESCRIPTION;
	}
}