package applusvelosi.projects.android.arc;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Hashtable;
import java.util.LinkedHashMap;
import java.util.Locale;

import android.app.AlertDialog;
import android.app.Application;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.graphics.Typeface;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Handler;
import android.os.Looper;
import applusvelosi.projects.android.arc.utils.OfflineGateway;
import applusvelosi.projects.android.arc.utils.OnlineGateway;
import applusvelosi.projects.android.arc.utils.TypeHolder;
import applusvelosi.projects.android.arc.utils.UserChoicesFactory;
import applusvelosi.projects.android.arc.utils.interfaces.AppListLoadFinishListener;

import com.google.gson.Gson;

public class ArcApplication extends Application {	
	public static final long ONEDAY = 24*60*60*1000;
	public OfflineGateway offlineGateway;
	public OnlineGateway onlineGateway;
	
	public Gson gson;
	public TypeHolder types;
	public Calendar calendar;
	public SimpleDateFormat dateTimeFormat, dateFormatDefault;
	public UserChoicesFactory userChoices;
	public int thisYear;
	

	public void onCreate() {
		onlineGateway = new OnlineGateway(this);
		offlineGateway = new OfflineGateway(this);
		dateTimeFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.UK);
		dateFormatDefault = new SimpleDateFormat("dd-MMM-yyyy", Locale.UK);
		calendar = Calendar.getInstance();
		gson = new Gson();
		types = new TypeHolder();
		userChoices = new UserChoicesFactory();
		thisYear = Integer.parseInt(new SimpleDateFormat("yyyy", Locale.ENGLISH).format(new Date()));
						
		if(offlineGateway.isLoggedIn()){
			
		}else{
		}
	}
	
	public boolean isNetworkAvailable(){
		ConnectivityManager connectivityManager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
		NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
		return activeNetworkInfo != null && activeNetworkInfo.isConnected();
	}
		
	public void showMessageDialog(Context context, String message){
		new AlertDialog.Builder(context).setMessage(message)
										.setPositiveButton("Ok", new OnClickListener() {
											
											@Override
											public void onClick(DialogInterface dialog, int arg1) {
												dialog.dismiss();
											}
										})
										.create().show();
	}
	
	public boolean hasSavedData(){
		return (this.offlineGateway!=null && this.offlineGateway.isLoggedIn())?true:false;
	}
		
	private static Hashtable<String, Typeface> fontCache = new Hashtable<String, Typeface>();

    public static Typeface myFont(Context context) {
    	String name = "fonts/Tahoma.ttf";
        Typeface tf = fontCache.get(name);
        if(tf == null) {
            try {
                tf = Typeface.createFromAsset(context.getAssets(), name);
            }
            catch (Exception e) {
                return null;
            }
            fontCache.put(name, tf);
        }
        return tf;
    }    
    
	public static LinkedHashMap<String, String> getCountries(){
		LinkedHashMap<String, String> countries = new LinkedHashMap<String, String>();
		countries.put("Any", "0");
		countries.put("Afghanistan", "1");
		countries.put("Aland Islands", "2");
		countries.put("Albania", "3");
		countries.put("Algeria", "4");
		countries.put("American Samoa", "5");
		countries.put("Andorra", "6");
		countries.put("Angola", "7");
		countries.put("Anguilla", "8");
		countries.put("Antarctica", "9");
		countries.put("Antigua and Barbuda", "10");
		countries.put("Argentina", "11");
		countries.put("Armenia", "12");
		countries.put("Aruba", "13");
		countries.put("Australia", "15");
		countries.put("Austria", "16");
		countries.put("Azerbaijan", "17");
		countries.put("Bahamas", "18");
		countries.put("Bahrain", "19");
		countries.put("Bangladesh", "20");
		countries.put("Barbados", "21");
		countries.put("Belarus", "22");
		countries.put("Belgium", "23");
		countries.put("Belize", "24");
		countries.put("Benin", "25");
		countries.put("Bermuda", "26");
		countries.put("Bhutan", "27");
		countries.put("Bolivia", "28");
		countries.put("Bonaire, Saint Eustatius and Saba", "29");
		countries.put("Bosnia and Herzegovina", "30");
		countries.put("Botswana", "31");
		countries.put("Bouvet Islands", "32");
		countries.put("Brazil", "33");
		countries.put("British Indian Ocean Territory", "34");
		countries.put("Brunei Darussalam", "35");
		countries.put("Bulgaria", "36");
		countries.put("Burkina Faso", "37");
		countries.put("Burundi", "38");
		countries.put("Cambodia", "39");
		countries.put("Cameroon", "40");
		countries.put("Canada", "41");
		countries.put("Cape Verde", "42");
		countries.put("Cayman Islands", "43");
		countries.put("Central African Republic", "44");
		countries.put("Chad", "45");
		countries.put("Chile", "46");
		countries.put("China", "47");
		countries.put("Christmas Islands", "48");
		countries.put("Cocos (Keeling) Islands", "49");
		countries.put("Colombia", "50");
		countries.put("Comoros", "51");
		countries.put("Congo", "52");
		countries.put("Congo, The Democratic Republic of the", "53");
		countries.put("Cook Islands", "54");
		countries.put("Costa Rica", "55");
		countries.put("Cote d'Ivoire", "56");
		countries.put("Croatia", "57");
		countries.put("Cuba", "58");
		countries.put("Curacao", "59");
		countries.put("Cyprus", "60");
		countries.put("Czech Republic", "61");
		countries.put("Denmark", "62");
		countries.put("Djibouti", "63");
		countries.put("Dominica", "64");
		countries.put("Dominican Republic", "65");
		countries.put("Ecuador", "66");
		countries.put("Egypt", "67");
		countries.put("El Salvador", "68");
		countries.put("Equatorial Guinea", "69");
		countries.put("Eritrea", "70");
		countries.put("Estonia", "71");
		countries.put("Ethiopia", "72");
		countries.put("Falkland Islands (Malvinas)", "74");
		countries.put("Faroe Islands", "75");
		countries.put("Fiji", "76");
		countries.put("Finland", "77");
		countries.put("France", "78");
		countries.put("Frech Guiana", "79");
		countries.put("French Polynesia", "80");
		countries.put("French Southern Territories", "81");
		countries.put("Gabon", "82");
		countries.put("Gambia", "83");
		countries.put("Georgia", "84");
		countries.put("Germany", "85");
		countries.put("Ghana", "86");
		countries.put("Gibraltar", "87");
		countries.put("Greece", "88");
		countries.put("Greenland", "89");
		countries.put("Grenada", "90");
		countries.put("Guadeloupe", "91");
		countries.put("Guam", "92");
		countries.put("Guatemala", "93");
		countries.put("Guernsey", "94");
		countries.put("Guinea", "95");
		countries.put("Guinea-Bissau", "96");
		countries.put("Guyana", "97");
		countries.put("Haiti", "98");
		countries.put("Heard Island and McDonald Islands", "99");
		countries.put("Holy See (Vatican City State)", "100");
		countries.put("Honduras", "101");
		countries.put("Hong Kong", "102");
		countries.put("Hungary", "103");
		countries.put("Iceland", "104");
		countries.put("India", "105");
		countries.put("Indonesia", "106");
		countries.put("Iran, Islamic Republic of", "107");
		countries.put("Iraq", "108");
		countries.put("Ireland", "109");
		countries.put("Isle of Man", "110");
		countries.put("Israel", "111");
		countries.put("Italy", "112");
		countries.put("Jamaica", "113");
		countries.put("Japan", "114");
		countries.put("Jersey", "115");
		countries.put("Jordan", "116");
		countries.put("Kazakhstan", "117");
		countries.put("Kenya", "118");
		countries.put("Kiribati", "119");
		countries.put("Korea, Democratic People's Republic of", "120");
		countries.put("Korea, Republic of", "121");
		countries.put("Kuwait", "122");
		countries.put("Kyrgyzstan", "123");
		countries.put("Lao People's Democratic Republic", "124");
		countries.put("Latvia", "125");
		countries.put("Lebanon", "126");
		countries.put("Lesotho", "127");
		countries.put("Liberia", "128");
		countries.put("Libyan Arab Jamahiriya", "129");
		countries.put("Liechtenstein", "130");
		countries.put("Lithuana", "131");
		countries.put("Luxembourg", "132");
		countries.put("Macao", "133");
		countries.put("Macedonia", "134");
		countries.put("Madagascar", "135");
		countries.put("Malawi", "136");
		countries.put("Malaysia", "137");
		countries.put("Maldives", "138");
		countries.put("Mali", "139");
		countries.put("Malta", "140");
		countries.put("Marshall Islands", "141");
		countries.put("Martinique", "142");
		countries.put("Mauritinia", "143");
		countries.put("Mauritius", "144");
		countries.put("Mayotte", "145");
		countries.put("Mexico", "146");
		countries.put("Micronesia, Federated States of", "147");
		countries.put("Moldova, Republic of", "148");
		countries.put("Monaco", "149");
		countries.put("Mongolia", "150");
		countries.put("Montenegro", "151");
		countries.put("Montserrat", "152");
		countries.put("Morocco", "153");
		countries.put("Mozambique", "154");
		countries.put("Myanmar", "155");
		countries.put("Namibia", "156");
		countries.put("Nauru", "157");
		countries.put("Nepal", "158");
		countries.put("Netherlands", "159");
		countries.put("New Caledonia", "160");
		countries.put("New Zealand", "161");
		countries.put("Nicaragua", "162");
		countries.put("Niger", "163");
		countries.put("Nigeria", "164");
		countries.put("Niue", "165");
		countries.put("Norfolk Island", "166");
		countries.put("Northern Mariana Islands", "167");
		countries.put("Norway", "168");
		countries.put("Oman", "169");
		countries.put("Other Country", "999");
		countries.put("Pakistan", "170");
		countries.put("Palau", "171");
		countries.put("Palestinian Territory", "172");
		countries.put("Panama", "173");
		countries.put("Papua New Guinea", "174");
		countries.put("Paraguay", "175");
		countries.put("Peru", "176");
		countries.put("Philippines", "177");
		countries.put("Pitcairn", "178");
		countries.put("Poland", "179");
		countries.put("Portugal", "180");
		countries.put("Puerto Rico", "181");
		countries.put("Qatar", "182");
		countries.put("Reunion", "183");
		countries.put("Romania", "184");
		countries.put("Russian Federation", "185");
		countries.put("Rwanda", "186");
		countries.put("Saint Bartelemey", "187");
		countries.put("Saint Helena", "188");
		countries.put("Saint Kitts and Nevis", "189");
		countries.put("Saint Lucia", "190");
		countries.put("Saint Martin", "191");
		countries.put("Saint Pierre and Miquelon", "192");
		countries.put("Saint Vincent and the Grenadines", "193");
		countries.put("Samoa", "194");
		countries.put("San Marino", "195");
		countries.put("Sao Tome and Principe", "196");
		countries.put("Saudi Arabia", "197");
		countries.put("Senegal", "198");
		countries.put("Serbia", "199");
		countries.put("Seychelles", "200");
		countries.put("Sierra Leone", "201");
		countries.put("Singapore", "202");
		countries.put("Sint Maarten", "203");
		countries.put("Slovakia", "204");
		countries.put("Slovenia", "205");
		countries.put("Solomon Islands", "206");
		countries.put("Somalia", "207");
		countries.put("South Africa", "208");
		countries.put("South Georgia and the South Sandwich Islands", "209");
		countries.put("Spain", "210");
		countries.put("Sri Lanka", "211");
		countries.put("Sudan", "212");
		countries.put("Suriname", "213");
		countries.put("Svalbard and Jan Mayen", "214");
		countries.put("Swaziland", "215");
		countries.put("Sweden", "216");
		countries.put("Switzerland", "217");
		countries.put("Syrian Arab Republic", "218");
		countries.put("Taiwan", "219");
		countries.put("Tajikistan", "220");
		countries.put("Tanzania, United Republic Of", "221");
		countries.put("Thailand", "222");
		countries.put("Timor-Leste", "223");
		countries.put("Togo", "224");
		countries.put("Tokelau", "225");
		countries.put("Tonga", "226");
		countries.put("Trinidad and Tobago", "227");
		countries.put("Tunisia", "228");
		countries.put("Turkey", "229");
		countries.put("Turkmenistan", "230");
		countries.put("Turks and Caicos Islands", "231");
		countries.put("Tuvalu", "232");
		countries.put("Uganda", "233");
		countries.put("Ukraine", "234");
		countries.put("United Arab Emirates", "235");
		countries.put("United Kingdom", "236");
		countries.put("United States", "237");
		countries.put("United States Minor Outlying Islands", "238");
		countries.put("Uruguay", "239");
		countries.put("Uzbekistan", "240");
		countries.put("Vanuatu", "241");
		countries.put("Venezuela", "242");
		countries.put("Vietnam", "243");
		countries.put("Virgin Islands, British", "244");
		countries.put("Virgin Islands, U.S.", "245");
		countries.put("Wallis and Futuna", "246");
		countries.put("Western Sahara", "247");
		countries.put("Yemen", "248");
		countries.put("Zambia", "249");
		countries.put("Zimbabwe", "250");
		
		return countries;
	}
    
}
