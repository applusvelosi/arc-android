package applusvelosi.projects.android.arc.views.fragments.profile;

import java.util.ArrayList;
import java.util.LinkedHashMap;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import applusvelosi.projects.android.arc.ArcApplication;
import applusvelosi.projects.android.arc.R;
import applusvelosi.projects.android.arc.adapters.lists.MultipleLabelCheckboxSelectionAdapter;
import applusvelosi.projects.android.arc.adapters.lists.MultipleLabelCheckboxSelectionAdapter.MultiSelectionLabelCheckboxSelectionAdapterHolder;
import applusvelosi.projects.android.arc.adapters.lists.MultipleLabelCheckboxSelectionAdapter.MultipleLabelCheckboxSelector;
import applusvelosi.projects.android.arc.views.fragments.ActionbarFragment;

public class PreferredLocationSelectionFragment extends ActionbarFragment implements OnCheckedChangeListener, OnItemClickListener, MultipleLabelCheckboxSelector{
	private static PreferredLocationSelectionFragment instance;
	private View actionbarBackButton, actionbarDoneButton;
	private TextView actionbarTitle;
	
	private ListView lv;
	private LinkedHashMap<String, String> countries;
	private ArrayList<String> countryNames, selecteds;
	private MultipleLabelCheckboxSelectionAdapter adapter;
	
	public static PreferredLocationSelectionFragment getInstance(){
		if(instance == null)
			instance = new PreferredLocationSelectionFragment();
		
		return instance;
	}
	
	@Override
	protected RelativeLayout setupActionbar() {
		RelativeLayout actionbarLayout = (RelativeLayout)LayoutInflater.from(activity).inflate(R.layout.actionbar_backdone, null);
		actionbarBackButton = actionbarLayout.findViewById(R.id.buttons_actionbar_back);
		actionbarDoneButton = actionbarLayout.findViewById(R.id.buttons_actionbar_done);
		actionbarTitle = (TextView)actionbarLayout.findViewById(R.id.tviews_actionbar_title);
		actionbarTitle.setText("Select Countries");

		actionbarTitle.setOnClickListener(this);
		actionbarBackButton.setOnClickListener(this);
		actionbarDoneButton.setVisibility(View.GONE);
		
		return actionbarLayout;
	}

	@Override
	protected View createView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		View v = inflater.inflate(R.layout.fragment_lv, null);
		lv = (ListView)v.findViewById(R.id.lists_lv);
		
		countries = new LinkedHashMap<String, String>();
		countries.putAll(ArcApplication.getCountries());
		countryNames = new ArrayList<String>(countries.keySet());
		countryNames.remove("Any");
		selecteds = activity.user.getPreferredLocations();
		
		adapter = new MultipleLabelCheckboxSelectionAdapter(this, countryNames, selecteds);
		lv.setAdapter(adapter);
		lv.setOnItemClickListener(this);
		return v;
	}

	@Override
	public void onClick(View v) {
		if(v == actionbarBackButton || v == actionbarTitle){
			activity.onBackPressed();
		}
	}
	
	@Override
	public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
		String selLanguage = ((MultiSelectionLabelCheckboxSelectionAdapterHolder)((RelativeLayout)buttonView.getParent()).getTag()).tv.getText().toString();

		if(selecteds.contains(selLanguage))
			selecteds.remove(selLanguage);
		else
			selecteds.add(selLanguage);
		
		activity.user.updatePreferredLocations(selecteds);
	}

	@Override
	public void onItemClick(AdapterView<?> parent, View v, int pos, long id) {
		MultiSelectionLabelCheckboxSelectionAdapterHolder holder = (MultiSelectionLabelCheckboxSelectionAdapterHolder)v.getTag();
		holder.cb.setChecked(!(selecteds.contains(holder.tv.getText().toString())));		
	}

	@Override
	public OnCheckedChangeListener getCheckChangeListener() {
		return this;
	}

}
