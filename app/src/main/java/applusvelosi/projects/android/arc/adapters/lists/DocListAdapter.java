package applusvelosi.projects.android.arc.adapters.lists;

import java.util.ArrayList;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;
import applusvelosi.projects.android.arc.R;
import applusvelosi.projects.android.arc.models.Document;
import applusvelosi.projects.android.arc.views.SidebarActivity;

public class DocListAdapter extends BaseAdapter{
	private SidebarActivity activity;
	private ArrayList<Document> docs;
	
	public DocListAdapter(SidebarActivity activity, ArrayList<Document> docs){
		this.activity = activity;
		this.docs = docs;
	}

	@Override
	public View getView(int pos, View v, ViewGroup parent) {
		Holder holder;
		if(v == null){
			holder = new Holder();
			v = LayoutInflater.from(activity).inflate(R.layout.cell_document, null);
			holder.tvName = (TextView)v.findViewById(R.id.tviews_profile_doc_name);
			holder.tvExpireDate = (TextView)v.findViewById(R.id.tviews_profile_doc_expiredate);
			holder.tvTypeDesc = (TextView)v.findViewById(R.id.tviews_profile_doc_type);
			holder.tvSize = (TextView)v.findViewById(R.id.tviews_profile_doc_size);
			v.setTag(holder);
		}
		
		holder = (Holder)v.getTag();
		Document doc = docs.get(pos);
		holder.tvName.setText(doc.getName());
		holder.tvExpireDate.setText("Expire on "+doc.getExpireDate());
		holder.tvTypeDesc.setText(getTypeDescForKey(doc.getType()));
		holder.tvSize.setText(String.valueOf(doc.getSize())+" kb");
		
		return v;
	}
	
	@Override
	public int getCount() {
		return docs.size();
	}

	@Override
	public Object getItem(int pos) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public long getItemId(int pos) {
		// TODO Auto-generated method stub
		return 0;
	}

	private class Holder{
		private TextView tvName, tvExpireDate, tvTypeDesc, tvSize;
	}
	
	private String getTypeDescForKey(int key){
		switch(key){
			case 20: return "CV";
			case 21: return "Certificate";
			case 23: return "Criminal Record Check";
			case 24: return "Candidate Assessment";
			case 25: return "Approvals";
			case 26: return "Work Permit";
			case 27: return "Medicals";
			case 28: return "Identification";
			case 29: return "Employment Auth";
			case 30: return "Security Clearance";
			case 31: return "Interview Notes";
			case 32: return "Passport";
			case 33: return "Visa";
			case 34: return "Offer";
			case 35: return "Qualification";
			case 36: return "Driving License";
			case 37: return "CV Prepped";
			case 38: return "On Boarding";
			case 39: return "Candidate Permission";
			case 40: return "Criminal Convictions";
			case 41: return "Disability Information";
			case 42: return "CV Other";
			default: return "Unknown";
		}
	}
}
