package applusvelosi.projects.android.arc.views.fragments;

import java.util.ArrayList;

import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.AdapterView.OnItemClickListener;
import applusvelosi.projects.android.arc.R;
import applusvelosi.projects.android.arc.adapters.lists.HomeAdapter;
import applusvelosi.projects.android.arc.models.JobLatest;
import applusvelosi.projects.android.arc.utils.SaltProgressDialog;
import applusvelosi.projects.android.arc.utils.interfaces.RootFragment;

public class HomeFragment extends ActionbarFragment implements RootFragment, OnItemClickListener{
	private static HomeFragment instance;
	private View actionbarMenuButton, actionbarRefereshButton;
	private ListView lv;
	private ArrayList<JobLatest> jobs;
	private HomeAdapter adapter;
	private SaltProgressDialog pd;
	
	public static HomeFragment getInstance(){
		if(instance == null)
			instance = new HomeFragment();
		
		return instance;
	}
	
	@Override
	protected RelativeLayout setupActionbar() {
		RelativeLayout actionbarLayout = (RelativeLayout)LayoutInflater.from(activity).inflate(R.layout.actionbar_menurefresh, null);
		actionbarMenuButton = actionbarLayout.findViewById(R.id.buttons_actionbar_menu);
		actionbarRefereshButton = actionbarLayout.findViewById(R.id.buttons_actionbar_refresh);
		((TextView)actionbarLayout.findViewById(R.id.tviews_actionbar_title)).setText("Latest Vacancies");

		actionbarMenuButton.setOnClickListener(this);
		actionbarRefereshButton.setOnClickListener(this);
		
		return actionbarLayout;
	}

	@Override
	protected View createView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		View v = inflater.inflate(R.layout.fragment_lv, null);
		lv = (ListView)v.findViewById(R.id.lists_lv);
		if(jobs == null){
			jobs = new ArrayList<JobLatest>();
			pd = new SaltProgressDialog(activity);
			pd.show();
			new Thread(new LatestVacancyLoader()).start();
		}
		
		adapter = new HomeAdapter(activity, jobs);
		lv.setAdapter(adapter);
		lv.setOnItemClickListener(this);
				
		return v;
	}

	@Override
	public void onClick(View v) {
		if(v == actionbarMenuButton){
			v.setEnabled(false);
			activity.toggleSidebar(v);
		}else if(v == actionbarRefereshButton){
			pd.show();
			new Thread(new LatestVacancyLoader()).start();			
		}
	}

	@Override
	public void disableUserInteractionsOnSidebarShown() {
		lv.setEnabled(false);
	}

	@Override
	public void enableUserInteractionsOnSidebarHidden() {
		lv.setEnabled(true);
	}

	@Override
	public void onItemClick(AdapterView<?> arg0, View arg1, int arg2, long arg3) {
		activity.changeChildPage(JobDetailFragment.newInstance(jobs.get(arg2).getJobID(), true));
	}
	
	private class LatestVacancyLoader implements Runnable{

		@Override
		public void run() {
			Object tempResult;
			try{
				tempResult = app.onlineGateway.getLatestVacancies("", "0", "0", "100", "0", "0", "0");
			}catch(Exception e){
				tempResult = e.getMessage();
			}
			final Object result = tempResult;
			
			new Handler(Looper.getMainLooper()).post(new Runnable() {
				
				@Override
				public void run() {
					pd.dismiss();
					if(result instanceof String)
						app.showMessageDialog(activity, result.toString());
					else{
						jobs.clear();
						jobs.addAll((ArrayList<JobLatest>)result);
						adapter.notifyDataSetChanged();
					}
				}
			});
		}
		
	}

}
