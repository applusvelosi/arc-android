package applusvelosi.projects.android.arc.models;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;

import org.json.JSONObject;

import applusvelosi.projects.android.arc.ArcApplication;
import applusvelosi.projects.android.arc.utils.OnlineGateway;

public class SavedSearch {

	public HashMap<String, Object> ss;
	
	public SavedSearch(JSONObject jsonApp) throws Exception{
		ss = new HashMap<String, Object>();
		if(jsonApp != JSONObject.NULL) {
			ss = OnlineGateway.toMap(jsonApp);
	    }
	}
	
	public static String getJSONFromNewSavedSearch(String name, int candidateID, String dateToday, String searchFor, 
												   int searchTypeID, String searchType, String location, float lat, float lng,
												   int countryID, int distance, int jobTypeID, String jobType, int postedWithin,
												   boolean willAlert, ArcApplication app){
		
		HashMap<String, Object> map = new HashMap<String, Object>();
		map.put("Name", name);
		map.put("Radius", distance);
		HashMap<String, Object> vacancyMap = new HashMap<String, Object>();
		vacancyMap.put("VacancySearchIn", searchTypeID);
		vacancyMap.put("VacancySearchInID", searchTypeID);
		vacancyMap.put("VacancySearchInName", searchType);
		map.put("VacancySearchIn", vacancyMap);
		map.put("JBEID", 0);
		HashMap<String, Object> industryMap = new HashMap<String, Object>();
		industryMap.put("IndustryName", "Unknown");
		industryMap.put("Industry", 0);
		industryMap.put("IndustryID", 0);
		map.put("Industry", industryMap);
		map.put("CandidateID", candidateID);
		map.put("EmailAlert", willAlert);
		map.put("SearchText", searchFor);
		map.put("DateCreated", dateToday);
		System.out.println("date created "+dateToday);
		HashMap<String, Object> locationMap = new HashMap<String, Object>();
		locationMap.put("AccentCity", "");
		locationMap.put("City", "");
		locationMap.put("CodeInt", 0);
		locationMap.put("Country2", "");
		locationMap.put("CountryName", "");
		locationMap.put("Latitude", lat);
		locationMap.put("LocationFull", location);
		locationMap.put("Longitude", lng);
		locationMap.put("Population", 0);
		locationMap.put("Region", "");
		locationMap.put("Region2", "");
		map.put("Location", locationMap);
		HashMap<String, Object> typeMap = new HashMap<String, Object>();
		typeMap.put("Description", jobType);
		typeMap.put("Type", jobTypeID);
		typeMap.put("TypeID", jobTypeID);
		map.put("VacancyType", typeMap);
		map.put("CountryID", countryID);
		map.put("LastXDays", postedWithin);
		return app.gson.toJson(map, app.types.hashmapOfStringObject);
	}
	
	public String getJSONFromEditSavedSearch(String name, String searchFor, int searchTypeID, String searchType, 
											 String location, float lat, float lng, int countryID, int distance,
											 int jobTypeID, String jobType, int postedWithin,
											 ArcApplication app){
		ss.put("Name", name);
		ss.put("SearchText", searchFor);
		HashMap<String, Object> vacancyMap = new HashMap<String, Object>();
		vacancyMap.put("VacancySearchIn", searchTypeID);
		vacancyMap.put("VacancySearchInID", searchTypeID);
		vacancyMap.put("VacancySearchInName", searchType);
		ss.put("VacancySearchIn", vacancyMap);
		HashMap<String, Object> locationMap = new HashMap<String, Object>();
		locationMap.put("AccentCity", "");
		locationMap.put("City", "");
		locationMap.put("CodeInt", 0);
		locationMap.put("Country2", "");
		locationMap.put("CountryName", "");
		locationMap.put("Latitude", lat);
		locationMap.put("LocationFull", location);
		locationMap.put("Longitude", lng);
		locationMap.put("Population", 0);
		locationMap.put("Region", "");
		locationMap.put("Region2", "");
		ss.put("Location", locationMap);
		ss.put("CountryID", countryID);
		ss.put("Radius", distance);
		HashMap<String, Object> typeMap = new HashMap<String, Object>();
		typeMap.put("Description", jobType);
		typeMap.put("Type", jobTypeID);
		typeMap.put("TypeID", jobTypeID);
		ss.put("VacancyType", typeMap);
		ss.put("LastXDays", postedWithin);
		
		return app.gson.toJson(ss, app.types.hashmapOfStringObject);
	}
	
	public int getJBEID(){
		return Integer.parseInt(ss.get("JBEID").toString());
	}
	
	public String getName(){
		return ss.get("Name").toString();
	}
	
	public String getDateAdded(OnlineGateway g){
		return g.dJsonizeDate(ss.get("DateCreated").toString());
	}

	public String getSearchFor(){
		return ss.get("SearchText").toString();
	}

	public String getSearchIn(){
		return ((HashMap<String, Object>)ss.get("VacancySearchIn")).get("VacancySearchInName").toString();
	}

	public int getSearchInID(){
		return Integer.parseInt(((HashMap<String, Object>)ss.get("VacancySearchIn")).get("VacancySearchInID").toString());
	}

	public void setLocation(String locationFull){
		((HashMap<String, Object>)ss.get("Location")).put("LocationFull", locationFull);
	}
	
	public String getLocation(){
		System.out.println("location "+ss.get("Location"));
		String loc = ((HashMap<String, Object>)ss.get("Location")).get("LocationFull").toString();
		return (loc.equals(""))?"Any":loc;
	}

	public void setCountryID(int countryID){
		ss.put("CountryID", countryID);
	}
	
	public int getCountryID(){
		return Integer.parseInt(ss.get("CountryID").toString());
	}

	public String getCountryName(){
		LinkedHashMap<String, String> countries = ArcApplication.getCountries();
		int countryID = getCountryID();
		return (countryID>=0)?new ArrayList<String>(countries.keySet()).get(new ArrayList<String>(countries.values()).indexOf(String.valueOf(getCountryID()))):"Any";
	}
	
	public String getDistance(){
		return ss.get("Radius").toString();
	}
	
	public int getJobTypeID(){
		return Integer.parseInt(((HashMap<String, Object>)ss.get("VacancyType")).get("TypeID").toString());
	}
	
	public int getPostedWithin(){
		return Integer.parseInt(ss.get("LastXDays").toString());
	}
	
	public boolean willAlert(){
		return Boolean.parseBoolean(ss.get("EmailAlert").toString());
	}
	
	public void changeSubscriptionWillAlert(boolean willAlert){
		ss.put("EmailAlert", willAlert);
	}
	
	public String jsonize(ArcApplication app){
		return app.gson.toJson(ss, app.types.hashmapOfStringObject);
	}
}
