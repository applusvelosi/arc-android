package applusvelosi.projects.android.arc.views.fragments.profile;

import java.util.Date;

import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnFocusChangeListener;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;
import applusvelosi.projects.android.arc.R;
import applusvelosi.projects.android.arc.models.Employment;
import applusvelosi.projects.android.arc.utils.SaltDatePicker;
import applusvelosi.projects.android.arc.utils.SaltProgressDialog;
import applusvelosi.projects.android.arc.views.fragments.ActionbarFragment;

public class UserEmploymentDetailsFragment extends ActionbarFragment implements OnFocusChangeListener{
	private static final String KEY_EMP_POS = "useremploymentdetailskeyemploymentposition";
	
	private View actionbarButtonBack;
	private TextView actionbarTitle, actionbarButtonSave;

	private EditText etEmployer, etJobTitle, etStartDate, etEndDate, etDesc;
	private SaltProgressDialog pd;
	private Employment emp;
	
	public static UserEmploymentDetailsFragment newInstance(int pos){
		UserEmploymentDetailsFragment frag = new UserEmploymentDetailsFragment();
		Bundle b = new Bundle();
		b.putInt(KEY_EMP_POS, pos);
		
		frag.setArguments(b);
		return frag;
	}
	
	@Override
	protected RelativeLayout setupActionbar() {
		RelativeLayout actionbar = (RelativeLayout)LayoutInflater.from(activity).inflate(R.layout.actionbar_backdone, null);
		actionbarButtonBack = actionbar.findViewById(R.id.buttons_actionbar_back);
		actionbarButtonSave = (TextView)actionbar.findViewById(R.id.buttons_actionbar_done);
		actionbarTitle = (TextView)actionbar.findViewById(R.id.tviews_actionbar_title);
		actionbarTitle.setText("Employment Detail");
		actionbarButtonSave.setText("Save");
		
		actionbarTitle.setOnClickListener(this);
		actionbarButtonBack.setOnClickListener(this);
		actionbarButtonSave.setOnClickListener(this);
		return actionbar;
	}

	@Override
	protected View createView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		View v = inflater.inflate(R.layout.fragment_profile_employment_detail, null);
		etEmployer = (EditText)v.findViewById(R.id.etexts_profile_employmentdetail_employer);
		etJobTitle = (EditText)v.findViewById(R.id.etexts_profile_employmentdetail_jobtitle);
		etStartDate = (EditText)v.findViewById(R.id.etexts_profile_employmentdetail_startdate);
		etEndDate = (EditText)v.findViewById(R.id.etexts_profile_employmentdetail_enddate);
		etDesc = (EditText)v.findViewById(R.id.etexts_profile_employmentdetail_desc);
		
		if(getArguments()!=null){
			emp = UserEmploymentFragment.getInstance().editEmployment(getArguments().getInt(KEY_EMP_POS), this);
			actionbarTitle.setText(emp.getJobTitle());
			etEmployer.setText(emp.getEmployer());
			etJobTitle.setText(emp.getJobTitle());
			etStartDate.setText(emp.getDateStart());
			etEndDate.setText(emp.getDateEnd());
			etDesc.setText(emp.getDescription());
		}
		
		etEmployer.setOnFocusChangeListener(this);
		etJobTitle.setOnFocusChangeListener(this);
		etStartDate.setOnFocusChangeListener(this);
		etEndDate.setOnFocusChangeListener(this);
		etDesc.setOnFocusChangeListener(this);
		
		return v;
	}

	@Override
	public void onClick(View v) {
		if(v == actionbarButtonBack || v == actionbarTitle)
			activity.onBackPressed();
		else if(v == actionbarButtonSave){
			try{
				String jsonEmployment;
				String employer = etEmployer.getText().toString();
				String jobTitle = etJobTitle.getText().toString();
				String startDate = etStartDate.getText().toString();
				String endDate = etEndDate.getText().toString();
				String desc = etDesc.getText().toString();
				String dateToday = app.onlineGateway.jsonizeDate(new Date());
				int userID = Integer.parseInt(app.offlineGateway.getUserID());
				
				if(getArguments() == null){
					jsonEmployment = Employment.getJSONFromNewEmployment(employer, jobTitle, startDate, endDate, desc, dateToday, userID, app);
				}else{
					jsonEmployment = emp.getJSONFromEditEmployment(employer, jobTitle, startDate, endDate, desc, app);
				}
				
				if(pd == null)
					pd = new SaltProgressDialog(activity);
				pd.show();
				new Thread(new EmploymentSaver(jsonEmployment)).start();
			}catch(Exception e){
				e.printStackTrace();
				app.showMessageDialog(activity, "Unable to parse current date");
			}
		}
	}
	
	private class EmploymentSaver implements Runnable{
		private String jsonEmployment;
		
		public EmploymentSaver(String jsonEmployment){
			this.jsonEmployment = jsonEmployment;
		}
		
		@Override
		public void run() {
			String tempResult;
			
			try{
				tempResult = app.onlineGateway.saveEmployment(jsonEmployment);
			}catch(Exception e){
				e.printStackTrace();
				tempResult = e.getMessage();
			}
			final String result = tempResult;
			
			new Handler(Looper.getMainLooper()).post(new Runnable() {
				
				@Override
				public void run() {
					pd.dismiss();
					app.showMessageDialog(activity, (result==null)?"Saved Successfully":result);
				}
			});
		}
		
	}

	@Override
	public void onFocusChange(View v, boolean hasFocus) {
		((TextView)v).setTextColor(activity.getResources().getColor((hasFocus)?R.color.black:R.color.menu_list_separator));

		if(v==etStartDate && hasFocus){
			new SaltDatePicker(activity, etStartDate);
		}else if(v==etEndDate && hasFocus){
			new SaltDatePicker(activity, etEndDate);
		}
	}

}
