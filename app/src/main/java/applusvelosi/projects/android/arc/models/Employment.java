package applusvelosi.projects.android.arc.models;

import java.util.HashMap;

import org.json.JSONObject;

import applusvelosi.projects.android.arc.ArcApplication;
import applusvelosi.projects.android.arc.utils.OnlineGateway;

public class Employment {

	private HashMap<String, Object> emp;
	
	public Employment(JSONObject jsonEmp) throws Exception{
		emp = new HashMap<String, Object>();
		if(jsonEmp != JSONObject.NULL) {
			emp = OnlineGateway.toMap(jsonEmp);
	    }
	}
	
	public int getEmploymentID(){
		return Integer.parseInt(emp.get("JobID").toString());
	}
	
	public String getEmployer(){
		return emp.get("Employer").toString();
	}
	
	public String getJobTitle(){
		return emp.get("Title").toString();
	}

	public String getDateStart(){
		return emp.get("StartDate").toString();
	}

	public String getDateEnd(){
		return emp.get("EndDate").toString();
	}
	
	public String getDescription(){
		return emp.get("Description").toString();
	}
	
	public static String getJSONFromNewEmployment(String employer, String jobTitle, String startDate, String endDate, String desc, String dateToday, int userID, ArcApplication app){
		HashMap<String, Object> map = new HashMap<String, Object>();
		map.put("JobID", 0);
		map.put("CandidateID", userID);
		map.put("DateCreated", dateToday);
		map.put("OrderID", 1);
		map.put("Employer", employer);
		map.put("Title", jobTitle);
		map.put("StartDate", startDate);
		map.put("EndDate", endDate);
		map.put("Description", desc);
		
		return app.gson.toJson(map, app.types.hashmapOfStringObject);
	}

	public String getJSONFromEditEmployment(String employer, String jobTitle, String startDate, String endDate, String desc, ArcApplication app){
		emp.put("Employer", employer);
		emp.put("Title", jobTitle);
		emp.put("StartDate", startDate);
		emp.put("EndDate", endDate);
		emp.put("Description", desc);
		
		return app.gson.toJson(emp, app.types.hashmapOfStringObject);
	}
}
